﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Uniwiki.Server.Shared.Tests.Extensions
{
	public static class CollectionExtensions
	{
		public static void CompareItems<TItem, TAnotherItem>(
			this IEnumerable<TItem> collection,
			IEnumerable<TAnotherItem> anotherCollection,
			Func<TItem, TAnotherItem, bool> matchFunc,
			Action<TItem, TAnotherItem> compareFunc)
		{
			Assert.AreEqual(collection.Count(), anotherCollection.Count());
			collection
				.Select(i => (Item: i, AnotherItem: anotherCollection
						.Single(ai => matchFunc(i, ai))
					))
				.ToList()
				.ForEach(p => compareFunc(p.Item, p.AnotherItem));
		}
	}
}