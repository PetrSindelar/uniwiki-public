using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.Shared.Tests
{
	[TestClass]
	public class FunctionalExtensionsTests
	{
		[TestMethod]
		public void ApplyIfNotCalledOnNull()
		{
			string? value = null;

			value?.Apply(v =>
			{
				Console.WriteLine("WOOOOOOOOOOOOOOOOO");
				return v;
			});
		}
	}
}