﻿using System;
using System.Linq;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.Shared.Tests
{
	public class Generators
	{
		// Root character sets
		private const string LowerAlphabetChars = "abcdefghijklmnopqrstuvwxyz";
		private const string NumericChars = "0123456789";
		private const string ExtendedAlphabetChars = "ěščřžýáíéůúňń";
		private const string SpecialChars = "+;~!@#$%^&*()_}{|\"':?><?:-¨§)( \t\n\n\r";
		private const string GeneralUrlChars = NumericChars + LowerAlphabetChars + "-";

		private static string GeneralNameChars { get; } = LowerAlphabetChars + LowerAlphabetChars.ToUpper() +
		                                                  ExtendedAlphabetChars + ExtendedAlphabetChars.ToUpper() + " ";

		private static string GeneralNameStandardized { get; } = LowerAlphabetChars + ExtendedAlphabetChars;

		private static string GeneralTextChars { get; } = GeneralNameChars + SpecialChars;

		private static Func<Random, string> RandomString(char[] chars)
			=> RandomString(5, chars);

		private static Func<Random, string> RandomString(int minLengthInclusive, char[] chars)
			=> RandomString(minLengthInclusive, 15, chars);

		private static Func<Random, string> RandomString(int minLengthInclusive, int maxLengthExclusive, char[] chars)
			=> random => random
				.Next(minLengthInclusive, maxLengthExclusive)
				.Apply(x => RandomStringOfLength(x, chars))(random);

		private static Func<Random, string> RandomStringOfLength(int length, char[] chars)
			=> random => new string(
				Enumerable.Repeat(chars, length)
					.Select(s => s[random.Next(s.Length)])
					.ToArray()
			);

		public static Func<Random, string> RandomText(int minLength, int maxLength)
			=> random => RandomString(minLength, maxLength, GeneralTextChars.ToCharArray())(random);

		public static Func<Random, string> RandomUrl()
			=> RandomString(
				Constants.Validations.UrlMinLength,
				Constants.Validations.UrlMaxLength,
				GeneralUrlChars.ToCharArray());

		// Note: Each
		public static Func<Random, string> RandomName(int maxLength)
			=> random => "A" + RandomString(2, maxLength - 1, GeneralNameChars.ToCharArray())(random);

		public static Func<Random, string> RandomNameStandardized(int maxLength)
			=> random => RandomString(4, maxLength, GeneralNameStandardized.ToCharArray())(random);

		public static Func<Random, string> RandomEmail()
			=> random => RandomString(2, 7, LowerAlphabetChars.ToCharArray())(random) + "@" +
			             RandomString(2, 7, LowerAlphabetChars.ToCharArray())(random) + "." +
			             RandomString(2, 3, LowerAlphabetChars.ToCharArray())(random);

		public static Func<Random, DateTime> RandomDate()
			=> random => new DateTime(
				random.Next(2010, 2031),
				random.Next(1, 13),
				random.Next(1, 28),
				random.Next(0, 24),
				random.Next(0, 60),
				random.Next(0, 60)
			);

		public static Func<Random, bool> RandomBool()
			=> random => random.Next(0, 2) switch
			{
				0 => false,
				1 => true,
				_ => throw new("Generated a number, that its not able to translate to a bool.")
			};

		public static Func<Random, int> RandomNumber(int max, int min = 1)
			=> random => random.Next(min, max + 1);
	}
}