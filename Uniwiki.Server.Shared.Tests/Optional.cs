﻿using System;

namespace Uniwiki.Server.Shared.Tests
{
	public readonly struct Optional<T>
	{
		// Make it possible to assing optional values just like regular values
		public static implicit operator Optional<T>(T value) => new Optional<T>(value);

		private readonly T _value;

		public bool HasValue { get; }

		public T Value
			=> HasValue ? _value : throw new NullReferenceException("Dont access a value, that does not exist.");

		public Optional(T value)
		{
			_value = value;
			HasValue = true;
		}

		public T GetOr(Func<T> valueGenerator) => HasValue ? Value : valueGenerator();

		public static Optional<T> operator +(Optional<T> a, Optional<T> b) => a;
	}
}