﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.WebApplication.Post.AddPost;

namespace Uniwiki.Server.WebApplication.Tests.Post.AddPost
{
	[TestClass]
	public class AddPostServerActionTests
	{
		[TestMethod]
		public async Task ExecuteAsync_AddsAPostToTheDB()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<AddPostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var course = new CourseModelBuilder().Build(uniwikiContext)(random);

			var category = "Post category";
			var description = "Post description";
			var isAnonymous = false;
			var request = new AddPostRequest(course.Faculty.University.Url, course.Faculty.Url, course.Url, category,
				description, isAnonymous, user.Id);

			// Act
			_ = await serverAction.ExecuteAsync(request);

			var postsCount = uniwikiContext.Posts.Count();
			var postFromDb = uniwikiContext.Posts.First();

			// Assert
			Assert.AreEqual(1, postsCount);
			Assert.AreEqual(postFromDb.Category, category);
			Assert.AreEqual(postFromDb.Text, description);
			Assert.AreEqual(postFromDb.IsAnonymous, isAnonymous);
		}

		[TestMethod]
		public async Task ExecuteAsync_RedirectsToTheNewlyCreatedPostPage()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<AddPostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var course = new CourseModelBuilder().Build(uniwikiContext)(random);

			var category = "Post category";
			var description = "Post description";
			var isAnonymous = false;
			var request = new AddPostRequest(course.Faculty.University.Url, course.Faculty.Url, course.Url, category,
				description, isAnonymous, user.Id);

			// Act
			var viewModel = await serverAction.ExecuteAsync(request);

			// Assert
			Assert.AreEqual(course.Faculty.University.Url, viewModel.RedirectRoute.UniversityUrl);
			Assert.AreEqual(course.Faculty.Url, viewModel.RedirectRoute.FacultyUrl);
			Assert.AreEqual(course.Url, viewModel.RedirectRoute.CourseUrl);
			Assert.AreEqual(uniwikiContext.Posts.First().Url, viewModel.RedirectRoute.PostUrl);
		}

		[TestMethod]
		public async Task ExecuteAsync_AssignsAllThePostFilesFromTheSameUserFromTheSameCourseToTheNewPost()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<AddPostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var course = new CourseModelBuilder().Build(uniwikiContext)(random);
			var rightFile1 =
				new PostFileModelBuilder() { Course = course, Profile = user }.Build(uniwikiContext)(random);
			var wrongFile1 = new PostFileModelBuilder().Build(uniwikiContext)(random);
			var wrongFile2 = new PostFileModelBuilder() { Profile = user }.Build(uniwikiContext)(random);
			var rightFile2 =
				new PostFileModelBuilder() { Course = course, Profile = user }.Build(uniwikiContext)(random);
			var wrongFile3 = new PostFileModelBuilder() { Course = course, }.Build(uniwikiContext)(random);

			var request = new AddPostRequest(course.Faculty.University.Url, course.Faculty.Url, course.Url, "Category",
				"Description", false, user.Id);

			// Act
			_ = await serverAction.ExecuteAsync(request);

			// Assert
			var post = uniwikiContext.Posts.Include(p => p.PostFiles).First();
			var rightFiles = post.PostFiles;
			Assert.AreEqual(2, rightFiles.Count);
			Assert.IsTrue(rightFiles.Any(pf => pf.Id == rightFile1.Id));
			Assert.IsTrue(rightFiles.Any(pf => pf.Id == rightFile2.Id));
		}
	}
}