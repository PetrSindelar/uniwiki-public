﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Tests;
using Uniwiki.Server.WebApplication.Uni.AddPostComment;

namespace Uniwiki.Server.WebApplication.Tests.Post.AddPostComment
{
	[TestClass]
	public class AddPostCommentServerActionTests
	{
		[TestMethod]
		public async Task Execute_AddsAPostToTheDB()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<AddPostCommentServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var post = new PostModelBuilder().Build(uniwikiContext)(random);

			var request = new AddPostCommentRequest(post.Id, Generators.RandomText(10, 100)(random), user.Id);

			// Act
			var viewModel = await serverAction.ExecuteAsync(request);

			var commentsCount = uniwikiContext.PostComments.Count();

			// Assert
			Assert.AreEqual(1, commentsCount);
			Assert.AreEqual(user.Url, viewModel.AuthorPageRoute.ProfileUrl);
			Assert.IsFalse(string.IsNullOrWhiteSpace(viewModel.Text));
		}
	}
}