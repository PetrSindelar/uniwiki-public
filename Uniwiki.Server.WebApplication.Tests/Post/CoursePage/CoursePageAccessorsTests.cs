﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Tests;
using Uniwiki.Server.Shared.Tests.Extensions;
using Uniwiki.Server.WebApplication.Uni.CoursePage;

namespace Uniwiki.Server.WebApplication.Tests.Post.CoursePage
{
	[TestClass]
	public class CoursePageAccessorsTests
	{
		[TestClass]
		public class GetPostItemsTests
		{
			[TestMethod]
			public async Task GetPostItems()
			{
				// Arrange
				var serviceProvider = TestSetup.GetServiceProvider();
				var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();

				var random = new Random(0);

				var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
				var course1 = new CourseModelBuilder().Build(uniwikiContext)(random);
				var course2 = new CourseModelBuilder().Build(uniwikiContext)(random);
				var course3 = new CourseModelBuilder().Build(uniwikiContext)(random);

				var post1 = new PostModelBuilder() { Course = course2 }.Build(uniwikiContext)(random);
				var postA = new PostModelBuilder().Build(uniwikiContext)(random);
				var post2 = new PostModelBuilder() { Course = course2 }.Build(uniwikiContext)(random);
				var postB = new PostModelBuilder().Build(uniwikiContext)(random);
				var post3 = new PostModelBuilder() { Course = course2 }.Build(uniwikiContext)(random);
				var postC = new PostModelBuilder().Build(uniwikiContext)(random);
				var post4 = new PostModelBuilder() { Course = course2 }.Build(uniwikiContext)(random);
				var postD = new PostModelBuilder().Build(uniwikiContext)(random);

				var expectedPosts = new[] { post1, post2, post3, post4 };

				var currentTime = Generators.RandomDate()(random);

				// Act
				var postItems =
					await CoursePageAccessors.GetPostItems(course2.Id, user.Id, currentTime)(uniwikiContext);

				// Assert
				postItems
					.CompareItems(
						expectedPosts,
						(pi, pm) => pi.PostPageRoute.PostUrl == pm.Url,
						(pi, pm) =>
						{
							Assert.AreEqual(pi.AuthorRoute.ProfileUrl, pm.Author.Url);
							Assert.AreEqual(pi.PostBarViewModel.LikesCount, pm.Likes.Count);
							Assert.AreEqual(pi.FilesCount, pm.PostFiles.Count);
							Assert.AreEqual(pi.PostBarViewModel.CommentsCount, pm.Comments.Count);
							Assert.AreEqual(pi.Text, pm.Text);
						}
					);
			}
		}
	}
}