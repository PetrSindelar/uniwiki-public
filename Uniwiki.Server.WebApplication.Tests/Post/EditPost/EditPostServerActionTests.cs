﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.EditPost;
using Uniwiki.Server.WebApplication.Tests.TestHelpers;

namespace Uniwiki.Server.WebApplication.Tests.Post.EditPost
{
	[TestClass]
	public class EditPostServerActionTests
	{
		[TestMethod]
		public async Task ExecuteAsync_EditsTheCategoryAndTextAndIsAnonymousSuccessfully()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<EditPostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);

			var initialCategory = "Post category";
			var newCategory = "New Post category";
			var initialText = "Post text";
			var newText = "New Post text";
			var initialIsAnonymous = false;
			var newIsAnonymous = true;

			var post = new PostModelBuilder
			{
				Author = user, Category = initialCategory, Text = initialText, IsAnonymous = initialIsAnonymous
			}.Build(uniwikiContext)(random);

			var request = new EditPostRequest(post.Id, user.ToUserInfo(), newText, newCategory, newIsAnonymous);

			// Act
			_ = await serverAction.ExecuteAsync(request);

			var postsCount = uniwikiContext.Posts.Count();
			var postFromDb = uniwikiContext.Posts.First();

			// Assert
			Assert.AreEqual(1, postsCount);
			Assert.AreEqual(postFromDb.Category, newCategory);
			Assert.AreEqual(postFromDb.Text, newText);
			Assert.AreEqual(postFromDb.IsAnonymous, newIsAnonymous);
		}

		[TestMethod]
		public async Task ExecuteAsync_UserCannotEditPostWhichIsNotHisIfHeIsNotAdmin()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<EditPostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);

			var initialCategory = "Post category";
			var newCategory = "New Post category";
			var initialText = "Post text";
			var newText = "New Post text";
			var initialIsAnonymous = false;
			var newIsAnonymous = true;

			var post = new PostModelBuilder()
			{
				Category = initialCategory, Text = initialText, IsAnonymous = initialIsAnonymous
			}.Build(uniwikiContext)(random);

			var request = new EditPostRequest(post.Id, user.ToUserInfo(), newText, newCategory, newIsAnonymous);

			// Act
			await Assert.ThrowsExceptionAsync<NotAuthorizedException>(() => serverAction.ExecuteAsync(request));

			var postsCount = uniwikiContext.Posts.Count();
			var postFromDb = uniwikiContext.Posts.First();

			// Assert
			Assert.AreEqual(1, postsCount);
			Assert.AreEqual(postFromDb.Category, initialCategory);
			Assert.AreEqual(postFromDb.Text, initialText);
			Assert.AreEqual(postFromDb.IsAnonymous, initialIsAnonymous);
		}

		[TestMethod]
		public async Task ExecuteAsync_UserCanEditPostWhichIsNotHisIfHeIsAnAdmin()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<EditPostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);

			var initialCategory = "Post category";
			var newCategory = "New Post category";
			var initialText = "Post text";
			var newText = "New Post text";
			var initialIsAnonymous = false;
			var newIsAnonymous = true;

			var post = new PostModelBuilder()
			{
				Category = initialCategory,
				Text = initialText,
				IsAnonymous = initialIsAnonymous
			}.Build(uniwikiContext)(random);

			var request = new EditPostRequest(post.Id, user.ToUserInfo(true), newText, newCategory, newIsAnonymous);

			// Act
			await serverAction.ExecuteAsync(request);

			var postsCount = uniwikiContext.Posts.Count();
			var postFromDb = uniwikiContext.Posts.First();

			// Assert
			Assert.AreEqual(1, postsCount);
			Assert.AreEqual(postFromDb.Category, newCategory);
			Assert.AreEqual(postFromDb.Text, newText);
			Assert.AreEqual(postFromDb.IsAnonymous, newIsAnonymous);
		}
	}
}