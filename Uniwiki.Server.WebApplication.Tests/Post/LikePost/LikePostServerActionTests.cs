﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Tests;
using Uniwiki.Server.WebApplication.Uni.LikePost;

namespace Uniwiki.Server.WebApplication.Tests.Post.LikePost
{
	[TestClass]
	public class LikePostServerActionTests : TestBase
	{
		[TestMethod]
		public async Task Execute_CanLikeDislikeLikeWithoutFailure()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<LikePostServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var post = new PostModelBuilder().Build(uniwikiContext)(random);

			var requestToLike = new LikePostRequest(post.Course.Faculty.University.Url,
				post.Course.Faculty.Url, post.Course.Url, post.Url, true, user.Id);
			var requestToDislike = new LikePostRequest(post.Course.Faculty.University.Url,
				post.Course.Faculty.Url, post.Course.Url, post.Url, false, user.Id);

			// Act
			await serverAction.ExecuteAsync(requestToLike);

			// Check if the post is liked
			var likesCountA = uniwikiContext.PostLikes.Count(l => l.PostId == post.Id && l.IsLiked);

			await serverAction.ExecuteAsync(requestToDislike);

			// Check if the post is disliked
			var likesCountB = uniwikiContext.PostLikes.Count(l => l.PostId == post.Id && l.IsLiked);

			await serverAction.ExecuteAsync(requestToLike);

			// Check if the post is liked again
			var likesCountC = uniwikiContext.PostLikes.Count(l => l.PostId == post.Id && l.IsLiked);

			// Assert
			Assert.AreEqual(1, likesCountA);
			Assert.AreEqual(0, likesCountB);
			Assert.AreEqual(1, likesCountC);
		}
	}
}