﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Tests;
using Uniwiki.Server.WebApplication.Uni.LikePostComment;

namespace Uniwiki.Server.WebApplication.Tests.Post.LikePostComment
{
	[TestClass]
	public class LikePostCommentServerActionTests : TestBase
	{
		[TestMethod]
		public async Task Execute_CanLikeDislikeLikeWithoutFailure()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var serverAction = serviceProvider.GetRequiredService<LikePostCommentServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var comment = new PostCommentModelBuilder().Build(uniwikiContext)(random);

			var requestToLike = new LikePostCommentRequest(comment.Id, true, user.Id);
			var requestToDislike = new LikePostCommentRequest(comment.Id, false, user.Id);

			// Act
			await serverAction.ExecuteAsync(requestToLike);

			// Check if the post is liked
			var likesCountA = uniwikiContext.PostCommentLikes.Count(l => l.PostCommentId == comment.Id && l.IsLiked);

			await serverAction.ExecuteAsync(requestToDislike);

			// Check if the post is disliked
			var likesCountB = uniwikiContext.PostCommentLikes.Count(l => l.PostCommentId == comment.Id && l.IsLiked);

			await serverAction.ExecuteAsync(requestToLike);

			// Check if the post is liked again
			var likesCountC = uniwikiContext.PostCommentLikes.Count(l => l.PostCommentId == comment.Id && l.IsLiked);

			// Assert
			Assert.AreEqual(1, likesCountA);
			Assert.AreEqual(0, likesCountB);
			Assert.AreEqual(1, likesCountC);
		}
	}
}