﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.WebApplication.Uni.PostPage;

namespace Uniwiki.Server.WebApplication.Tests.Post.PostPage
{
	[TestClass]
	public class PostPageAccessorsTests
	{
		[TestMethod]
		public async Task GetPostPageViewModel_CanBeExecutedCorrectly()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();

			var currentTime = new DateTime(2020, 11, 4, 3, 2, 1);
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);

			var post = new PostModelBuilder().Build(uniwikiContext)(random);

			// Likes for the post
			new PostLikeModelBuilder { Post = post, IsLiked = true }.Build(uniwikiContext)(random);
			new PostLikeModelBuilder { Post = post, IsLiked = true }.Build(uniwikiContext)(random);
			new PostLikeModelBuilder { Post = post, IsLiked = true }.Build(uniwikiContext)(random);
			new PostLikeModelBuilder { Post = post, IsLiked = true }.Build(uniwikiContext)(random);

			// Comments for the post
			new PostCommentModelBuilder { Post = post }.Build(uniwikiContext)(random);
			var comment2 = new PostCommentModelBuilder { Post = post }.Build(uniwikiContext)(random);
			var comment3 = new PostCommentModelBuilder { Post = post }.Build(uniwikiContext)(random);

			// PostComment likes
			new PostCommentLikeModelBuilder { PostComment = comment2, IsLiked = true }.Build(uniwikiContext)(random);
			new PostCommentLikeModelBuilder { PostComment = comment2, IsLiked = true }.Build(uniwikiContext)(random);
			new PostCommentLikeModelBuilder { PostComment = comment3, IsLiked = true }.Build(uniwikiContext)(random);

			// Post Files
			new PostFileModelBuilder { Post = post, IsImage = true }.Build(uniwikiContext)(random);
			new PostFileModelBuilder { Post = post, IsImage = false }.Build(uniwikiContext)(random);
			new PostFileModelBuilder { Post = post, IsImage = true }.Build(uniwikiContext)(random);

			// Act
			var viewModel =
				await PostPageAccessors.GetPostPageViewModel(post.Id, user.FirstName, user.FamilyName, user.Url,
					user.ProfileImageUrl, user.Id, currentTime)(
					uniwikiContext);

			// Assert
			Assert.AreEqual(post.Author.Url, viewModel.AuthorRoute.ProfileUrl);
			Assert.AreEqual(post.Likes.Count, viewModel.PostBarViewModel.LikesCount);
			Assert.AreEqual(post.Comments.Count, viewModel.PostBarViewModel.CommentsCount);
			Assert.AreEqual(post.PostFiles.Count, viewModel.PostFiles.Length);
			Assert.AreEqual(post.PostFiles.Count, viewModel.PostFiles.Length);

			// TODO: Test is commented by user
			// TODO: Test comment is liked by user
			// TODO: Test back route depending on where the user came from
		}

		[TestMethod]
		public async Task GetPostPageViewModel_PostIsLikedIfHasBeenLikedByTheUser()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var currentTime = new DateTime(2020, 11, 4, 3, 2, 1);
			var random = new Random(0);
			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);

			var postNotLiked = new PostModelBuilder().Build(uniwikiContext)(random);

			var postLiked = new PostModelBuilder().Build(uniwikiContext)(random);
			new PostLikeModelBuilder { Post = postLiked, Profile = user, IsLiked = true }.Build(uniwikiContext)(random);

			var postLikedDisliked = new PostModelBuilder().Build(uniwikiContext)(random);
			new PostLikeModelBuilder
				{ Post = postLikedDisliked, Profile = user, IsLiked = false }.Build(uniwikiContext)(random);

			// Act
			var viewModelPostNotLiked =
				await PostPageAccessors.GetPostPageViewModel(postNotLiked.Id, user.FirstName, user.FamilyName, user.Url,
					user.ProfileImageUrl, user.Id, currentTime)(
					uniwikiContext);

			var viewModelPostLiked =
				await PostPageAccessors.GetPostPageViewModel(postLiked.Id, user.FirstName, user.FamilyName, user.Url,
					user.ProfileImageUrl, user.Id, currentTime)(
					uniwikiContext);

			var viewModelPostLikedDisliked =
				await PostPageAccessors.GetPostPageViewModel(postLikedDisliked.Id, user.FirstName, user.FamilyName,
					user.Url,
					user.ProfileImageUrl,
					user.Id,
					currentTime)(
					uniwikiContext);

			// Assert
			Assert.IsFalse(viewModelPostNotLiked.PostBarViewModel.IsLikedByUser);
			Assert.IsTrue(viewModelPostLiked.PostBarViewModel.IsLikedByUser);
			Assert.IsFalse(viewModelPostLikedDisliked.PostBarViewModel.IsLikedByUser);
		}


		[TestMethod]
		public async Task GetPostPageViewModel_PostCommentIsLikedIfHasBeenLikedByTheUser()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var currentTime = new DateTime(2020, 11, 4, 3, 2, 1);
			var random = new Random(0);
			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);

			var post = new PostModelBuilder().Build(uniwikiContext)(random);

			var commentNotLiked = new PostCommentModelBuilder() { Post = post }.Build(uniwikiContext)(random);

			var commentLiked = new PostCommentModelBuilder() { Post = post }.Build(uniwikiContext)(random);
			new PostCommentLikeModelBuilder()
				{ PostComment = commentLiked, Profile = user, IsLiked = true }.Build(uniwikiContext)(random);

			var commentLikedDisliked = new PostCommentModelBuilder() { Post = post }.Build(uniwikiContext)(random);
			new PostCommentLikeModelBuilder() { PostComment = commentLikedDisliked, Profile = user, IsLiked = false }
				.Build(uniwikiContext)(random);


			// Act
			var viewModelPostNotLiked =
				await PostPageAccessors.GetPostPageViewModel(post.Id, user.FirstName, user.FamilyName, user.Url,
					user.ProfileImageUrl, user.Id, currentTime)(
					uniwikiContext);
			var vmCommentNotLiked = viewModelPostNotLiked.Comments.Single(c
				=> c.First().LikePostCommentRouteTrue!.PostCommentId == commentNotLiked.Id);

			var viewModelPostLiked =
				await PostPageAccessors.GetPostPageViewModel(post.Id, user.FirstName, user.FamilyName, user.Url,
					user.ProfileImageUrl, user.Id, currentTime)(
					uniwikiContext);
			var vmCommentLiked = viewModelPostLiked.Comments.Single(c
				=> c.First().LikePostCommentRouteTrue!.PostCommentId == commentLiked.Id);

			var viewModelPostLikedDisliked =
				await PostPageAccessors.GetPostPageViewModel(post.Id, user.FirstName, user.FamilyName,
					user.Url,
					user.ProfileImageUrl, user.Id, currentTime)(
					uniwikiContext);
			var vmCommentLikedDisliked = viewModelPostLikedDisliked.Comments.Single(c
				=> c.First().LikePostCommentRouteTrue!.PostCommentId == commentLikedDisliked.Id);


			// Assert
			Assert.IsFalse(vmCommentNotLiked.First().IsLikedByUser);
			Assert.IsTrue(vmCommentLiked.First().IsLikedByUser);
			Assert.IsFalse(vmCommentLikedDisliked.First().IsLikedByUser);
		}
	}
}