﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.RemovePostFile;

namespace Uniwiki.Server.WebApplication.Tests.Post.RemovePostFile
{
	[TestClass]
	public class RemovePostFileServerActionTests
	{
		[TestMethod]
		public async Task ExecuteAsync_RemovesTheFileFromThePostFiles()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var removePostFileServerAction = serviceProvider.GetRequiredService<RemovePostFileServerAction>();
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var postFile = new PostFileModelBuilder() { Profile = user }.Build(uniwikiContext)(random);
			var request = new RemovePostFileRequest(postFile.Id, user.Id, false);

			// Act
			await removePostFileServerAction.ExecuteAsync(request);

			// Assert
			Assert.IsFalse(uniwikiContext.PostFiles.Any(pf => pf.Id == postFile.Id));
		}

		[TestMethod]
		public async Task ExecuteAsync_RemovesPostFileFromTheCorrectUser()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var removePostFileServerAction = serviceProvider.GetRequiredService<RemovePostFileServerAction>();

			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var course = new CourseModelBuilder().Build(uniwikiContext)(random);
			var otherPostFile = new PostFileModelBuilder() { Course = course }.Build(uniwikiContext)(random);
			var postFile = new PostFileModelBuilder() { Profile = user, Course = course }.Build(uniwikiContext)(random);
			var request = new RemovePostFileRequest(postFile.Id, user.Id, false);

			// Act
			await removePostFileServerAction.ExecuteAsync(request);

			// Assert
			Assert.IsFalse(uniwikiContext.PostFiles.Any(pf => pf.Id == postFile.Id));
			Assert.IsTrue(uniwikiContext.PostFiles.Any(pf => pf.Id == otherPostFile.Id));
		}

		[TestMethod]
		public async Task ExecuteAsync_ThrowsNotAuthorizedExceptionIdTheUserIsNotTheOwnerOrAdmin()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var removePostFileServerAction = serviceProvider.GetRequiredService<RemovePostFileServerAction>();
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var postFile = new PostFileModelBuilder().Build(uniwikiContext)(random);
			var request = new RemovePostFileRequest(postFile.Id, user.Id, false);

			// Act
			await Assert.ThrowsExceptionAsync<NotAuthorizedException>(()
				=> removePostFileServerAction.ExecuteAsync(request));

			// Assert
			Assert.IsTrue(uniwikiContext.PostFiles.Any(pf => pf.Id == postFile.Id));
		}

		[TestMethod]
		public async Task ExecuteAsync_RemovesPostFileIfTheUserIsAdmin()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var removePostFileServerAction = serviceProvider.GetRequiredService<RemovePostFileServerAction>();
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var postFile = new PostFileModelBuilder().Build(uniwikiContext)(random);
			var request = new RemovePostFileRequest(postFile.Id, user.Id, true);

			// Act
			await removePostFileServerAction.ExecuteAsync(request);

			// Assert
			Assert.IsFalse(uniwikiContext.PostFiles.Any(pf => pf.Id == postFile.Id));
		}
	}
}