﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.RenamePostFile;

namespace Uniwiki.Server.WebApplication.Tests.Post.RenamePostFile
{
	[TestClass]
	public class RenamePostFileServerActionTests
	{
		[TestMethod]
		public async Task ExecuteAsync_RenamesTheFile()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var renamePostFileServerAction = serviceProvider.GetRequiredService<RenamePostFileServerAction>();
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var initialFileName = "Initial file name";
			var newFileName = "New file name";
			var postFile =
				new PostFileModelBuilder { Profile = user, NameWithoutExtension = initialFileName }.Build(
					uniwikiContext)(random);
			var request = new RenamePostFileRequest(postFile.Id, newFileName, user.Id, false);

			// Act
			await renamePostFileServerAction.ExecuteAsync(request);

			// Assert
			Assert.AreEqual(newFileName, uniwikiContext.PostFiles.Find(postFile.Id)?.NameWithoutExtension);
		}

		[TestMethod]
		public async Task ExecuteAsync_RenamesPostFileFromTheCorrectUser()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var renamePostFileServerAction = serviceProvider.GetRequiredService<RenamePostFileServerAction>();

			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var course = new CourseModelBuilder().Build(uniwikiContext)(random);
			var initialFileName = "Initial file name";
			var newFileName = "New file name";
			var otherFileName = "Other file name";
			var otherPostFile =
				new PostFileModelBuilder() { Course = course, NameWithoutExtension = otherFileName }.Build(
					uniwikiContext)(random);
			var postFile = new PostFileModelBuilder()
					{ Profile = user, Course = course, NameWithoutExtension = initialFileName }
				.Build(uniwikiContext)(random);
			var request = new RenamePostFileRequest(postFile.Id, newFileName, user.Id, false);

			// Act
			await renamePostFileServerAction.ExecuteAsync(request);

			// Assert
			Assert.AreEqual(newFileName, uniwikiContext.PostFiles.Find(postFile.Id)?.NameWithoutExtension);
			Assert.AreEqual(otherFileName, uniwikiContext.PostFiles.Find(otherPostFile.Id)?.NameWithoutExtension);
		}

		[TestMethod]
		public async Task ExecuteAsync_ThrowsNotAuthorizedExceptionIdTheUserIsNotTheOwnerOrAdmin()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var renamePostFileServerAction = serviceProvider.GetRequiredService<RenamePostFileServerAction>();
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var initialFileName = "Initial file name";
			var newFileName = "New file name";
			var postFile =
				new PostFileModelBuilder { NameWithoutExtension = initialFileName }.Build(
					uniwikiContext)(random);
			var request = new RenamePostFileRequest(postFile.Id, newFileName, user.Id, false);

			// Act
			await Assert.ThrowsExceptionAsync<NotAuthorizedException>(()
				=> renamePostFileServerAction.ExecuteAsync(request));

			// Assert
			Assert.AreEqual(initialFileName, uniwikiContext.PostFiles.Find(postFile.Id)?.NameWithoutExtension);
		}

		[TestMethod]
		public async Task ExecuteAsync_RenamesPostFileIfTheUserIsNotTheOwnerButIsAdmin()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var renamePostFileServerAction = serviceProvider.GetRequiredService<RenamePostFileServerAction>();
			var random = new Random(0);

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var initialFileName = "Initial file name";
			var newFileName = "New file name";
			var postFile =
				new PostFileModelBuilder { NameWithoutExtension = initialFileName }.Build(
					uniwikiContext)(random);
			var request = new RenamePostFileRequest(postFile.Id, newFileName, user.Id, true);

			// Act
			await renamePostFileServerAction.ExecuteAsync(request);

			// Assert
			Assert.AreEqual(newFileName, uniwikiContext.PostFiles.Find(postFile.Id)?.NameWithoutExtension);
		}
	}
}