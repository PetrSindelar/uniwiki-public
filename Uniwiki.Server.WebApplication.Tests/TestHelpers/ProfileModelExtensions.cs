﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;

namespace Uniwiki.Server.WebApplication.Tests.TestHelpers
{
	public static class ProfileModelExtensions
	{
		public static UserInfo ToUserInfo(this ProfileModel profileModel, bool isAdmin = false)
			=> new UserInfo(
				profileModel.FirstName,
				profileModel.FamilyName,
				new ProfilePageRoute(profileModel.Url).Build(),
				profileModel.Url,
				profileModel.ProfileImageUrl == null
					? null
					: new ProfileImageRoute(profileModel.ProfileImageUrl).Build(),
				profileModel.ProfileImageUrl, isAdmin, // TODO: Add the admins
				profileModel.Id
			);
	}
}