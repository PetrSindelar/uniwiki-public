﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Uniwiki.Server.Persistence;

namespace Uniwiki.Server.WebApplication.Tests
{
	public static class TestSetup
	{
		public static IServiceProvider GetServiceProvider()
		{
			var serviceProvider = new ServiceCollection()
				.AddUniwikiServerWebApplicationServices(
					builder =>
						builder.UseSqlServer(
							"Data Source=PSINDELAR01\\SQLEXPRESS01;" +
							"Initial Catalog=Uniwiki_Test;" +
							"Integrated Security=True"
						)
				)
				.BuildServiceProvider();

			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			uniwikiContext.Database.EnsureDeleted();
			uniwikiContext.Database.EnsureCreated();

			return serviceProvider;
		}
	}
}