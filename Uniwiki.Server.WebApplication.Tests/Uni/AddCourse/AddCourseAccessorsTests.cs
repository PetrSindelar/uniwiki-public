using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.WebApplication.Uni.AddCourse;

namespace Uniwiki.Server.WebApplication.Tests.Uni.AddCourse
{
	[TestClass]
	public class AddCourseAccessorsTests
	{
		[TestMethod]
		public async Task GetFacultyId_ReturnsTheRightIdOfAFaculty()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var random = new Random(0);
			var university = new UniversityModelBuilder().Build(uniwikiContext)(random);

			var faculty1 = new FacultyModelBuilder { University = university }.Build(uniwikiContext)(random);
			var faculty2 = new FacultyModelBuilder { University = university }.Build(uniwikiContext)(random);
			var faculty3 = new FacultyModelBuilder().Build(uniwikiContext)(random);

			// Act
			var id1 = await AddCourseAccessors.GetFacultyId(faculty1.University.Url, faculty1.Url)(uniwikiContext);
			var id2 = await AddCourseAccessors.GetFacultyId(faculty2.University.Url, faculty2.Url)(uniwikiContext);
			var id3 = await AddCourseAccessors.GetFacultyId(faculty3.University.Url, faculty3.Url)(uniwikiContext);

			// Assert
			Assert.AreEqual(id1, faculty1.Id);
			Assert.AreEqual(id2, faculty2.Id);
			Assert.AreEqual(id3, faculty3.Id);
		}
	}
}