﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.Uni.AddCourse;

namespace Uniwiki.Server.WebApplication.Tests.Uni.AddCourse
{
	[TestClass]
	public class AddCourseServerActionTests : TestBase
	{
		[TestMethod]
		public async Task Execute_AddsTheCourseToTheDb()
		{
			// Arrange
			var random = new Random(0);
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var university = new UniversityModelBuilder().Build(uniwikiContext)(random);
			var serverAction = serviceProvider.GetRequiredService<AddCourseServerAction>();

			var user = new ProfileModelBuilder().Build(uniwikiContext)(random);
			var faculty =
				new FacultyModelBuilder { University = university }.Build(uniwikiContext)(random);
			var courseName =
				Standardizers.StandardizeName(Generators.RandomName(Constants.Validations.CourseNameMaxLength)(random));
			var courseCode =
				Generators.RandomName(Constants.Validations.CourseCodeMaxLength)(random);
			var request = new AddCourseRequest(faculty.University.Url, faculty.Url, courseName, courseCode, user.Id);

			// Act
			uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var viewModel = await serverAction.ExecuteAsync(request);
			var course = uniwikiContext
				.Courses
				.Single(c => c.FacultyId == faculty.Id && c.Url == viewModel.CourseUrl);

			// Assert
			Assert.IsNotNull(course);
			Assert.AreEqual(courseName, course.LongName);
			Assert.AreEqual(courseCode, course.Code);
		}
	}
}