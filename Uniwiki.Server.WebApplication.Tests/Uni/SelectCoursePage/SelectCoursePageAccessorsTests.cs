﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Tests.ModelBuilders;
using Uniwiki.Server.Shared.Tests.Extensions;
using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;

namespace Uniwiki.Server.WebApplication.Tests.Uni.SelectCoursePage
{
	[TestClass]
	public class SelectCoursePageAccessorsTests
	{
		[TestMethod]
		public async Task GetViewModel_DoesNotThrowErrors()
		{
			// Arrange
			var serviceProvider = TestSetup.GetServiceProvider();
			var uniwikiContext = serviceProvider.GetRequiredService<UniwikiContext>();
			var random = new Random(0);
			new UniversityModelBuilder().Build(uniwikiContext)(random);
			var university = new UniversityModelBuilder().Build(uniwikiContext)(random);
			new UniversityModelBuilder().Build(uniwikiContext)(random);

			new FacultyModelBuilder { University = university }.Build(uniwikiContext)(random);
			new FacultyModelBuilder().Build(uniwikiContext)(random);
			var faculty = new FacultyModelBuilder { University = university }.Build(uniwikiContext)(random);
			new FacultyModelBuilder { University = university }.Build(uniwikiContext)(random);

			var course1 = new CourseModelBuilder { Faculty = faculty }.Build(uniwikiContext)(random);
			var course2 = new CourseModelBuilder { Faculty = faculty }.Build(uniwikiContext)(random);
			var course3 = new CourseModelBuilder { Faculty = faculty }.Build(uniwikiContext)(random);
			var courses = new[] { course1, course2, course3 };

			new CourseModelBuilder().Build(uniwikiContext)(random);
			new CourseModelBuilder().Build(uniwikiContext)(random);

			var showTip = false;

			// Act
			var viewModel =
				await SelectCoursePageAccessors.GetViewModel(course1.Faculty.University.Url, course1.Faculty.Url,
					showTip)(
					uniwikiContext);

			// Assert
			viewModel
				.Courses
				.CompareItems(
					courses,
					(cvmi, cm) => cvmi.CourseRoute.CourseUrl == cm.Url &&
					              cvmi.CourseRoute.FacultyUrl == cm.Faculty.Url &&
					              cvmi.CourseRoute.UniversityUrl == cm.Faculty.University.Url,
					(cvmi, cm) =>
					{
						Assert.AreEqual(cm.LongName, cvmi.LongName);
						Assert.AreEqual(cm.Code, cvmi.Code);
					}
				);
		}
	}
}