﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	public static class AddFacultyAccessors
	{
		public static Func<UniwikiContext, Task<Guid>> GetUniversityId(string universityUrl)
			=> async uniwikiContext
				=> (await uniwikiContext
					.Universities
					.Select(u => new { u.Id, u.Url })
					.FirstAsync(u => u.Url == universityUrl)).Id;
	}
}