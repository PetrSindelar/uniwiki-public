﻿using System;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	public class AddFacultyRequest
	{
		public Guid UniversityId { get; }
		public string FacultyLongName { get; }
		public string FacultyShortName { get; }
		public string FacultyUrl { get; }
		public Language PrimaryLanguage { get; }

		public AddFacultyRequest(Guid universityId, string facultyLongName, string facultyShortName,
			string facultyUrl,
			Language primaryLanguage)
		{
			UniversityId = universityId;
			FacultyLongName = facultyLongName;
			FacultyShortName = facultyShortName;
			FacultyUrl = facultyUrl;
			PrimaryLanguage = primaryLanguage;
		}
	}
}