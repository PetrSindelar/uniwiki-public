﻿using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	public class AddFacultyResponse
	{
		public SelectCoursePageRoute RedirectRoute { get; }

		public AddFacultyResponse(string universityUrl, string facultyUrl)
		{
			RedirectRoute = new SelectCoursePageRoute(universityUrl, facultyUrl);
		}
	}
}