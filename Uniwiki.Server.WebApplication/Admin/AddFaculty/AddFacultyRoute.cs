﻿using Uniwiki.Server.Shared;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	public class AddFacultyRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/AddFaculty";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => $"{BaseRoute}";
	}
}