﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	[ScopedService]
	public class AddFacultyServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddFacultyServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<AddFacultyResponse> ExecuteAsync(AddFacultyRequest request)
		{
			request = AddFacultyStandardizer.Standardize(request);

			AddFacultyValidator.Validate(request);

			// Create a DB model
			var facultyModel =
				new FacultyModel(Guid.NewGuid(), request.UniversityId, request.FacultyShortName,
					request.FacultyLongName,
					request.FacultyUrl,
					request.PrimaryLanguage,
					request.FacultyUrl);

			// Add university to the DB
			await _uniwikiContext.Faculties.AddAsync(facultyModel);

			// Save the changes
			await _uniwikiContext.SaveChangesAsync();

			// Get the university url
			var universityUrl = await _uniwikiContext
				.Universities
				.AsNoTracking()
				.Where(uni => uni.Id == request.UniversityId)
				.Select(u => u.Url)
				.FirstAsync();

			var viewModel = new AddFacultyResponse(universityUrl, request.FacultyUrl);

			return viewModel;
		}
	}
}