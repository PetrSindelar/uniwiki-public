﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	public static class AddFacultyStandardizer
	{
		public static AddFacultyRequest Standardize(AddFacultyRequest request)
			=> new AddFacultyRequest(
				request.UniversityId,
				request.FacultyLongName.Apply(Standardizers.OptimizeWhiteSpaces).Apply(Standardizers.FirstCharToUpper),
				request.FacultyShortName.Apply(Standardizers.OptimizeWhiteSpaces).Apply(Standardizers.FirstCharToUpper),
				request.FacultyUrl.Apply(Standardizers.StandardizeUrl),
				request.PrimaryLanguage
			);
	}
}