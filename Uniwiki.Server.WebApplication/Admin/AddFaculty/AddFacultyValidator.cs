﻿using System;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Admin.AddFaculty
{
	public static class AddFacultyValidator
	{
		public static void Validate(AddFacultyRequest request)
		{
			if (request.FacultyLongName.Apply(string.IsNullOrWhiteSpace) ||
			    request.FacultyShortName.Apply(string.IsNullOrWhiteSpace) ||
			    request.FacultyUrl.Apply(string.IsNullOrWhiteSpace) ||
			    request.PrimaryLanguage.Apply(Validators.IsEnumInvalid)
			)
			{
				// TODO: Change for the logger
				Console.WriteLine(request.PropertiesToString());
				throw new RequestException("One of the inputs is in a bad format.");
			}
		}
	}
}