﻿using System;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.WebApplication.Admin.AddFacultyPage
{
	public class AddFacultyPageForm
	{
		public Guid UniversityId { get; set; }

		public string FacultyLongName { get; set; } = null!;

		public string FacultyShortName { get; set; } = null!;

		public string FacultyUrl { get; set; } = null!;

		public Language PrimaryLanguage { get; set; }
	}
}