﻿using Uniwiki.Server.WebApplication.Post.EditPostPage;

namespace Uniwiki.Server.WebApplication.Admin.AddFacultyPage
{
	public record AddFacultyPageRequest
	{
		public string UniversityUrl { get; }

		public AddFacultyPageRequest(string universityUrl)
		{
			UniversityUrl = universityUrl;
		}
	}
}