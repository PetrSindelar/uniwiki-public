﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Admin.AddFacultyPage
{
	public class AddFacultyPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/AddFaculty";

		public const string UniversityUrlRouteParameter = "UniversityUrl";
		public const string RouteAttribute = BaseRoute + "/{" + UniversityUrlRouteParameter + "}";

		public string UniversityUrl { get; }

		public AddFacultyPageRoute(string universityUrl)
		{
			UniversityUrl = universityUrl;
		}

		public override string Build() => BaseRoute + "/" + UniversityUrl;
	}
}