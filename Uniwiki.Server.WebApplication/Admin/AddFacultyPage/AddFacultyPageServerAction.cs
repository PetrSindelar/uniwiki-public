﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.Admin.AddFacultyPage
{
	[ScopedService]
	public class AddFacultyPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddFacultyPageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<AddFacultyPageViewModel> ExecuteAsync(AddFacultyPageRequest request)
		{
			var universityId = await _uniwikiContext.Universities.Where(u => u.Url == request.UniversityUrl)
				.Select(u => u.Id)
				.FirstOrDefaultAsync();

			if (universityId == default)
			{
				throw new NotFoundException("University not found.");
			}

			var viewModel = new AddFacultyPageViewModel(universityId);

			return viewModel;
		}
	}
}