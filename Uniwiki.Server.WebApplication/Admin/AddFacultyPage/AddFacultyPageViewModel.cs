﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Uniwiki.Server.Shared;
using Uniwiki.Server.WebApplication.Admin.AddFaculty;
using Uniwiki.Server.WebApplication.Admin.AdminDashboardPage;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Admin.AddFacultyPage
{
	public class AddFacultyPageViewModel : PageLayoutViewModel
	{
		// TODO: Handle the languages in a better way
		private static readonly SelectListItem[] AllFacultyLanguages = new SelectListItem[]
		{
			new SelectListItem("Czech", ((int) Language.Czech).ToString()),
			new SelectListItem("English", ((int) Language.English).ToString()),
		};

		public AddFacultyRoute AddFacultyRoute { get; }
		public AddFacultyPageForm AddFacultyPageForm { get; }
		public IEnumerable<SelectListItem> Languages => AllFacultyLanguages;

		public AddFacultyPageViewModel(Guid universityId) : base(new AdminDashboardPageRoute(), "Add faculty")
		{
			AddFacultyRoute = new();
			AddFacultyPageForm = new AddFacultyPageForm() { UniversityId = universityId };
		}
	}
}