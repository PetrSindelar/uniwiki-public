﻿namespace Uniwiki.Server.WebApplication.Admin.AddUniversity
{
	public class AddUniversityRequest
	{
		public string UniversityLongName { get; }
		public string UniversityShortName { get; }
		public string UniversityUrl { get; }

		public AddUniversityRequest(string universityLongName, string universityShortName,
			string universityUrl)
		{
			UniversityLongName = universityLongName;
			UniversityShortName = universityShortName;
			UniversityUrl = universityUrl;
		}
	}
}