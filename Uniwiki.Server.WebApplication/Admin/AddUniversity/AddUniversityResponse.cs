﻿using System;
using Uniwiki.Server.WebApplication.Uni.SelectFacultyPage;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversity
{
	public class AddUniversityResponse
	{
		public SelectFacultyPageRoute RedirectRoute { get; }
		public Guid UniversityId { get; }

		public AddUniversityResponse(string universityUrl, Guid universityId)
		{
			RedirectRoute = new SelectFacultyPageRoute(universityUrl);
			UniversityId = universityId;
		}
	}
}