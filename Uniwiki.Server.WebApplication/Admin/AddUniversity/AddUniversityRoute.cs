﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversity
{
	public class AddUniversityRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/AddUniversity";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => BaseRoute;
	}
}