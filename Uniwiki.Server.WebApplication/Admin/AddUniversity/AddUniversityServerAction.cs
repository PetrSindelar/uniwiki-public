﻿using System;
using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversity
{
	[ScopedService]
	public class AddUniversityServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddUniversityServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		/// <returns>The new university ID</returns>
		public async Task<AddUniversityResponse> ExecuteAsync(AddUniversityRequest request)
		{
			// Standardize inputs
			request = AddUniversityStandardizer.Standardize(request);

			// Validate inputs
			AddUniversityValidator.Validate(request);

			// Create a DB model
			var universityModel =
				new UniversityModel(Guid.NewGuid(), request.UniversityLongName, request.UniversityShortName,
					request.UniversityUrl, request.UniversityUrl);

			// Add university to the DB
			await _uniwikiContext.Universities.AddAsync(universityModel);

			await _uniwikiContext.SaveChangesAsync();

			var viewModel = new AddUniversityResponse(request.UniversityUrl, universityModel.Id);

			return viewModel;
		}
	}
}