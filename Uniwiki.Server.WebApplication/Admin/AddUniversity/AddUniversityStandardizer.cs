﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversity
{
	public static class AddUniversityStandardizer
	{
		public static AddUniversityRequest Standardize(AddUniversityRequest request)
			=> new AddUniversityRequest(
				request.UniversityLongName
					.Apply(Standardizers.OptimizeWhiteSpaces)
					.Apply(Standardizers.FirstCharToUpper),
				request.UniversityShortName
					.Apply(Standardizers.OptimizeWhiteSpaces)
					.Apply(Standardizers.FirstCharToUpper),
				request.UniversityUrl
					.Apply(Standardizers.StandardizeUrl)
			);
	}
}