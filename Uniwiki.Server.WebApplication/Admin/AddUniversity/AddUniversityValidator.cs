﻿using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversity
{
	public static class AddUniversityValidator
	{
		public static void Validate(AddUniversityRequest request)
		{
			if (string.IsNullOrWhiteSpace(request.UniversityLongName) ||
			    string.IsNullOrWhiteSpace(request.UniversityShortName) ||
			    string.IsNullOrWhiteSpace(request.UniversityUrl))
				throw new RequestException("One of the inputs is in a bad format.");
		}
	}
}