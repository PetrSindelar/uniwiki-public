﻿namespace Uniwiki.Server.WebApplication.Admin.AddUniversityPage
{
	public class AddUniversityPageForm
	{
		public string LongName { get; set; } = null!;
		public string ShortName { get; set; } = null!;
		public string Url { get; set; } = null!;
	}
}