﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversityPage
{
	public class AddUniversityPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/AddUniversity";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => BaseRoute;
	}
}