﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversityPage
{
	[ScopedService]
	public class AddUniversityPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddUniversityPageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public Task<AddUniversityPageViewModel> ExecuteAsync(AddUniversityPageRequest request)
		{
			var viewModel = new AddUniversityPageViewModel();

			return Task.FromResult(viewModel);
		}
	}
}