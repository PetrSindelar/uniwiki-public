﻿using Uniwiki.Server.WebApplication.Admin.AddUniversity;
using Uniwiki.Server.WebApplication.Admin.AdminDashboardPage;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Admin.AddUniversityPage
{
	public class AddUniversityPageViewModel : PageLayoutViewModel
	{
		public AddUniversityPageForm AddUniversityPageForm { get; }
		public AddUniversityRoute AddUniversityRoute { get; }

		public AddUniversityPageViewModel() : base(new AdminDashboardPageRoute(), "Add University")
		{
			AddUniversityPageForm = new();
			AddUniversityRoute = new AddUniversityRoute();
		}
	}
}