﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Admin.AdminDashboardPage
{
	public class AdminDashboardPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Admin/Dashboard";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => BaseRoute + "/";
	}
}