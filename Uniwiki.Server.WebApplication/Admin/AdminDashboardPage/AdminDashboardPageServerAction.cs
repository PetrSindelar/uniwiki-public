﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Admin.AdminDashboardPage
{
	[ScopedService]
	public class AdminDashboardPageServerAction
	{
		private readonly UniwikiContext _uniwikiService;

		public AdminDashboardPageServerAction(UniwikiContext uniwikiService)
		{
			_uniwikiService = uniwikiService;
		}

		public async Task<AdminDashboardPageViewModel> ExecuteAsync(AdminDashboardPageRequest request)
		{
			var universities =
				(await _uniwikiService.Universities.Select(u => new { u.Url, u.LongName }).ToArrayAsync())
				.Select(university => (university.LongName, university.Url)).ToArray();

			var usersCount = await _uniwikiService.Users.CountAsync();

			var viewModel = new AdminDashboardPageViewModel(universities, usersCount);

			return viewModel;
		}
	}
}