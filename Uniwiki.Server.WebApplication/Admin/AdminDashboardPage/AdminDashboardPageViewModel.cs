﻿using System.Linq;
using Uniwiki.Server.WebApplication.Admin.AddFacultyPage;
using Uniwiki.Server.WebApplication.Admin.AddUniversityPage;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Admin.AdminDashboardPage
{
	public class AdminDashboardPageViewModel : PageLayoutViewModel
	{
		public int UsersCount { get; }
		public AddUniversityPageRoute AddUniversityPageRoute { get; }
		public (string UniversityLongName, AddFacultyPageRoute AddFacultyPageRoute)[] AddFacultyLinks { get; }

		public AdminDashboardPageViewModel((string UniversityLongName, string UniversityUrl)[] universities,
			int usersCount) : base(
			null,
			"Admin Dashboard")
		{
			UsersCount = usersCount;
			AddUniversityPageRoute = new AddUniversityPageRoute();
			AddFacultyLinks = universities.Select(u => (u.UniversityLongName, new AddFacultyPageRoute(u.UniversityUrl)))
				.ToArray();
		}
	}
}