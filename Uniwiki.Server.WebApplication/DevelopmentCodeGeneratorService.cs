﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Uniwiki.Server.WebApplication.User.Shared.Services;

namespace Uniwiki.Server.WebApplication
{
	/// <summary>
	/// FOR DEVELOPMENT PURPOSES ONLY! - makes it easy to fill in the confirmation codes. It generates codes containing just '1' x-times
	/// </summary>
	public class DevelopmentCodeGeneratorService : ICodeGeneratorService
	{
		public DevelopmentCodeGeneratorService(IWebHostEnvironment webHostEnvironment)
		{
			if (!webHostEnvironment.IsDevelopment())
			{
				throw new ApplicationException("This should be used only in the development mode.");
			}
		}

		// Generates codes containing just the character '1' so for example it generates code: '1111'
		public string GenerateVerificationCode(int codeLength) => "".PadLeft(codeLength, '1');
	}
}