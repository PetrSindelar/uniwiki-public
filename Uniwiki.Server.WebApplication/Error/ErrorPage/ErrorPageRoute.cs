﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Error.ErrorPage
{
	public class ErrorPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Error";
		public const string CodeRouteParameter = "Code";
		public const string RouteAttribute = BaseRoute + "/{" + CodeRouteParameter + "}";
		public int Code { get; }

		public ErrorPageRoute(int code)
		{
			Code = code;
		}

		public override string Build() => BaseRoute + "/" + Code;
		public string BuildFormat() => BaseRoute + "/{0}";
	}
}