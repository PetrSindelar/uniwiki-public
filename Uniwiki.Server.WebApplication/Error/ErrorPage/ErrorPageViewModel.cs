﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniwiki.Server.WebApplication.Shared.Routes;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Error.ErrorPage
{
	public class ErrorPageViewModel : PageLayoutViewModel
	{
		public string Message { get; }

		public ErrorPageViewModel(string message, string title) : base(null, title)
		{
			Message = message;
		}
	}
}