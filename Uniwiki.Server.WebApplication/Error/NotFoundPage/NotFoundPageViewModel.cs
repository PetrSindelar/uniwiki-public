﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Error.NotFoundPage
{
	public class NotFoundPageViewModel : PageLayoutViewModel
	{
		public string Message { get; }

		public NotFoundPageViewModel(string message)
			: base(null, "Not found")
		{
			Message = message;
		}
	}
}