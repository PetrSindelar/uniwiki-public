﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Post.AddOrEditPostPage
{
	public static class AddOrEditPostPageAccessors
	{
		private static readonly string?[] DefaultCategories = { null, "Exam", "Homework", "Study material", "Test" };

		public static Func<UniwikiContext, Task<(string? CategoryName, int CategoryPostsCount)[]>>
			GetCourseCategoriesForPost(Guid courseId)
		{
			return async uniwikiContext => (await uniwikiContext.Posts
					.Where(p => p.CourseId == courseId)
					.GroupBy(p => p.Category)
					.Select(g => new { CategoryName = g.Key, CategoryPostsCount = g.Count() })
					.ToArrayAsync())
				// ReSharper disable once RedundantCast - without this cast there is an error in the console
				.Apply(categories => categories.Select(c => ((string?) c.CategoryName, c.CategoryPostsCount))
					.ToArray())
				// Add default categories
				.Apply(categories => categories
					.Concat(DefaultCategories
						.Where(dc => categories
							.All(c => c.Item1 != dc))
						.Select(c => (c, 0))
						.ToArray())
					.OrderBy(c => c.Item1)
					.ToArray());
		}
	}
}