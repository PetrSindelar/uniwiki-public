﻿using System;
using Uniwiki.Server.WebApplication.Post.AddPost;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;
using Uniwiki.Server.WebApplication.Shared.Routes;
using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.CoursePage;

namespace Uniwiki.Server.WebApplication.Post.AddOrEditPostPage
{
	public abstract class AddOrEditPostPageViewModel : PageLayoutViewModel
	{
		public PostFileComponentViewModel[] PostFileComponentViewModels { get; }
		public SelectCategoryModalViewModel SelectCategoryModalViewModel { get; }
		public UploadingFilesViewModel UploadingFilesViewModel { get; }
		public string ProfileFullName { get; }
		public RouteBase AddOrEditPostRoute { get; }
		public ProfileImageRoute? ProfileImageRoute { get; }
		public ProfilePageRoute ProfilePageRoute { get; }
		public AddOrEditPostForm AddOrEditPostForm { get; }
		public PostAnonymouslyModalViewModel PostAnonymouslyModalViewModel { get; }
		public string UploadedFilesContainerId { get; }

		protected AddOrEditPostPageViewModel(string universityUrl,
			string facultyUrl, string courseUrl,
			string profileFirstName, string profileFamilyName, string profileUrl, string? profileImageUrl,
			(string? CategoryName, int PostsCount)[] categories, Guid courseId,
			PostFileComponentViewModel[] postFileComponentViewModels, string postDescription, string? postCategory,
			bool postIsAnonymous, RouteBase addOrEditPostRoute, string title)
			: base(
				new CoursePageRoute(universityUrl, facultyUrl, courseUrl),
				title
			)
		{
			ProfileFullName = profileFirstName + " " + profileFamilyName;
			PostFileComponentViewModels = postFileComponentViewModels;
			SelectCategoryModalViewModel = new(postCategory, categories);
			UploadingFilesViewModel = new(courseId, null);
			PostAnonymouslyModalViewModel = new("What is an anonymous post?", "postAnonymouslyModal");
			AddOrEditPostRoute = addOrEditPostRoute;
			ProfileImageRoute = profileImageUrl == null ? null : new(profileImageUrl);
			ProfilePageRoute = new(profileUrl);
			AddOrEditPostForm = new(postDescription, postCategory, postIsAnonymous);
			UploadedFilesContainerId = "uploadedFilesContainer";
		}
	}
}