﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Post.AddOrEditPostPage
{
	public class PostAnonymouslyModalViewModel : ModalViewModel
	{
		public PostAnonymouslyModalViewModel(string title, string modalId) : base(title, modalId)
		{
		}
	}
}