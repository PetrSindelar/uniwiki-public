﻿namespace Uniwiki.Server.WebApplication.Post.AddPost
{
	public class AddOrEditPostForm
	{
		public string Description { get; set; } = string.Empty;
		public string? Category { get; set; }
		public bool PostIsAnonymous { get; set; }

		// Default for creating from the controllers
		public AddOrEditPostForm()
		{
		}

		public AddOrEditPostForm(string description, string? category, bool postIsAnonymous)
		{
			Description = description;
			Category = category;
			PostIsAnonymous = postIsAnonymous;
		}
	}
}