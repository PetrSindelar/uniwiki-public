﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;

namespace Uniwiki.Server.WebApplication.Post.AddPost
{
	public static class AddPostAccessors
	{
		public static Func<UniwikiContext, Task<Guid>> GetCourseIdFromUrl(string universityUrl, string facultyUrl,
			string courseUrl)
			=> uniwikiContext => uniwikiContext.Courses
				.AsNoTracking()
				.Include(c => c.Faculty)
				.ThenInclude(c => c.University)
				.Where(c => c.Faculty.University.Url == universityUrl &&
				            c.Faculty.Url == facultyUrl &&
				            c.Url == courseUrl)
				.Select(c => c.Id)
				.FirstAsync();
	}
}