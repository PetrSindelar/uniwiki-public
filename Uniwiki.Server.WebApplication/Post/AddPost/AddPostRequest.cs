﻿using System;

namespace Uniwiki.Server.WebApplication.Post.AddPost
{
	public class AddPostRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string? PostCategory { get; }
		public string PostDescription { get; }
		public bool PostIsAnonymous { get; }
		public Guid UserId { get; }

		public AddPostRequest(string universityUrl, string facultyUrl, string courseUrl, string? postCategory,
			string postDescription, bool postIsAnonymous, Guid userId)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostCategory = postCategory;
			PostDescription = postDescription;
			PostIsAnonymous = postIsAnonymous;
			UserId = userId;
		}
	}
}