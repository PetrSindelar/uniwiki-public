﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Post.AddPost
{
	[ScopedService]
	public class AddPostServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public AddPostServerAction(UniwikiContext uniwikiContext, ITimeService timeService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task<AddPostViewModel> ExecuteAsync(AddPostRequest request)
		{
			// Standardize
			request = AddPostStandardizer.Standardize(request);

			// TODO: Validate the request

			var courseId =
				await AddPostAccessors.GetCourseIdFromUrl(request.UniversityUrl, request.FacultyUrl, request.CourseUrl)(
					_uniwikiContext);

			var postCreationDateTime = _timeService.Now;

			var postId = Guid.NewGuid();

			// This might seem like there could be collisions, but the chance is way too low..
			var postUrl = postId.ToString().Substring(0, 8);

			var post =
				new PostModel(postId,
					request.PostCategory,
					request.UserId,
					request.PostDescription,
					courseId,
					postCreationDateTime,
					postUrl,
					request.PostIsAnonymous);

			_uniwikiContext.Posts.Add(post);

			// Link all the post files to the post
			var postFilesToLink = await _uniwikiContext
				.PostFiles
				.Where(pf => pf.CourseId == courseId && pf.PostId == null && pf.ProfileId == request.UserId)
				.ToListAsync();

			postFilesToLink.ForEach(pf => pf.PostId = postId);

			await _uniwikiContext.SaveChangesAsync();

			var viewModel =
				new AddPostViewModel(request.UniversityUrl, request.FacultyUrl, request.CourseUrl, post.Url);

			return viewModel;
		}
	}
}