﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Post.AddPost
{
	public static class AddPostStandardizer
	{
		public static AddPostRequest Standardize(AddPostRequest request)
			=> new(
				request.UniversityUrl.Apply(Standardizers.StandardizeUrl),
				request.FacultyUrl.Apply(Standardizers.StandardizeUrl),
				request.CourseUrl.Apply(Standardizers.StandardizeUrl),
				string.IsNullOrWhiteSpace(request.PostCategory) ? null : request.PostCategory,
				Standardizers.FirstCharToUpper(request.PostDescription.Trim()),
				request.PostIsAnonymous,
				request.UserId
			);
	}
}