﻿using Uniwiki.Server.WebApplication.Uni.PostPage;

namespace Uniwiki.Server.WebApplication.Post.AddPost
{
	public class AddPostViewModel
	{
		public PostPageRoute RedirectRoute { get; }

		public AddPostViewModel(
			string universityUrl,
			string facultyUrl,
			string courseUrl,
			string postUrl)
		{
			RedirectRoute = new(universityUrl, facultyUrl, courseUrl, postUrl);
		}
	}
}