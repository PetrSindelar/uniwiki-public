﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class AddPostPageAccessors
	{
		public static Func<UniwikiContext, Task<(string CourseName, Guid CourseId)>> GetCourseNameAndIdFromUrl(
			string universityUrl, string facultyUrl,
			string courseUrl)
			=> async uniwikiContext => (await uniwikiContext.Courses
					.Include(c => c.Faculty)
					.ThenInclude(c => c.University)
					.Where(c => c.Faculty.University.Url == universityUrl &&
					            c.Faculty.Url == facultyUrl &&
					            c.Url == courseUrl)
					.Select(c => new { c.LongName, c.Id })
					.FirstAsync())
				.Apply(c => (c.LongName, c.Id));
	}
}