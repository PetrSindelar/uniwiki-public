﻿namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class AddPostPageRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }

		public AddPostPageRequest(string universityUrl, string facultyUrl, string courseUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
		}
	}
}