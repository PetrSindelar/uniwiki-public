﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class AddPostPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/AddPost";
		public const string UniversityRouteParameter = "universityUrl";
		public const string FacultyRouteParameter = "facultyUrl";
		public const string CourseRouteParameter = "courseUrl";

		public const string RouteAttribute =
			BaseRoute + "/{" + UniversityRouteParameter + "}/{" + FacultyRouteParameter + "}/{" + CourseRouteParameter +
			"}";

		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }

		public AddPostPageRoute(string universityUrl, string facultyUrl, string courseUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
		}

		public override string Build() => $"{BaseRoute}/{UniversityUrl}/{FacultyUrl}/{CourseUrl}";
	}
}