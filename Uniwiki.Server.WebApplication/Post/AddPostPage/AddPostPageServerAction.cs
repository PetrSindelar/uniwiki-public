﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Post.AddOrEditPostPage;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	[ScopedService]
	public class AddPostPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddPostPageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<AddPostPageViewModel> ExecuteAsync(AddPostPageRequest request)
		{
			// Standardize
			request = AddPostPageStandardizer.Standardize(request);

			var (courseName, courseId) = await AddPostPageAccessors.GetCourseNameAndIdFromUrl(
				request.UniversityUrl,
				request.FacultyUrl,
				request.CourseUrl)(_uniwikiContext);

			var categories =
				await AddOrEditPostPageAccessors.GetCourseCategoriesForPost(courseId)(_uniwikiContext);

			// Get the files for the post
			var postFileComponentViewModels = _uniwikiContext
				.PostFiles
				.Where(pf => pf.CourseId == courseId && pf.Post == null)
				.Select(
					p => new PostFileComponentViewModel(p.Url, p.NameWithoutExtension, p.Extension, p.IsImage, p.Id))
				.ToArray();

			var user = await _uniwikiContext.Users.FirstAsync();

			var viewModel = new AddPostPageViewModel(
				request.UniversityUrl,
				request.FacultyUrl,
				request.CourseUrl,
				courseName,
				user.FirstName,
				user.FamilyName,
				user.Url,
				user.ProfileImageUrl,
				categories,
				courseId,
				postFileComponentViewModels
			);

			return viewModel;
		}
	}
}