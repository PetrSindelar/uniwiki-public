﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public static class AddPostPageStandardizer
	{
		public static AddPostPageRequest Standardize(AddPostPageRequest request)
			=> new AddPostPageRequest(
				request.UniversityUrl.Apply(Standardizers.StandardizeUrl),
				request.FacultyUrl.Apply(Standardizers.StandardizeUrl),
				request.CourseUrl.Apply(Standardizers.StandardizeUrl)
			);
	}
}