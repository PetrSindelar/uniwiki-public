﻿using System;
using Uniwiki.Server.WebApplication.Post.AddOrEditPostPage;
using Uniwiki.Server.WebApplication.Post.AddPost;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class AddPostPageViewModel : AddOrEditPostPageViewModel
	{
		public AddPostPageViewModel(string universityUrl,
			string facultyUrl, string courseUrl, string courseLongName,
			string profileFirstName, string profileFamilyName, string profileUrl, string? profileImageUrl,
			(string? CategoryName, int PostsCount)[] categories, Guid courseId,
			PostFileComponentViewModel[] postFileComponentViewModels)
			: base(universityUrl, facultyUrl, courseUrl, profileFirstName, profileFamilyName,
				profileUrl,
				profileImageUrl, categories, courseId, postFileComponentViewModels, string.Empty, null, false,
				new AddPostRoute(universityUrl, facultyUrl, courseUrl),
				"Add a post to " + courseLongName
			)
		{
		}
	}
}