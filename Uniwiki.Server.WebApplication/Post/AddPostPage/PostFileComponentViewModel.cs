﻿using System;
using Uniwiki.Server.WebApplication.Post.RemovePostFile;
using Uniwiki.Server.WebApplication.Post.RenamePostFile;
using Uniwiki.Server.WebApplication.Uni.DownloadPostFile;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class PostFileComponentViewModel
	{
		public string OriginalFileNameInputName { get; }
		public string FileNameElementName { get; }

		public string PostFileComponentId { get; }
		public string ModalId { get; }
		public string FileNameWithoutExtension { get; }
		public string FileNameExtension { get; }
		public string FileName => FileNameWithoutExtension + FileNameExtension;
		public bool IsImage { get; }

		public RenamePostFileForm RenamePostFileForm { get; }
		public RenamePostFileRoute RenamePostFileRoute { get; }
		public DownloadPostFileRoute DownloadPostFileRoute { get; }
		public RemovePostFileRoute RemovePostFileRoute { get; }


		public PostFileComponentViewModel(
			string postFileUrl,
			string fileNameWithoutExtension,
			string fileNameExtension,
			bool isImage,
			Guid postFileId)
		{
			OriginalFileNameInputName = "originalFileName";
			FileNameElementName = "fileName";

			// Add 'a' at the beginning, to prevent it to start with a number
			ModalId = "a" + postFileId;
			PostFileComponentId = "a" + Guid.NewGuid();
			FileNameWithoutExtension = fileNameWithoutExtension;
			FileNameExtension = fileNameExtension;

			IsImage = isImage;

			RenamePostFileForm = new();
			DownloadPostFileRoute = new(postFileUrl);
			RenamePostFileRoute = new(postFileId);
			RemovePostFileRoute = new(postFileId);
		}
	}
}