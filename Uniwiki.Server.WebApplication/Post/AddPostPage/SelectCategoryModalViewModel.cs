﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class SelectCategoryModalViewModel : ModalViewModel
	{
		public (string? CategoryName, int PostsCount)[] Categories { get; }
		public string SelectCategoryInputId { get; }
		public string SelectedCategoryTextId { get; }
		public string? SelectedCategory { get; }

		public SelectCategoryModalViewModel(string? selectedCategory,
			(string? CategoryName, int PostsCount)[] categories)
			: base(
				"Select a category",
				"selectCategoryModalId")
		{
			SelectedCategory = selectedCategory;
			Categories = categories;
			SelectCategoryInputId = "selectCategoryInputId";
			SelectedCategoryTextId = "selectedCategoryTextId";
		}
	}
}