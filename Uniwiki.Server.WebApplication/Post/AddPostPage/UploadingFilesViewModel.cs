﻿using System;
using Uniwiki.Server.WebApplication.Post.UploadPostFile;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Post.AddPostPage
{
	public class UploadingFilesViewModel : ModalViewModel
	{
		public string UploadingFilesContainerId { get; }
		public UploadPostFileRoute UploadPostFileRoute { get; }
		public string UploadingPostFileTemplateId { get; }

		public UploadingFilesViewModel(Guid courseId, Guid? postId)
			: base(
				"Uploading files",
				"uploadingFilesModal",
				false
			)
		{
			UploadingFilesContainerId = "uploadingFilesContainer";
			UploadingPostFileTemplateId = "uploadingPostFileTemplate";
			UploadPostFileRoute = new(courseId, postId);
		}
	}
}