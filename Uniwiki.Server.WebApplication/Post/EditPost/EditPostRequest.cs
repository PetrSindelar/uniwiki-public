﻿using System;
using Uniwiki.Server.WebApplication.Post.EditPostPage;

namespace Uniwiki.Server.WebApplication.Post.EditPost
{
	public class EditPostRequest
	{
		public Guid PostId { get; }
		public string PostText { get; }
		public string? PostCategory { get; }
		public bool PostIsAnonymous { get; }

		public EditPostRequest(Guid postId, string postText, string? postCategory,
			bool postIsAnonymous)
		{
			PostId = postId;
			PostText = postText;
			PostCategory = postCategory;
			PostIsAnonymous = postIsAnonymous;
		}
	}
}