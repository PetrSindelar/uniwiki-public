﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Post.EditPost
{
	public class EditPostRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/EditPost";
		public const string PostIdRouteParameter = "postId";

		public const string RouteAttribute =
			BaseRoute + "/{" +
			PostIdRouteParameter + "}";

		public Guid PostId { get; }

		public EditPostRoute(Guid postId)
		{
			PostId = postId;
		}

		public override string Build() => $"{BaseRoute}/{PostId}";
	}
}