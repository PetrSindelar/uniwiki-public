﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Services;

namespace Uniwiki.Server.WebApplication.Post.EditPost
{
	[ScopedService]
	public class EditPostServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IUserInfoService _userInfoService;

		public EditPostServerAction(UniwikiContext uniwikiContext, IUserInfoService userInfoService)
		{
			_uniwikiContext = uniwikiContext;
			_userInfoService = userInfoService;
		}

		public async Task<EditPostViewModel> ExecuteAsync(EditPostRequest request)
		{
			var userClaims = _userInfoService.GetUserInfoOrThrow().UserClaims;

			// Find the post
			var post = await _uniwikiContext
				.Posts
				.Include(p => p.Course)
				.ThenInclude(c => c.Faculty)
				.ThenInclude(f => f.University)
				.FirstAsync(p => p.Id == request.PostId);

			// Check if the user has the rights to do it
			if (!userClaims.UserRoles.IsAdministrator && userClaims.UserId != post.AuthorId)
			{
				throw new NotAuthorizedException();
			}

			post.Text = request.PostText;
			post.IsAnonymous = request.PostIsAnonymous;
			post.Category = request.PostCategory;

			await _uniwikiContext.SaveChangesAsync();

			return new EditPostViewModel(post.Course.Faculty.University.Url, post.Course.Faculty.Url,
				post.Course.Url, post.Url);
		}
	}
}