﻿using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Post.EditPost
{
	public static class EditPostStandardizer
	{
		public static EditPostRequest Standardize(EditPostRequest request)
			=> new EditPostRequest(
				request.PostId,
				Standardizers.FirstCharToUpper(request.PostText.Trim()),
				string.IsNullOrWhiteSpace(request.PostCategory)
					? null
					: Standardizers.FirstCharToUpper(request.PostCategory),
				request.PostIsAnonymous);
	}
}