﻿using Uniwiki.Server.WebApplication.Uni.PostPage;

namespace Uniwiki.Server.WebApplication.Post.EditPost
{
	public class EditPostViewModel
	{
		public PostPageRoute RedirectRoute { get; }

		public EditPostViewModel(string universityUrl, string facultyUrl, string courseUrl, string postUrl)
		{
			RedirectRoute = new(universityUrl, facultyUrl, courseUrl, postUrl);
		}
	}
}