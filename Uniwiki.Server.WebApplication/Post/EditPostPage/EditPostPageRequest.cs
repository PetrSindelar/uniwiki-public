﻿namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	public class EditPostPageRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string PostUrl { get; }

		public EditPostPageRequest(string universityUrl,
			string facultyUrl,
			string courseUrl,
			string postUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostUrl = postUrl;
		}
	}
}