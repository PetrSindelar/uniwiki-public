﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	public class EditPostPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/EditPost";
		public const string UniversityRouteParameter = "universityUrl";
		public const string FacultyRouteParameter = "facultyUrl";
		public const string CourseRouteParameter = "courseUrl";
		public const string PostRouteParameter = "postUrl";

		public const string RouteAttribute =
			BaseRoute + "/{" +
			UniversityRouteParameter + "}/{" +
			FacultyRouteParameter + "}/{" +
			CourseRouteParameter + "}/{" +
			PostRouteParameter + "}";

		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string PostUrl { get; }

		public EditPostPageRoute(string universityUrl, string facultyUrl, string courseUrl, string postUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostUrl = postUrl;
		}

		public override string Build() => $"{BaseRoute}/{UniversityUrl}/{FacultyUrl}/{CourseUrl}/{PostUrl}";
	}
}