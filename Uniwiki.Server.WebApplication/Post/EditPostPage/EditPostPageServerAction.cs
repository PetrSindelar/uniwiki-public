﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.AddOrEditPostPage;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebApplication.Shared.Services;

namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	[ScopedService]
	public class EditPostPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IUserInfoService _userInfoService;

		public EditPostPageServerAction(UniwikiContext uniwikiContext, IUserInfoService userInfoService)
		{
			_uniwikiContext = uniwikiContext;
			_userInfoService = userInfoService;
		}

		public async Task<EditPostPageViewModel> ExecuteAsync(EditPostPageRequest request)
		{
			var userClaims = _userInfoService.GetUserInfoOrThrow().UserClaims;

			var post = await _uniwikiContext
				.Posts
				.Include(p => p.Course)
				.ThenInclude(c => c.Faculty)
				.ThenInclude(f => f.University)
				.Where(p => p.Url == request.PostUrl &&
				            p.Course.Url == request.CourseUrl &&
				            p.Course.Faculty.Url == request.FacultyUrl &&
				            p.Course.Faculty.University.Url == request.UniversityUrl)
				.FirstAsync();

			// Check if the user has the rights to do it
			if (!userClaims.UserRoles.IsAdministrator && userClaims.UserId != post.AuthorId)
			{
				throw new NotAuthorizedException();
			}

			var categories =
				await AddOrEditPostPageAccessors.GetCourseCategoriesForPost(post.CourseId)(_uniwikiContext);

			// Get the files for the post
			var postFileComponentViewModels = _uniwikiContext
				.PostFiles
				.Where(pf => pf.PostId == post.Id)
				.Select(
					pf => new PostFileComponentViewModel(pf.Url, pf.NameWithoutExtension, pf.Extension, pf.IsImage,
						pf.Id))
				.ToArray();

			// Prepare a view model for AddOrEditPage
			var editPageViewModel = new EditPostPageViewModel(
				post.Id,
				request.UniversityUrl,
				request.FacultyUrl,
				request.CourseUrl,
				post.Course.LongName,
				userClaims.FirstName,
				userClaims.FamilyName,
				userClaims.ProfileUrl,
				userClaims.ProfileImageUrl,
				categories,
				post.CourseId,
				postFileComponentViewModels,
				post.Text,
				post.Category,
				post.IsAnonymous
			);

			return editPageViewModel;
		}
	}
}