﻿using System;
using Uniwiki.Server.WebApplication.Post.AddOrEditPostPage;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebApplication.Post.EditPost;

namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	public class EditPostPageViewModel : AddOrEditPostPageViewModel
	{
		public EditPostPageViewModel(
			Guid postId,
			string universityUrl,
			string facultyUrl,
			string courseUrl,
			string courseLongName,
			string profileFirstName,
			string profileFamilyName,
			string profileUrl,
			string? profileImageUrl,
			(string? CategoryName, int PostsCount)[] categories,
			Guid courseId,
			PostFileComponentViewModel[] postFileComponentViewModels,
			string postDescription,
			string? postCategory,
			bool postIsAnonymous
		) : base(
			universityUrl,
			facultyUrl,
			courseUrl,
			profileFirstName,
			profileFamilyName,
			profileUrl,
			profileImageUrl,
			categories,
			courseId,
			postFileComponentViewModels,
			postDescription,
			postCategory,
			postIsAnonymous,
			new EditPostRoute(postId),
			"Edit a post in " + courseLongName
		)
		{
		}
	}
}