﻿using System;

namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	public record UserClaims
	{
		public string FirstName { get; }
		public string FamilyName { get; }
		public string FullProfileUrl { get; }
		public string ProfileUrl { get; }
		public string? FullProfileImageUrl { get; }
		public string? ProfileImageUrl { get; }
		public Guid UserId { get; }
		public UserRoles UserRoles { get; }

		public UserClaims(string firstName, string familyName, string fullProfileUrl, string profileUrl,
			string? fullProfileImageUrl, string? profileImageUrl,
			Guid userId, string[] userRoles)
		{
			FirstName = firstName;
			FamilyName = familyName;
			FullProfileUrl = fullProfileUrl;
			ProfileUrl = profileUrl;
			FullProfileImageUrl = fullProfileImageUrl;
			ProfileImageUrl = profileImageUrl;
			UserId = userId;
			UserRoles = new UserRoles(userRoles);
		}
	}
}