﻿namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	public class UserInfo
	{
		public UserClaims UserClaims { get; }
		public int NotificationsCount { get; }

		public UserInfo(UserClaims userClaims, int notificationsCount)
		{
			UserClaims = userClaims;
			NotificationsCount = notificationsCount;
		}
	}
}