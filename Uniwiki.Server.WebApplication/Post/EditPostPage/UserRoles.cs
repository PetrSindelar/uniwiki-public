﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Uniwiki.Server.WebApplication.Shared.Authentication;

namespace Uniwiki.Server.WebApplication.Post.EditPostPage
{
	public class UserRoles : IEnumerable<string>
	{
		private readonly string[] _roles;

		public UserRoles(string[] roles)
		{
			_roles = roles;
		}

		public bool IsAdministrator => _roles.Contains(CustomRoles.Administrator);
		public bool IsUser => _roles.Contains(CustomRoles.User);

		public IEnumerator<string> GetEnumerator() => _roles.AsEnumerable().GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}