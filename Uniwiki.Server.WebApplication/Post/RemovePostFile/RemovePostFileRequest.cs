﻿using System;

namespace Uniwiki.Server.WebApplication.Post.RemovePostFile
{
	public class RemovePostFileRequest
	{
		public Guid PostFileId { get; }
		public Guid UserId { get; }
		public bool UserIsAdmin { get; }

		public RemovePostFileRequest(Guid postFileId, Guid userId, bool userIsAdmin)
		{
			PostFileId = postFileId;
			UserId = userId;
			UserIsAdmin = userIsAdmin;
		}
	}
}