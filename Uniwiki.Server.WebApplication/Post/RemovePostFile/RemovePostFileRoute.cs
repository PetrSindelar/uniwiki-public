﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Post.RemovePostFile
{
	public class RemovePostFileRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/RemovePostFile";
		public const string PostFileIdParameter = "postFileId";

		public const string RouteAttribute =
			BaseRoute + "/{" + PostFileIdParameter + "}";

		public Guid PostFileId { get; }

		public RemovePostFileRoute(Guid postFileId)
		{
			PostFileId = postFileId;
		}

		public override string Build() => $"{BaseRoute}/{PostFileId}";
	}
}