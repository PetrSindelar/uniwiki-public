﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.Post.RemovePostFile
{
	[ScopedService]
	public class RemovePostFileServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public RemovePostFileServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task ExecuteAsync(RemovePostFileRequest request)
		{
			var postFile = await _uniwikiContext.PostFiles.FindAsync(request.PostFileId);

			if (!request.UserIsAdmin && postFile.ProfileId != request.UserId)
			{
				throw new NotAuthorizedException();
			}

			_uniwikiContext.PostFiles.Remove(postFile);

			await _uniwikiContext.SaveChangesAsync();
		}
	}
}