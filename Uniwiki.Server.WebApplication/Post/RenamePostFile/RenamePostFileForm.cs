﻿namespace Uniwiki.Server.WebApplication.Post.RenamePostFile
{
	public class RenamePostFileForm
	{
		public string FileName { get; set; } = null!;

		public RenamePostFileForm()
		{
		}
	}
}