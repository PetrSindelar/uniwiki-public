﻿using System;

namespace Uniwiki.Server.WebApplication.Post.RenamePostFile
{
	public class RenamePostFileRequest
	{
		public Guid PostFileId { get; }
		public string NewFileNameWithoutExtension { get; }
		public Guid UserId { get; }
		public bool UserIsAdmin { get; }

		public RenamePostFileRequest(Guid postFileId, string newFileNameWithoutExtension, Guid userId, bool userIsAdmin)
		{
			PostFileId = postFileId;
			NewFileNameWithoutExtension = newFileNameWithoutExtension;
			UserId = userId;
			UserIsAdmin = userIsAdmin;
		}
	}
}