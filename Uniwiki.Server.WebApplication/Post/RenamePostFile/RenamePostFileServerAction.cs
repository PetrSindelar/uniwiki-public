﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.Post.RenamePostFile
{
	[ScopedService]
	public class RenamePostFileServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public RenamePostFileServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task ExecuteAsync(RenamePostFileRequest request)
		{
			var postFile = await _uniwikiContext.PostFiles.FindAsync(request.PostFileId);

			// Check if the user is authorized for thios operation
			if (!request.UserIsAdmin && postFile.ProfileId != request.UserId)
			{
				throw new NotAuthorizedException();
			}

			postFile.NameWithoutExtension = request.NewFileNameWithoutExtension;

			await _uniwikiContext.SaveChangesAsync();
		}
	}
}