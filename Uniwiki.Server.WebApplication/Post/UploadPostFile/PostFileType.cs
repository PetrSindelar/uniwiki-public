﻿namespace Uniwiki.Server.WebApplication.Post.UploadPostFile
{
	public enum PostFileType
	{
		Image = 1,
		Video = 2,
		Audio = 3,
		Document = 4,
		Archive = 5,
		Script = 6,
		Other = 50,
	}
}