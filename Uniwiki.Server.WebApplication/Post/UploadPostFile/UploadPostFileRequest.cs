﻿using System;
using Microsoft.AspNetCore.Http;

namespace Uniwiki.Server.WebApplication.Post.UploadPostFile
{
	public class UploadPostFileRequest
	{
		public IFormFile FormFile { get; }
		public Guid CourseId { get; }
		public string StorageRootPath { get; }
		public Guid UserId { get; }
		public Guid? PostId { get; }

		public UploadPostFileRequest(IFormFile formFile, Guid courseId, string storageRootPath, Guid userId,
			Guid? postId)
		{
			FormFile = formFile;
			CourseId = courseId;
			StorageRootPath = storageRootPath;
			UserId = userId;
			PostId = postId;
		}
	}
}