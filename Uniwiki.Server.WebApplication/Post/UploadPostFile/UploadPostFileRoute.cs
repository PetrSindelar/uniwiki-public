﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Post.UploadPostFile
{
	public class UploadPostFileRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/UploadPostFile";

		public const string CourseIdRouteParameter = "courseId";
		public const string PostIdRouteParameter = "postId";

		public const string RouteAttribute =
			BaseRoute + "/{" + CourseIdRouteParameter + "}/{" + PostIdRouteParameter + "?}";

		public Guid CourseId { get; }
		public Guid? PostId { get; }

		public UploadPostFileRoute(Guid courseId, Guid? postId)
		{
			CourseId = courseId;
			PostId = postId;
		}

		public override string Build() => $"{BaseRoute}/{CourseId}{(PostId != null ? $"/{PostId}" : string.Empty)}";
	}
}