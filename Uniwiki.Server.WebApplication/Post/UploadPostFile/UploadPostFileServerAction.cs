﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Post.UploadPostFile
{
	// Scaling images: https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement/srcset

	[ScopedService]
	public class UploadPostFileServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public UploadPostFileServerAction(UniwikiContext uniwikiContext, ITimeService timeService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task<PostFileComponentViewModel> ExecuteAsync(UploadPostFileRequest request)
		{
			// Get storage info for the course
			var storagePath = (await _uniwikiContext
					.Courses
					.Include(c => c.Faculty)
					.ThenInclude(c => c.University)
					.Where(c => c.Id == request.CourseId)
					.Select(c => new
					{
						CourseStoragePath = c.StoragePath,
						FacultyStoragePath = c.Faculty.StoragePath,
						UniversityStoragePath = c.Faculty.University.StoragePath
					})
					.FirstAsync())
				.Apply(c => (c.CourseStoragePath, c.FacultyStoragePath, c.UniversityStoragePath));

			// Get the directory for the new file
			var directory = Path.Combine(request.StorageRootPath, storagePath.UniversityStoragePath,
				storagePath.FacultyStoragePath, storagePath.CourseStoragePath);

			// If the directory does not exist
			if (!Directory.Exists(directory))
			{
				// Create it
				Directory.CreateDirectory(directory);
			}

			// Generate ID for the file it will be used as a name when saving
			var postFileId = Guid.NewGuid();
			var extension = Path.GetExtension(request.FormFile.FileName);
			var fileNameInStorage = postFileId + extension;

			// Get absolute path for the file
			var path = Path.Combine(directory, fileNameInStorage);

			// Open a stream for the file
			await using var stream = new FileStream(path, FileMode.Create);

			// Write to the stream contents of the uploaded file
			await request.FormFile.CopyToAsync(stream);

			await stream.DisposeAsync();

			var nameWithoutExtension = Path.GetFileNameWithoutExtension(request.FormFile.FileName);

			var isImage = UploadPostFileUtils.GetPostFileType(extension) == PostFileType.Image;

			// Save a record to the DB
			var postFileModel = new PostFileModel(postFileId, nameWithoutExtension, extension, true, request.UserId,
				request.CourseId, request.PostId, _timeService.Now, request.FormFile.Length, isImage,
				fileNameInStorage);

			_uniwikiContext.PostFiles.Add(postFileModel);

			await _uniwikiContext.SaveChangesAsync();

			var viewModel = new PostFileComponentViewModel(postFileModel.Url, postFileModel.NameWithoutExtension,
				postFileModel.Extension, postFileModel.IsImage, postFileModel.Id);

			return viewModel;
		}
	}
}