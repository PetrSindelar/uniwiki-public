﻿using MimeTypes;

namespace Uniwiki.Server.WebApplication.Post.UploadPostFile
{
	public class UploadPostFileUtils
	{
		public static PostFileType GetPostFileType(string extension)
		{
			extension = extension.ToLower();
			var mimeType = MimeTypeMap.GetMimeType(extension.ToLower());

			if (mimeType.StartsWith("image/"))
				return PostFileType.Image;

			if (mimeType.StartsWith("video/"))
				return PostFileType.Video;

			if (mimeType.StartsWith("text/"))
				return PostFileType.Document;

			if (mimeType.StartsWith("audio/"))
				return PostFileType.Audio;

			switch (extension)
			{
				case ".doc":
				case ".docx":
				case ".ppt":
				case ".pptx":
				case ".xls":
				case ".xlsx":
				case ".abw":
				case ".epub":
				case ".odp":
				case ".ods":
				case ".odt":
				case ".pdf":
				case ".azw": return PostFileType.Document;

				case ".json":
				case ".php":
				case ".sh":
				case ".cs":
				case ".java":
				case ".r":
				case ".py":
				case ".csh": return PostFileType.Script;

				case ".zip":
				case ".7z":
				case ".bz":
				case ".bz2":
				case ".gz":
				case ".jar":
				case ".rar":
				case ".tar":
				case ".arc": return PostFileType.Archive;
			}


			return PostFileType.Other;
		}

		// From https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
		//public static IDictionary<string, PostFileDetails> PostFileDetails = new Dictionary<string, PostFileDetails>()
		//	// Archives
		//	.ApplySideEffect(d => d.Add(".arc", new(PostFileType.Archive, "application/x-freearc")))
		//	.ApplySideEffect(d => d.Add(".bz", new(PostFileType.Archive, "application/x-bzip")))
		//	.ApplySideEffect(d => d.Add(".bz2", new(PostFileType.Archive, "application/x-bzip2")))

		//	// /Archives

		//	// Audio
		//	.ApplySideEffect(d => d.Add(".acc", new(PostFileType.Audio, "audio/aac")))

		//	// /Audio

		//	// Documents
		//	.ApplySideEffect(d => d.Add(".abw", new(PostFileType.Document, "application/x-abiword")))
		//	.ApplySideEffect(d => d.Add(".azw", new(PostFileType.Document, "application/vnd.amazon.ebook")))
		//	// /Documents

		//	// Images
		//	.ApplySideEffect(d => d.Add(".apng", new(PostFileType.Image, "image/apng")))
		//	.ApplySideEffect(d => d.Add(".avif", new(PostFileType.Image, "image/avif")))
		//	.ApplySideEffect(d => d.Add(".gif", new(PostFileType.Image, "image/gif")))
		//	.ApplySideEffect(d => d.Add(".pjp", new(PostFileType.Image, "image/jpeg")))
		//	.ApplySideEffect(d => d.Add(".pjpeg", new(PostFileType.Image, "image/jpeg")))
		//	.ApplySideEffect(d => d.Add(".jfif", new(PostFileType.Image, "image/jpeg")))
		//	.ApplySideEffect(d => d.Add(".jpg", new(PostFileType.Image, "image/jpeg")))
		//	.ApplySideEffect(d => d.Add(".bmp", new(PostFileType.Image, "image/bmp")))
		//	// /Images

		//	// Other
		//	.ApplySideEffect(d => d.Add(".bin", new(PostFileType.Other, "application/octet-stream")))

		//	// /Other

		//	// Scripts
		//	.ApplySideEffect(d => d.Add(".apng", new(PostFileType.Image, "image/apng")))

		//	// Scripts

		//	// Video
		//	.ApplySideEffect(d => d.Add(".avi", new(PostFileType.Video, "video/x-msvideo")))

		//	// /Video

		/// <summary>
		/// Based on https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
		/// </summary>
		/// <param name="extension">Extension of a file containing the starting dot (e.g. ".png")</param>
		/// <returns>The corresponding content type for the browser to understand what file is served.</returns>
		public static string GetContentTypeForExtension(string extension)
		{
			switch (extension.ToLower())
			{
				// Image types
				case ".png": return "image/png";
				case ".svg": return "image/svg+xml";
				case ".webp": return "image/webp";
				// /Image types

				// Documents
				case ".txt": return "text/plain";
				case ".html": return "text/html";
				case ".css": return "text/css";
				case ".javascript": return "text/javascript";
				// Documents


				default: return "application/octet-stream";
			}
		}
	}
}