﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Profile.ProfileImage
{
	public class ProfileImageRoute : RouteBase
	{
		public string ProfileImageUrl { get; }

		public const string BaseRoute = StaticFileRoute + "/img/profile-images";
		public const string RouteAttribute = BaseRoute;

		public ProfileImageRoute(string profileImageUrl)
		{
			ProfileImageUrl = profileImageUrl;
		}

		public override string Build() => BaseRoute + "/" + ProfileImageUrl;
	}
}