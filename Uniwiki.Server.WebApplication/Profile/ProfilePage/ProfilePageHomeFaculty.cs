﻿namespace Uniwiki.Server.WebApplication.Profile.ProfilePage
{
	public class ProfilePageHomeFaculty
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string UniversityShortName { get; }
		public string FacultyLongName { get; }

		public ProfilePageHomeFaculty(string facultyFacultyUrl, string universityShortName, string facultyLongName,
			string universityUrl)
		{
			FacultyUrl = facultyFacultyUrl;
			UniversityShortName = universityShortName;
			FacultyLongName = facultyLongName;
			UniversityUrl = universityUrl;
		}
	}
}