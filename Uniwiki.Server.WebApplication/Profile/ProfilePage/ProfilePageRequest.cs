﻿using Uniwiki.Server.WebApplication.Post.EditPostPage;

namespace Uniwiki.Server.WebApplication.Profile.ProfilePage
{
	public class ProfilePageRequest
	{
		public string ProfileUrl { get; }

		public ProfilePageRequest(string profileUrl)
		{
			ProfileUrl = profileUrl;
		}
	}
}