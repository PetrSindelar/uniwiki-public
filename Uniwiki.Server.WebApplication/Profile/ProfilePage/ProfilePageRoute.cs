﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Profile.ProfilePage
{
	public class ProfilePageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Profile";

		public const string ProfileUrlRouteParameter = "profileUrl";
		public const string RouteAttribute = BaseRoute + "/{" + ProfileUrlRouteParameter + "}";

		public string ProfileUrl { get; }

		public ProfilePageRoute(string profileUrl)
		{
			ProfileUrl = profileUrl;
		}

		public override string Build() => BaseRoute + "/" + ProfileUrl;
	}
}