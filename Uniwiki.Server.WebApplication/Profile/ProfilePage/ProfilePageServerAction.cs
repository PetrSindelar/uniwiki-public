﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Services;

namespace Uniwiki.Server.WebApplication.Profile.ProfilePage
{
	[ScopedService]
	public class ProfilePageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IUserInfoService _userInfoService;

		public ProfilePageServerAction(UniwikiContext uniwikiContext, IUserInfoService userInfoService)
		{
			_uniwikiContext = uniwikiContext;
			_userInfoService = userInfoService;
		}

		public async Task<ProfilePageViewModel> ExecuteAsync(ProfilePageRequest request)
		{
			var isUsersProfile = request.ProfileUrl == _userInfoService.GetUserInfo()?.UserClaims.ProfileUrl;

			var profile = await _uniwikiContext
				.Users
				.AsNoTracking()
				.Where(u => u.Url == request.ProfileUrl)
				.Select(u => new { u.FirstName, u.FamilyName, u.ProfileImageUrl, u.Id })
				.FirstOrDefaultAsync();

			// Make sure, that the user exists
			if (profile == null)
			{
				throw new NotFoundException("This user does not exist");
			}

			ProfilePageHomeFaculty? profilePageHomeFaculty = null; // TODO: Create the home faculty

			var postsCount = await _uniwikiContext.Posts.Where(p => p.AuthorId == profile.Id).CountAsync();
			var usersCount = 0; // TODO: Add when implementing the refferals of users
			var postsStarsCount =
				(await _uniwikiContext.Posts.Where(p => p.AuthorId == profile.Id).Select(p => p.Likes.Count)
					.ToArrayAsync()).Sum();

			var starsCount = postsStarsCount;


			return new ProfilePageViewModel(profile.Id, profile.FirstName, profile.FamilyName, isUsersProfile,
				profile.ProfileImageUrl, postsCount, starsCount, usersCount, profilePageHomeFaculty);
		}
	}
}