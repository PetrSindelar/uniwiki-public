﻿using System;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.ReportUserRequestPage;
using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;
using Uniwiki.Server.WebApplication.User.SignOut;

namespace Uniwiki.Server.WebApplication.Profile.ProfilePage
{
	public class ProfilePageViewModel : PageLayoutViewModel
	{
		public string FirstName { get; }
		public string FamilyName { get; }
		public bool IsUsersProfile { get; }
		public SignOutRoute SignOutRoute { get; }
		public ProfileImageRoute AuthorImageRoute { get; }
		public int PostsCount { get; }
		public int StarsCount { get; }
		public int UsersCount { get; }
		public ProfilePageHomeFaculty? ProfilePageHomeFaculty { get; }
		public SelectCoursePageRoute? SelectCoursePageRoute { get; }
		public UsersInfoModalViewModel UsersInfoModalViewModel { get; }
		public ReportUserRequestPageRoute ReportUserRequestPageRoute { get; }

		public ProfilePageViewModel(Guid userId, string firstName, string familyName, bool isUsersProfile,
			string? profileImageUrl,
			int postsCount, int starsCount, int usersCount, ProfilePageHomeFaculty? profilePageHomeFaculty)
			: base(null, firstName + " " + familyName)
		{
			FirstName = firstName;
			FamilyName = familyName;
			IsUsersProfile = isUsersProfile;
			PostsCount = postsCount;
			StarsCount = starsCount;
			UsersCount = usersCount;
			ProfilePageHomeFaculty = profilePageHomeFaculty;
			SignOutRoute = new SignOutRoute();
			AuthorImageRoute = new ProfileImageRoute(profileImageUrl ?? "profile-placeholder.png");
			SelectCoursePageRoute = ProfilePageHomeFaculty == null
				? null
				: new SelectCoursePageRoute(ProfilePageHomeFaculty.UniversityUrl,
					ProfilePageHomeFaculty.FacultyUrl);
			UsersInfoModalViewModel = new UsersInfoModalViewModel();
			ReportUserRequestPageRoute = new ReportUserRequestPageRoute(userId);
		}
	}
}