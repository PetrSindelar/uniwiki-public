﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Profile.ProfilePage
{
	public class UsersInfoModalViewModel : ModalViewModel
	{
		public UsersInfoModalViewModel()
			: base("What are 'Users'?", "usersInfoModal")
		{
		}
	}
}