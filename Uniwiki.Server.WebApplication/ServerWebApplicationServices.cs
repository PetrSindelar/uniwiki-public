﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Admin.AddFaculty;
using Uniwiki.Server.WebApplication.Admin.AddUniversity;
using Uniwiki.Server.WebApplication.Post.AddPost;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebApplication.Post.EditPost;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Post.RemovePostFile;
using Uniwiki.Server.WebApplication.Post.RenamePostFile;
using Uniwiki.Server.WebApplication.Post.UploadPostFile;
using Uniwiki.Server.WebApplication.Uni.AddCourse;
using Uniwiki.Server.WebApplication.Uni.AddCoursePage;
using Uniwiki.Server.WebApplication.Uni.AddPostComment;
using Uniwiki.Server.WebApplication.Uni.CoursePage;
using Uniwiki.Server.WebApplication.Uni.DownloadPostFile;
using Uniwiki.Server.WebApplication.Uni.HomePage;
using Uniwiki.Server.WebApplication.Uni.LikePost;
using Uniwiki.Server.WebApplication.Uni.LikePostComment;
using Uniwiki.Server.WebApplication.Uni.PostPage;
using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;
using Uniwiki.Server.WebApplication.Uni.SelectFacultyPage;
using Uniwiki.Server.WebApplication.User.Shared.Services;

namespace Uniwiki.Server.WebApplication
{
	public static class ServerWebApplicationServices
	{
		public static IServiceCollection AddUniwikiServerWebApplicationServices(this IServiceCollection services,
			Action<DbContextOptionsBuilder> databaseBuilder, bool isDevelopment)
			=> services
				.AddUniwikiServerPersistence(databaseBuilder)
				.AddAttributeServices()
				.Apply(s => isDevelopment ? s.AddScoped<ICodeGeneratorService, DevelopmentCodeGeneratorService>() : s);

		// Server actions
		// Account
		//.AddScoped<SignInUpViaEmailServerAction>()
		//.AddScoped<SignInUpViaEmailRequestCodeServerAction>()
		//.AddScoped<SignInUpViaFacebookServerAction>()
		//.AddScoped<SignUpViaEmailServerAction>()

		//// Admin
		//.AddScoped<AddFacultyServerAction>()
		//.AddScoped<AddUniversityServerAction>()

		//// Post
		//.AddScoped<AddPostServerAction>()
		//.AddScoped<AddPostPageServerAction>()
		//.AddScoped<EditPostPageServerAction>()
		//.AddScoped<EditPostServerAction>()
		//.AddScoped<RemovePostFileServerAction>()
		//.AddScoped<RenamePostFileServerAction>()
		//.AddScoped<UploadPostFileServerAction>()

		//// Uni
		//.AddScoped<AddCourseServerAction>()
		//.AddScoped<AddCoursePageServerAction>()
		//.AddScoped<AddPostCommentServerAction>()
		//.AddScoped<CoursePageServerAction>()
		//.AddScoped<DownloadPostFileServerAction>()
		//.AddScoped<HomePageServerAction>()
		//.AddScoped<LikePostServerAction>()
		//.AddScoped<LikePostCommentServerAction>()
		//.AddScoped<PostPageServerAction>()
		//.AddScoped<SelectCoursePageServerAction>()
		//.AddScoped<SelectFacultyPageServerAction>();
	}
}