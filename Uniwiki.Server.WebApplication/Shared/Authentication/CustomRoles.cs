﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniwiki.Server.WebApplication.Shared.Authentication
{
	public static class CustomRoles
	{
		public const string Administrator = "Administrator";
		public const string User = "User";

		public static string[] AllRoles => new[]
		{
			Administrator,
			User
		};
	}
}