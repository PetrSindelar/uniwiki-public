﻿namespace Uniwiki.Server.WebApplication.Shared.Configurations
{
	public class AuthenticationOptions
	{
		public string FacebookAppSecret { get; set; } = null!;
		public int SignInTokenDurationDays { get; set; }
		public int ConfirmationCodeDurationMinutes { get; set; }
		public int EmailVerificationCodeLength { get; set; }
		public string DefaultProfileImageUrl { get; set; } = null!;
		public string AdministratorEmail { get; set; } = null!;
	}
}