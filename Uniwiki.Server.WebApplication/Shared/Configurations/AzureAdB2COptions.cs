﻿namespace Uniwiki.Server.WebApplication.Shared.Configurations
{
	public class AzureAdB2COptions
	{
		public const string PolicyAuthenticationProperty = "Policy";

		public string ClientId { get; set; } = null!;
		public string AzureAdB2CInstance { get; set; }
		public string Tenant { get; set; } = null!;
		public string SignUpSignInPolicyId { get; set; } = null!;
		public string SignInPolicyId { get; set; } = null!;
		public string SignUpPolicyId { get; set; } = null!;
		public string ResetPasswordPolicyId { get; set; } = null!;
		public string EditProfilePolicyId { get; set; } = null!;
		public string RedirectUri { get; set; } = null!;

		public string DefaultPolicy => SignUpSignInPolicyId;
		public string Authority => $"{AzureAdB2CInstance}/{Tenant}/{DefaultPolicy}/v2.0";

		public string ClientSecret { get; set; } = null!;
		public string ApiUrl { get; set; } = null!;
		public string ApiScopes { get; set; } = null!;

		public AzureAdB2COptions() => AzureAdB2CInstance = "https://fabrikamb2c.b2clogin.com/tfp";
	}
}