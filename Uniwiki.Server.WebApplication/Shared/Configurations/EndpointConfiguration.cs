namespace Uniwiki.Server.WebApplication.Shared.Configurations
{
	public class EndpointConfiguration
	{
		public string Host { get; set; } = null!;
		public int? Port { get; set; }
		public string Scheme { get; set; } = null!;
		public string StoreName { get; set; } = null!;
		public string StoreLocation { get; set; } = null!;
		public string FilePath { get; set; } = null!;
		public string Password { get; set; } = null!;
	}
}