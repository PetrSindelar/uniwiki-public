﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniwiki.Server.WebApplication.Shared.Configurations
{
	public class FormsOptions
	{
		public string AddUniversityRequestForm { get; set; } = null!;
		public string AddFacultyRequestForm { get; set; } = null!;
		public string RenameCourseRequestForm { get; set; } = null!;
		public string ReportCourseRequestForm { get; set; } = null!;
		public string ReportUserRequestForm { get; set; } = null!;
		public string ReportPostRequestForm { get; set; } = null!;

		public string GetAddUniversityRequestForm()
			=> AddUniversityRequestForm;

		public string GetAddFacultyRequestForm(string universityLongName)
			=> string.Format(AddFacultyRequestForm, universityLongName);

		public string GetRenameCourseRequestForm(Guid courseId)
			=> string.Format(RenameCourseRequestForm, courseId);

		public string GetReportCourseRequestForm(Guid courseId)
			=> string.Format(ReportCourseRequestForm, courseId);

		public string GetReportUserRequestForm(Guid userId)
			=> string.Format(ReportUserRequestForm, userId);

		public string GetReportPostRequestForm(Guid postId)
			=> string.Format(ReportPostRequestForm, postId);
	}
}