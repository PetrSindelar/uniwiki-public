﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniwiki.Server.WebApplication.Shared.Constants
{
	public static class CustomClaimTypes
	{
		public const string FirstName = "FirstName";
		public const string FamilyName = "FamilyName";
		public const string Id = "Id";
		public const string ProfileUrl = "ProfileUrl";
		public const string FullProfileUrl = "FullProfileUrl";
		public const string FullProfileImageUrl = "FullProfileImageUrl";
		public const string ProfileImageUrl = "ProfileImageUrl";
	}
}