﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniwiki.Server.WebApplication.Shared.Constants
{
	public class RouteConstants
	{
		public const string ApiRoute = "/api";
		public const string PageRoute = "";
		public const string StaticFileRoute = "";
		public const string PartialViewRoute = "/partial";
	}
}