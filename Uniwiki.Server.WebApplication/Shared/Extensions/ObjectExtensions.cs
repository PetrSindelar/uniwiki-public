﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniwiki.Server.WebApplication.Shared.Extensions
{
	public static class ObjectExtensions
	{
		public static string PropertiesToString<T>(this T? obj)
		{
			StringBuilder stringBuilder = new StringBuilder();

			if (obj == null)
			{
				stringBuilder.AppendLine($"{typeof(T).FullName} is null");
				return stringBuilder.ToString();
			}

			stringBuilder.AppendLine("Object: " + typeof(T).FullName);

			var props = GetProperties(obj);

			if (props.Count == 0)
			{
				stringBuilder.AppendLine("[Object has no properties]");
			}

			foreach (var prop in props)
			{
				stringBuilder.AppendLine($"> {prop.Key}: '{prop.Value}'");
			}

			return stringBuilder.ToString();
		}

		private static Dictionary<string, string> GetProperties(object obj)
		{
			var props = new Dictionary<string, string>();

			var type = obj.GetType();
			foreach (var prop in type.GetProperties())
			{
				var val = prop.GetValue(obj, new object[] { })?.ToString() ?? string.Empty;
				props.Add(prop.Name, val);
			}

			return props;
		}
	}
}