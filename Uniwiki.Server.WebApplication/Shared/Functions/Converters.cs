﻿using System;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Shared.Functions
{
	public static class Converters
	{
		public static string ToReadableFileSize(long fileSizeInBytes)
		{
			if (fileSizeInBytes / 1_000 == 0)
			{
				return "> 1 KB";
			}

			if (fileSizeInBytes / 1_000_000 == 0)
			{
				return fileSizeInBytes / 1_000 + " KB";
			}

			if (fileSizeInBytes / 1_000_000_000 == 0)
			{
				return fileSizeInBytes / 1_000_000 + " MB";
			}

			return fileSizeInBytes / 1_000_000_000 + " GB";
		}

		public static string ToPassedDayTime(DateTime currentTime, DateTime dateTime) => (currentTime - dateTime).Apply(
			timeDifference => timeDifference switch
			{
				var t when (t.Days / 365 > 1) => $"{t.Days / 365} years ago",
				var t when (t.Days / 365 == 1) => $"{t.Days / 365} year ago",
				var t when (t.Days / 30 > 1) => $"{t.Days / 30} months ago",
				var t when (t.Days / 30 == 1) => $"{t.Days / 30} month ago",
				var t when (t.Days / 7 > 1) => $"{t.Days / 7} weeks ago",
				var t when (t.Days / 7 == 1) => $"{t.Days / 7} week ago",
				var t when (t.Days > 1) => $"{t.Days} days ago",
				var t when (t.Days == 1) => $"{t.Days} day ago",
				var t when (t.Hours > 1) => $"{t.Hours} hours ago",
				var t when (t.Hours == 1) => $"{t.Hours} hour ago",
				var t when (t.Minutes > 1) => $"{t.Minutes} minutes ago",
				_ => "Right now"
			});
	}
}