﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Shared.Functions
{
	internal static class Standardizers
	{
		public static string StandardizeUrl(string url)
		{
			return RemoveWhiteSpaces(url.ToLower());
		}

		public static async Task<string> CreateUrl(string text, Func<string, Task<bool>> isUniqAsync)
		{
			string url;
			int? salt = null;
			do
			{
				url = CreateUrl(text, salt);
				salt = salt == null ? 1 : salt + 1;
			} while (!await isUniqAsync(url));

			return url;
		}

		private static string CreateUrl(string text, int? salt)
		{
			text = RemoveAccents(text);
			text = RemoveNonEnglishLettersKeepNumbers(text, true);
			text = OptimizeWhiteSpaces(text, "-");
			text = text.Trim('-');
			text = text.ToLower();
			text = HttpUtility.UrlEncode(text);
			text = new string(text.Take(Server.Shared.Constants.Validations.UrlMaxLength)
				.ToArray()); // Take only first X letters
			text = text + salt; // Add salt

			return text;
		}

		/// <summary>
		///     Replaces all multiple whitespace characters by a single space character and trims whitespaces from both sides.
		/// </summary>
		public static string OptimizeWhiteSpaces(string text, string optimizeBy)
		{
			return Regex.Replace(text.Trim(), @"\s+", optimizeBy);
		}

		public static string OptimizeWhiteSpaces(string text)
		{
			return OptimizeWhiteSpaces(text, " ");
		}

		public static string FirstCharToUpper(string text)
			=> text switch
			{
				null => throw new ArgumentNullException(nameof(text)),
				"" => string.Empty,
				_ => text.First().ToString().ToUpper() + text.Substring(1)
			};

		public static string RemoveWhiteSpaces(string text)
		{
			return Regex.Replace(text, @"\s+", string.Empty).Trim();
		}

		public static string RemoveAccents(string text)
		{
			StringBuilder sbReturn = new StringBuilder();
			var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
			foreach (var letter in arrayText)
				if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
					sbReturn.Append(letter);

			return sbReturn.ToString();
		}

		public static string StandardizeSearchText(string text)
		{
			var reducedWhiteSpaceText = Regex.Replace(text, @"\s+", " ").ToLower().Trim();

			return RemoveAccents(reducedWhiteSpaceText);
		}

		public static string StandardizeEmail(string email)
		{
			return email.Trim().ToLower();
		}

		public static string StandardizeName(string name)
			=> name
				.Apply(OptimizeWhiteSpaces)
				.Apply(FirstCharToUpper);

		public static string RemoveNonEnglishLettersKeepNumbers(string text, bool preserveWhiteSpace)
		{
			return text.Aggregate(
				"",
				(acc, letter) =>
					IsEnglishLetter(letter) || char.IsDigit(letter) || preserveWhiteSpace && char.IsWhiteSpace(letter)
						? acc + letter
						: acc
			);
		}

		public static bool IsEnglishLetter(char letter)
		{
			return letter >= 'A' && letter <= 'Z' || letter >= 'a' && letter <= 'z';
		}
	}
}