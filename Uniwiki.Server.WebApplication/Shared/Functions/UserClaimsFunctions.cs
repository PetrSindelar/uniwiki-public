﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;
using Uniwiki.Server.WebApplication.Shared.Constants;

namespace Uniwiki.Server.WebApplication.Shared.Functions
{
	[ScopedService()]
	public class UserClaimsFunctions
	{
		public static ClaimsIdentity GenerateClaimsAsync(ClaimsIdentity claimsIdentity, ProfileModel user,
			IList<string> roles)
		{
			if (!string.IsNullOrWhiteSpace(user.ProfileImageUrl))
			{
				claimsIdentity.AddClaim(new Claim(CustomClaimTypes.ProfileImageUrl, user.ProfileImageUrl));
				claimsIdentity.AddClaim(new Claim(CustomClaimTypes.FullProfileImageUrl,
					new ProfileImageRoute(user.ProfileImageUrl).Build()));
			}

			claimsIdentity.AddClaim(new Claim(CustomClaimTypes.ProfileUrl, user.Url));
			claimsIdentity.AddClaim(new Claim(CustomClaimTypes.FullProfileUrl, new ProfilePageRoute(user.Url).Build()));
			claimsIdentity.AddClaim(new Claim(CustomClaimTypes.FirstName, user.FirstName));
			claimsIdentity.AddClaim(new Claim(CustomClaimTypes.FamilyName, user.FamilyName));
			claimsIdentity.AddClaim(new Claim(CustomClaimTypes.Id, user.Id.ToString()));
			claimsIdentity.AddClaims(roles.Select(r => new Claim(ClaimTypes.Role, r)));

			return claimsIdentity;
		}

		public static UserClaims? ClaimsPrincipalToUserClaims(ClaimsPrincipal claimsPrincipal)
		{
			if (!claimsPrincipal.Identity?.IsAuthenticated ?? false)
			{
				return null;
			}

			var firstName = claimsPrincipal.FindFirstValue(CustomClaimTypes.FirstName);
			var familyName = claimsPrincipal.FindFirstValue(CustomClaimTypes.FamilyName);
			var id = claimsPrincipal.FindFirstValue(CustomClaimTypes.Id);
			var profileUrl = claimsPrincipal.FindFirstValue(CustomClaimTypes.ProfileUrl);
			var fullProfileUrl = claimsPrincipal.FindFirstValue(CustomClaimTypes.FullProfileUrl);
			var profileImageUrl = claimsPrincipal.FindFirstValue(CustomClaimTypes.ProfileImageUrl);
			var fullProfileImageUrl = claimsPrincipal.FindFirstValue(CustomClaimTypes.FullProfileImageUrl);
			var roles = claimsPrincipal.FindAll(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToArray();

			// Check that all of the claims exist
			if (string.IsNullOrEmpty(firstName) ||
			    string.IsNullOrEmpty(familyName) ||
			    string.IsNullOrEmpty(id) ||
			    string.IsNullOrEmpty(profileUrl) ||
			    string.IsNullOrEmpty(fullProfileUrl))
			{
				throw new RequestException("Your log in information is corrupted, please log out and log in again.");
			}

			return new UserClaims(
				firstName,
				familyName,
				fullProfileUrl,
				profileUrl,
				fullProfileImageUrl,
				profileImageUrl,
				Guid.Parse(id),
				roles
			);
		}
	}
}