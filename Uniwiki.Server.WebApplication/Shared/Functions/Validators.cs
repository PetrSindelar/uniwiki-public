﻿using System;

namespace Uniwiki.Server.WebApplication.Shared.Functions
{
	public static class Validators
	{
		public static bool IsEnumInvalid<TEnum>(TEnum enumValue)
			where TEnum : struct
		{
			if (!enumValue.GetType().IsEnum)
			{
				throw new Exception();
			}

#pragma warning disable CS8602 // Dereference of a possibly null reference.
			var firstChar = enumValue.ToString()[0];
#pragma warning restore CS8602 // Dereference of a possibly null reference.

			return (firstChar >= '0' && firstChar <= '9') || firstChar == '-';
		}
	}
}