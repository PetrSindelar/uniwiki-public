﻿namespace Uniwiki.Server.WebApplication.Shared.Routes
{
	public class GenericRoute : RouteBase
	{
		public string Url { get; }

		public GenericRoute(string url)
		{
			Url = url;
		}

		public override string Build() => Url;
	}
}