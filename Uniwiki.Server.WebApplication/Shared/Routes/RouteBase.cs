﻿using Uniwiki.Server.WebApplication.Shared.Constants;

namespace Uniwiki.Server.WebApplication.Shared.Routes
{
	public abstract class RouteBase
	{
		// Shortcut to the constants
		public const string ApiRoute = RouteConstants.ApiRoute;
		public const string PageRoute = RouteConstants.PageRoute;
		public const string StaticFileRoute = RouteConstants.StaticFileRoute;
		public const string PartialViewRoute = RouteConstants.PartialViewRoute;

		public abstract string Build();
		public override string ToString() => Build();
	}
}