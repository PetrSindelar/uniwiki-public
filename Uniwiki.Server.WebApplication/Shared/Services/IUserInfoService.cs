﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Uniwiki.Server.WebApplication.Post.EditPostPage;

namespace Uniwiki.Server.WebApplication.Shared.Services
{
	public interface IUserInfoService
	{
		UserInfo? GetUserInfo();
		UserInfo GetUserInfoOrThrow();
		Guid GetUserIdOrThrow();
		Guid? GetUserId();
		Task InitializeAsync(ClaimsPrincipal user);
	}
}