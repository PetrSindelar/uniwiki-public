﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Admin.AddFaculty;
using Uniwiki.Server.WebApplication.Admin.AddUniversity;
using Uniwiki.Server.WebApplication.Post.AddPost;
using Uniwiki.Server.WebApplication.Shared.Authentication;
using Uniwiki.Server.WebApplication.Uni.AddCourse;
using Uniwiki.Server.WebApplication.Uni.AddPostComment;

[assembly: InternalsVisibleTo("Uniwiki.Server.WebApplication.Tests")]

namespace Uniwiki.Server.WebApplication.Shared.Services
{
	[ScopedService]
	public class ServerContentInitiatorService
	{
		private readonly AddFacultyServerAction _addFacultyServerAction;
		private readonly AddCourseServerAction _addCourseServerAction;
		private readonly AddPostServerAction _addPostServerAction;
		private readonly AddPostCommentServerAction _addPostCommentServerAction;
		private readonly RoleManager<IdentityRole<Guid>> _roleManager;
		private readonly AddUniversityServerAction _addUniversityServerAction;
		private readonly UniwikiContext _uniwikiContext;

		public ServerContentInitiatorService(UniwikiContext uniwikiContext,
			AddUniversityServerAction addUniversityServerAction, AddFacultyServerAction addFacultyServerAction,
			AddCourseServerAction addCourseServerAction, AddPostServerAction addPostServerAction,
			AddPostCommentServerAction addPostCommentServerAction, RoleManager<IdentityRole<Guid>> roleManager)
		{
			_uniwikiContext = uniwikiContext;
			_addUniversityServerAction = addUniversityServerAction;
			_addFacultyServerAction = addFacultyServerAction;
			_addCourseServerAction = addCourseServerAction;
			_addPostServerAction = addPostServerAction;
			_addPostCommentServerAction = addPostCommentServerAction;
			_roleManager = roleManager;
		}

		public async Task Initialize()
		{
			await _uniwikiContext.Database.EnsureDeletedAsync();
			await _uniwikiContext.Database.EnsureCreatedAsync();

			// Create roles
			await CustomRoles.AllRoles.ForEachAsync(r => _roleManager.CreateAsync(new IdentityRole<Guid>(r)));

			// Universities
			var cvut = await _addUniversityServerAction.ExecuteAsync(new AddUniversityRequest(
				"České Vysoké Učení Technické",
				"ČVUT", "cvut"));
			var czu = await _addUniversityServerAction.ExecuteAsync(new AddUniversityRequest(
				"Česká Zemědělská Univerzita",
				"ČZU", "czu"));
			var hse = await _addUniversityServerAction.ExecuteAsync(new AddUniversityRequest("Vysoká Škola Ekonomická",
				"VŠE", "vse"));
			await _addUniversityServerAction.ExecuteAsync(new AddUniversityRequest("Higher School of Economics", "HSE",
				"hse"));
			await _addUniversityServerAction.ExecuteAsync(new AddUniversityRequest(
				"Vysoká Škola Chemicko-Technologická",
				"VŠCHT", "vscht"));

			// Faculties
			var cvutFit = await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(
				cvut.UniversityId,
				"Fakulta Informačních Technologií", "FIT",
				"fit", Language.Czech));
			var cvutFel =
				await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(cvut.UniversityId,
					"Fakulta Elektro Technická", "FEL", "fel",
					Language.Czech));
			var cvutFs = await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(
				cvut.UniversityId,
				"Fakulta Strojní", "FS", "fs", Language.Czech));
			var cvutFa =
				await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(cvut.UniversityId,
					"Fakulta Architektury", "FA", "fa", Language.Czech));
			var cvutFsv =
				await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(cvut.UniversityId,
					"Fakulta Stavební", "FSv", "fsv", Language.Czech));
			var hseWe = await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(
				hse.UniversityId,
				"Faculty of World Economy and International Affairs",
				"WE", "we", Language.English));
			var hseLaw = await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(
				hse.UniversityId,
				"Faculty of Law", "FL", "fl", Language.English));
			var czuPef =
				await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(czu.UniversityId,
					"Provozně ekonomická Fakulta", "PEF", "pef",
					Language.Czech));
			var czuFappz = await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(czu.UniversityId,
				"Fakulta agrobiologie, potravinových a přírodních zdrojů", "FAPPZ", "fappz", Language.Czech));
			var czuTf = await _addFacultyServerAction.ExecuteAsync(new AddFacultyRequest(
				czu.UniversityId,
				"Technická fakulta", "TF", "tf", Language.Czech));

			// Users
			var user2 = _uniwikiContext.Users
				.Add(new ProfileModel(Guid.NewGuid(), "pokko@posp.cz", "Petr",
					"Šindelář", "petr-sindelar", null, DateTime.Now, AuthenticationLevel.RegularUser,
					null, 0));
			var user1 = _uniwikiContext.Users
				.Add(new ProfileModel(Guid.NewGuid(), "pokko@pospdd.cz", "Marek",
					"Maly", "marek-maly", null, DateTime.Now, AuthenticationLevel.Admin,
					null, 0));
			var user3 = _uniwikiContext.Users
				.Add(new ProfileModel(Guid.NewGuid(), "pokko@pospdd.cz", "Alena",
					"Zelená", "marek-maly", null, DateTime.Now, AuthenticationLevel.RegularUser,
					null, 0));

			await _uniwikiContext.SaveChangesAsync();


			// Courses
			// CVUT FIT
			await CreateCourse("BI-3D", "3D Tisk", cvut.RedirectRoute.UniversityUrl, cvutFit.RedirectRoute.FacultyUrl,
				user1.Entity.Id);
			await CreateCourse("BI-EMP", "Ekonomické principy a management", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-CS1", "Programování v C# 1", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-CS2", "Programování v C# 2", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-CS3", "Programování v C# 3", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-AAG", "Automaty a gramatiky", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-AG1", "Algoritmy a grafy 1", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-BEZ", "Bezpečnost", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-CAO", "Číslicové a analogové obvody", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-DBS", "Databázové systémy", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-MLO", "Matematická logika", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-PA1", "Programování a algoritmizace 1", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-PA2", "Programování a algoritmizace 2", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			await CreateCourse("BI-OSY", "Operační systémy", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);
			var cvutFitLin = await CreateCourse("BI-LIN", "Lineární algebra", cvut.RedirectRoute.UniversityUrl,
				cvutFit.RedirectRoute.FacultyUrl, user1.Entity.Id);

			// HSE FE courses
			//CreateCourse("", "Economics of Natural Resources", hseWe.Id, aId, hse.Entity.Url, "economics-of-natural-sciences", hseWe.Entity.Url);
			//CreateCourse("", "Game Theory", hseWe.Entity.Id, aId, hse.Entity.Url, "game-theory", hseWe.Entity.Url);
			//CreateCourse("", "Digital Transformation of the World Economy", hseWe.Entity.Id, aId, hse.Entity.Url, "digital-transformation-of-the-world-economy", hseWe.Entity.Url);
			//CreateCourse("", "Mergers, Acquisitions and Restructuring of a Firm", hseWe.Entity.Id, aId, hse.Entity.Url, "mergers-acquisitions-and-restructuring-or-a-firm", hseWe.Entity.Url);
			//CreateCourse("", "Mergers and Acquisitions in Financial Markets", hseWe.Entity.Id, aId, hse.Entity.Url, "economics-of-natural-sciences", hseWe.Entity.Url);
			//CreateCourse("", "Microeconomic Methods of Economic Policy Analysis", hseWe.Entity.Id, aId, hse.Entity.Url, "microeconomics-methods-of-economic-policy-analysis", hseWe.Entity.Url);
			//CreateCourse("", "Models with Qualitative Dependent Variables", hseWe.Entity.Id, aId, hse.Entity.Url, "models-with-qualitative-dependent-variables", hseWe.Entity.Url);
			//CreateCourse("", "Microeconomics: applications", hseWe.Entity.Id, aId, hse.Entity.Url, "microeconomics-applications", hseWe.Entity.Url);
			//CreateCourse("", "Microeconomics", hseWe.Entity.Id, aId, hse.Entity.Url, "microeconomics", hseWe.Entity.Url);
			//CreateCourse("", "Personnel Economics", hseWe.Entity.Id, aId, hse.Entity.Url, "personnel-economics", hseWe.Entity.Url);
			//CreateCourse("", "Personal Money Management", hseWe.Entity.Id, aId, hse.Entity.Url, "personal-money-management", hseWe.Entity.Url);
			//CreateCourse("", "Portfolio Management", hseWe.Entity.Id, aId, hse.Entity.Url, "portfolio-management", hseWe.Entity.Url);
			//CreateCourse("", "Principles of Corporate Finance", hseWe.Entity.Id, aId, hse.Entity.Url, "principles-of-corporate-finance", hseWe.Entity.Url);
			//CreateCourse("", "Econometrics of Program Evaluation", hseWe.Entity.Id, aId, hse.Entity.Url, "econometrics-of-program-evaluation", hseWe.Entity.Url);
			//CreateCourse("", "Empirical Industrial Organisations", hseWe.Entity.Id, aId, hse.Entity.Url, "empirical-industrial-organizations", hseWe.Entity.Url);
			//CreateCourse("", "English for Financiers (Advanced Level)", hseWe.Entity.Id, aId, hse.Entity.Url, "english-for-financiers-advanced-level", hseWe.Entity.Url);
			//CreateCourse("", "Financial Innovation", hseWe.Entity.Id, aId, hse.Entity.Url, "financial-innovation", hseWe.Entity.Url);
			//CreateCourse("", "Financial Markets: Problems and Decisions", hseWe.Entity.Id, aId, hse.Entity.Url, "financial-markets-problems-and-decisions", hseWe.Entity.Url);
			//CreateCourse("", "Fundamental and Technical Analysis", hseWe.Entity.Id, aId, hse.Entity.Url, "fundamental-and-technical-analysis", hseWe.Entity.Url);
			//CreateCourse("", "Advanced Microeconomics", hseWe.Entity.Id, aId, hse.Entity.Url, "advanced-microeconomics", hseWe.Entity.Url);
			//CreateCourse("", "Behavioral Finance", hseWe.Entity.Id, aId, hse.Entity.Url, "behavioral-finance", hseWe.Entity.Url);
			//CreateCourse("", "Effective economics", hseWe.Entity.Id, aId, hse.Entity.Url);

			_ = await _addPostServerAction.ExecuteAsync(new(
				cvutFitLin.UniversityUrl,
				cvutFitLin.FacultyUrl,
				cvutFitLin.CourseUrl,
				"Homework",
				"This is some description of a post.",
				false,
				user1.Entity.Id
			));

			var postModel = _uniwikiContext.Posts.First();

			var request1 = new AddPostCommentRequest(postModel.Id,
				"This is an saassa asasasassa text.",
				user1.Entity.Id);
			var request2 = new AddPostCommentRequest(postModel.Id, "This is an awesome text.",
				user2.Entity.Id);
			var request3 = new AddPostCommentRequest(postModel.Id,
				"This is an awesome text. But a little bit longer. How are you btw.",
				user3.Entity.Id);

			_ = await _addPostCommentServerAction.ExecuteAsync(request2);
			_ = await _addPostCommentServerAction.ExecuteAsync(request2);
			_ = await _addPostCommentServerAction.ExecuteAsync(request1);
			_ = await _addPostCommentServerAction.ExecuteAsync(request2);
			_ = await _addPostCommentServerAction.ExecuteAsync(request3);
			_ = await _addPostCommentServerAction.ExecuteAsync(request3);
			_ = await _addPostCommentServerAction.ExecuteAsync(request2);
			_ = await _addPostCommentServerAction.ExecuteAsync(request1);
			_ = await _addPostCommentServerAction.ExecuteAsync(request2);

			_uniwikiContext.SaveChanges();
		}

		public Task<AddCourseRedirection> CreateCourse(string code, string name, string universityUrl,
			string facultyUrl, Guid authorId)
		{
			var request = new AddCourseRequest(universityUrl, facultyUrl, name, code, authorId);
			return _addCourseServerAction.ExecuteAsync(request);
		}
	}
}