﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Shared.Constants;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.User.GetNotificationsCount;

namespace Uniwiki.Server.WebApplication.Shared.Services
{
	[ScopedService]
	internal class UserInfoService : IUserInfoService
	{
		private readonly GetNotificationsCountServerAction _getNotificationsCountServerAction;
		private readonly ILogger<UserInfoService> _logger;
		private readonly SignInManager<ProfileModel> _signInManager;
		private bool _isInitialized;
		private UserInfo? _userInfo;

		public UserInfoService(GetNotificationsCountServerAction getNotificationsCountServerAction, ILogger<
			UserInfoService> logger, SignInManager<ProfileModel> signInManager)
		{
			_getNotificationsCountServerAction = getNotificationsCountServerAction;
			_logger = logger;
			_signInManager = signInManager;
		}


		public async Task InitializeAsync(ClaimsPrincipal user)
		{
			_isInitialized = true;

			var userClaims = UserClaimsFunctions.ClaimsPrincipalToUserClaims(user);

			if (userClaims == null)
			{
				return;
			}

			try
			{
				var getNotificationsCountRequest = new GetNotificationsCountRequest(userClaims.UserId);

				var notificationsCount =
					await _getNotificationsCountServerAction.ExecuteAsync(getNotificationsCountRequest);

				_userInfo = new UserInfo(userClaims, notificationsCount);
			}
			catch (Exception e)
			{
				_logger.LogError(e,
					$"Error while Initializing the user info for the user ({userClaims}) - going to sign out the user");
				await _signInManager.SignOutAsync();
			}
		}

		public UserInfo? GetUserInfo()
		{
			// Throw error if the user is not initialized
			if (!_isInitialized)
			{
				throw new ServerException(
					debugMessage: $"The {nameof(GetUserInfo)} was called without initialization.");
			}

			return _userInfo;
		}

		public UserInfo GetUserInfoOrThrow()
			=> GetUserInfo() ?? throw new ServerException(
				debugMessage: $"Throwing in '{nameof(GetUserInfoOrThrow)}' in {nameof(UserInfoService)}");

		public Guid GetUserIdOrThrow() => GetUserInfoOrThrow().UserClaims.UserId;
		public Guid? GetUserId() => GetUserInfo()?.UserClaims.UserId;
	}
}