﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Shared.ViewModels
{
	public class GoogleFormPageViewModel : PageLayoutViewModel
	{
		public string FormUrl { get; }

		public GoogleFormPageViewModel(RouteBase? backRoute, string title, string formUrl) : base(backRoute, title)
		{
			FormUrl = formUrl;
		}
	}
}