﻿namespace Uniwiki.Server.WebApplication.Shared.ViewModels
{
	public abstract class ModalViewModel
	{
		public string Title { get; }
		public string ModalId { get; }
		public bool IsClosableOnBackgroundClick { get; }

		protected ModalViewModel(string title, string modalId, bool isClosableOnBackgroundClick = true)
		{
			Title = title;
			ModalId = modalId;
			IsClosableOnBackgroundClick = isClosableOnBackgroundClick;
		}
	}
}