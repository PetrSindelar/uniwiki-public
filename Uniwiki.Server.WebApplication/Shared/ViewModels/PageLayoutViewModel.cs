﻿using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Shared.ViewModels
{
	public class PageLayoutViewModel : LayoutViewModel
	{
		/// <summary>
		/// The route, that the back button in the top of the page will direct to. If left to null, then there will be called a browser back button (via JS as window.history.back())
		/// </summary>
		public RouteBase? BackRoute { get; }

		public string Title { get; }

		public PageLayoutViewModel(RouteBase? backRoute, string title)
		{
			BackRoute = backRoute;
			Title = title;
		}
	}
}