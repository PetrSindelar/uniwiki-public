﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Shared.ViewModels
{
	public class TopBarComponentViewModel
	{
		public RouteBase BackRoute { get; }
		public string Title { get; }

		public TopBarComponentViewModel(RouteBase backRoute, string title)
		{
			BackRoute = backRoute;
			Title = title;
		}
	}
}