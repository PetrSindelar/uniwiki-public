﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;

namespace Uniwiki.Server.WebApplication.Uni.AddCourse
{
	internal static class AddCourseAccessors
	{
		public static Func<UniwikiContext, Task<Guid>> GetFacultyId(string universityUrl, string facultyUrl)
			=> uniwikiContext
				=> uniwikiContext.Faculties
					.Include(f => f.University)
					.Where(f => f.University.Url == universityUrl && f.Url == facultyUrl)
					.Select(f => f.Id)
					.FirstAsync();

		public static Func<UniwikiContext, Task<bool>> IsCourseUrlUnique(string courseUrl)
			=> uniwikiContext
				=> uniwikiContext
					.Courses
					.AllAsync(c => c.Url != courseUrl);

		public static Func<UniwikiContext, Task> AddCourse(CourseModel course)
			=> uniwikiContext
				=> uniwikiContext
					.Courses
					.Add(course)
					.Context
					.SaveChangesAsync();
	}
}