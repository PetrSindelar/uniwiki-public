﻿using Uniwiki.Server.WebApplication.Uni.CoursePage;

namespace Uniwiki.Server.WebApplication.Uni.AddCourse
{
	public class AddCourseRedirection
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public CoursePageRoute RedirectRoute { get; }

		public AddCourseRedirection(string universityUrl, string facultyUrl, string courseUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			RedirectRoute = new CoursePageRoute(universityUrl, facultyUrl, courseUrl);
		}
	}
}