﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.AddCourse
{
	public readonly struct AddCourseRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseName { get; }
		public string? CourseCode { get; }
		public Guid CourseAuthorId { get; }

		public AddCourseRequest(string universityUrl, string facultyUrl, string courseName, string? courseCode,
			Guid courseAuthorId)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseName = courseName;
			CourseCode = courseCode;
			CourseAuthorId = courseAuthorId;
		}
	}
}