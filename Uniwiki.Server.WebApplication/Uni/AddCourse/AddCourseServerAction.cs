﻿using System;
using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.AddCourse
{
	[ScopedService]
	public class AddCourseServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddCourseServerAction(UniwikiContext uniwikiContext) => _uniwikiContext = uniwikiContext;

		public async Task<AddCourseRedirection> ExecuteAsync(AddCourseRequest request)
		{
			request = AddCourseStandardizer.Standardize(request);
			AddCourseValidator.Validate(request);

			var searchStandardizedFullName = Standardizers.StandardizeSearchText(request.CourseName);
			var searchStandardizedCode = request.CourseCode?.Apply(Standardizers.StandardizeSearchText);

			var facultyId =
				await AddCourseAccessors.GetFacultyId(request.UniversityUrl, request.FacultyUrl)(_uniwikiContext);

			var courseUrl = await Standardizers.CreateUrl(request.CourseName,
				url => AddCourseAccessors.IsCourseUrlUnique(url)(_uniwikiContext));

			var course = new CourseModel(Guid.NewGuid(), request.CourseCode, searchStandardizedCode, request.CourseName,
				searchStandardizedFullName, request.CourseAuthorId, facultyId, courseUrl, courseUrl);

			await AddCourseAccessors.AddCourse(course)(_uniwikiContext);

			return new AddCourseRedirection(request.UniversityUrl, request.FacultyUrl, course.Url);
		}
	}
}