﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.AddCourse
{
	internal static class AddCourseStandardizer
	{
		public static AddCourseRequest Standardize(AddCourseRequest request) => new AddCourseRequest(
			request.UniversityUrl.Apply(Standardizers.StandardizeUrl),
			request.FacultyUrl.Apply(Standardizers.StandardizeUrl),
			request.CourseName.Apply(Standardizers.StandardizeName),
			request.CourseCode?.Apply(Standardizers.OptimizeWhiteSpaces).Apply(Standardizers.FirstCharToUpper),
			request.CourseAuthorId);
	}
}