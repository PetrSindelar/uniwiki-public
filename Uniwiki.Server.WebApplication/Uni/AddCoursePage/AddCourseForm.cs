﻿using System.ComponentModel.DataAnnotations;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.WebApplication.Uni.AddCoursePage
{
	public class AddCourseForm
	{
		[Required]
		[MaxLength(Constants.Validations.CourseNameMaxLength)]
		[MinLength(Constants.Validations.CourseNameMinLength)]
		public string CourseName { get; set; } = null!;

		[MaxLength(Constants.Validations.CourseCodeMaxLength)]
		[MinLength(Constants.Validations.CourseCodeMinLength)]

		public string? CourseCode { get; set; }

		public AddCourseForm()
		{
		}

		public AddCourseForm(string courseName, string? courseCode)
		{
			CourseName = courseName;
			CourseCode = courseCode;
		}
	}
}