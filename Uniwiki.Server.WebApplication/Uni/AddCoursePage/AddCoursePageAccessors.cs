﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Uni.AddCoursePage
{
	internal static class AddCoursePageAccessors
	{
		public static Func<UniwikiContext, Task<(string universityShortName, string facultyShortName)>> GetFaculty(
			string universityUrl, string facultyUrl)
			=> async uniwikiContext
				=> (await uniwikiContext.Faculties
					.Include(f => f.University)
					.Where(f => f.University.Url == universityUrl && f.Url == facultyUrl)
					.Select(f => new { UniversityShortName = f.University.ShortName, FacultyShortName = f.ShortName })
					.FirstAsync())
				.Apply(f => (f.UniversityShortName, f.FacultyShortName));
	}
}