﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.AddCoursePage
{
	public class AddCoursePageRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public Guid UserId { get; }

		public AddCoursePageRequest(string universityUrl, string facultyUrl, Guid userId)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			UserId = userId;
		}
	}
}