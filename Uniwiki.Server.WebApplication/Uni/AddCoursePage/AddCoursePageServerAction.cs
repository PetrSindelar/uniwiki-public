﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.Uni.AddCoursePage
{
	[ScopedService]
	public class AddCoursePageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public AddCoursePageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<AddCoursePageViewModel> ExecuteAsync(AddCoursePageRequest request)
		{
			var faculty =
				await AddCoursePageAccessors.GetFaculty(request.UniversityUrl, request.FacultyUrl)(_uniwikiContext);

			var viewModel = new AddCoursePageViewModel(
				request.UniversityUrl,
				request.FacultyUrl,
				faculty.universityShortName,
				faculty.facultyShortName);

			return viewModel;
		}
	}
}