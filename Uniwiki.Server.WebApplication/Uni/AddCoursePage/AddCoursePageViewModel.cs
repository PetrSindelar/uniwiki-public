﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.AddCourse;
using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;

namespace Uniwiki.Server.WebApplication.Uni.AddCoursePage
{
	public class AddCoursePageViewModel : PageLayoutViewModel
	{
		public string UniversityShortName { get; }
		public string FacultyShortName { get; }
		public AddCourseForm AddCourseForm { get; }
		public AddCourseRoute AddCourseRoute { get; }

		public AddCoursePageViewModel(string universityUrl, string facultyUrl, string universityShortName,
			string facultyShortName)
			: base(
				new SelectCoursePageRoute(universityUrl, facultyUrl),
				$"Add a course to {universityShortName} - {facultyShortName}")
		{
			UniversityShortName = universityShortName;
			FacultyShortName = facultyShortName;
			AddCourseForm = new AddCourseForm(string.Empty, string.Empty);
			AddCourseRoute = new AddCourseRoute(universityUrl, facultyUrl);
		}
	}
}