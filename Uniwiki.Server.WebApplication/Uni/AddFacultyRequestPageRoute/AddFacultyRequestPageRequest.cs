﻿namespace Uniwiki.Server.WebApplication.Uni.AddFacultyRequestPageRoute
{
	public class AddFacultyRequestPageRequest
	{
		public string UniversityUrl { get; }

		public AddFacultyRequestPageRequest(string universityUrl)
		{
			UniversityUrl = universityUrl;
		}
	}
}