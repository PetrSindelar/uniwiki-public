﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.AddFacultyRequestPageRoute
{
	public class AddFacultyRequestPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/AddFacultyRequest";

		public const string UniversityUrlRouteParameter = "UniversityUrlRouteParameter";
		public const string RouteAttribute = BaseRoute + "/{" + UniversityUrlRouteParameter + "}";

		public string UniversityUrl { get; }

		public AddFacultyRequestPageRoute(string universityUrl)
		{
			UniversityUrl = universityUrl;
		}

		public override string Build() => $"{BaseRoute}" + "/" + UniversityUrl;
	}
}