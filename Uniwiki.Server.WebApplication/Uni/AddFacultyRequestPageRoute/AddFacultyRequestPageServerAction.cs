﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Uni.AddFacultyRequestPageRoute
{
	[ScopedService]
	public class AddFacultyRequestPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IOptions<FormsOptions> _formsOptions;

		public AddFacultyRequestPageServerAction(UniwikiContext uniwikiContext, IOptions<FormsOptions> formsOptions)
		{
			_uniwikiContext = uniwikiContext;
			_formsOptions = formsOptions;
		}

		public async Task<GoogleFormPageViewModel> ExecuteAsync(AddFacultyRequestPageRequest request)
		{
			var universityLongName = await _uniwikiContext.Universities.Where(u => u.Url == request.UniversityUrl)
				.Select(u => u.LongName).FirstAsync();


			var viewModel = new GoogleFormPageViewModel(
				null,
				"Add a faculty request",
				_formsOptions.Value.GetAddFacultyRequestForm(universityLongName));

			return viewModel;
		}
	}
}