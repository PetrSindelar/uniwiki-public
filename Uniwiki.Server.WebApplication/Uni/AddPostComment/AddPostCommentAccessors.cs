﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Uni.AddPostComment
{
	public class AddPostCommentAccessors
	{
		public static Func<UniwikiContext,
				Task<(string FirstName, string FamilyName, string Url, string ImageUrl, Guid Id)>>
			GetProfile(
				Guid profileId)
			=> async uniwikiContext =>
				(await uniwikiContext
					.Users
					.Where(p => p.Id == profileId)
					.Select(p => new { p.FirstName, p.FamilyName, p.Url, p.ProfileImageUrl, p.Id })
					.FirstAsync())
				.Apply(p => (p.FirstName, p.FamilyName, p.Url, p.ProfileImageUrl, p.Id));
	}
}