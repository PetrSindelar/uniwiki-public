﻿using System.ComponentModel.DataAnnotations;

namespace Uniwiki.Server.WebApplication.Uni.AddPostComment
{
	public class AddPostCommentForm
	{
		[Required] public string Text { get; set; } = null!;
	}
}