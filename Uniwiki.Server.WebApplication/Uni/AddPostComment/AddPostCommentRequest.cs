﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.AddPostComment
{
	public class AddPostCommentRequest
	{
		public Guid PostId { get; }
		public string Text { get; }
		public Guid UserId { get; }

		public AddPostCommentRequest(Guid postId, string text, Guid userId)
		{
			PostId = postId;
			Text = text;
			UserId = userId;
		}
	}
}