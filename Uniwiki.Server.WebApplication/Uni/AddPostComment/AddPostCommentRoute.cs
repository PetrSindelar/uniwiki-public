﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.AddPostComment
{
	public class AddPostCommentRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/AddPostComment";

		public const string PostIdRouteParameter = "postId";

		public const string RouteAttribute = BaseRoute + "/{" +
		                                     PostIdRouteParameter + "}";

		public Guid PostId { get; }

		public AddPostCommentRoute(Guid postId)
		{
			PostId = postId;
		}

		public override string Build() => $"{BaseRoute}/{PostId}";
	}
}