﻿using System;
using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Uni.PostPage;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Uni.AddPostComment
{
	[ScopedService]
	public class AddPostCommentServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public AddPostCommentServerAction(UniwikiContext uniwikiContext, ITimeService timeService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task<PostCommentViewModel> ExecuteAsync(AddPostCommentRequest request)
		{
			// Standardize
			request = AddPostCommentStandardizer.Standardize(request);

			var author = await AddPostCommentAccessors.GetProfile(request.UserId)(_uniwikiContext);

			var commentModel = new PostCommentModel(Guid.NewGuid(), request.UserId, request.PostId, request.Text,
				_timeService.Now);

			_uniwikiContext.PostComments.Add(commentModel);
			await _uniwikiContext.SaveChangesAsync();

			var viewModel =
				new PostCommentViewModel(
					author.Id,
					author.FirstName,
					author.FamilyName,
					author.Url,
					author.ImageUrl,
					commentModel.CreationTime,
					commentModel.Text,
					0,
					false,
					commentModel.Id,
					_timeService.Now,
					request.UserId);

			return viewModel;
		}
	}
}