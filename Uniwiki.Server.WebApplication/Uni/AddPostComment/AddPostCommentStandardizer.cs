﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.AddPostComment
{
	public static class AddPostCommentStandardizer
	{
		public static AddPostCommentRequest Standardize(AddPostCommentRequest request)
			=> new(
				request.PostId,
				request.Text.Apply(Standardizers.FirstCharToUpper),
				request.UserId
			);
	}
}