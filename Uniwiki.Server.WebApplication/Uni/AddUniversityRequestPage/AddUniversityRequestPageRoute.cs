﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.AddUniversityRequestPage
{
	public class AddUniversityRequestPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/AddUniversityRequest";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => $"{BaseRoute}";
	}
}