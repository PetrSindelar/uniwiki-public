﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.HomePage;

namespace Uniwiki.Server.WebApplication.Uni.AddUniversityRequestPage
{
	[ScopedService]
	public class AddUniversityRequestPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IOptions<FormsOptions> _formsOptions;

		public AddUniversityRequestPageServerAction(UniwikiContext uniwikiContext, IOptions<FormsOptions> formsOptions)
		{
			_uniwikiContext = uniwikiContext;
			_formsOptions = formsOptions;
		}

		public Task<GoogleFormPageViewModel> ExecuteAsync(AddUniversityRequestPageRequest request)
		{
			var viewModel = new GoogleFormPageViewModel(new HomePageRoute(), "Add a university request",
				_formsOptions.Value.GetAddUniversityRequestForm());

			return Task.FromResult(viewModel);
		}
	}
}