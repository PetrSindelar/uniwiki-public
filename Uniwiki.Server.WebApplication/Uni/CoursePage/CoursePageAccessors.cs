﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Uni.PostPage;

namespace Uniwiki.Server.WebApplication.Uni.CoursePage
{
	public static class CoursePageAccessors
	{
		public static Func<UniwikiContext, Task<(Guid CourseId, string CourseName)>> GetCourseIdFromUrl(
			string universityUrl, string facultyUrl,
			string courseUrl)
			=> async uniwikiContext =>
			{
				var value = await uniwikiContext
					.Courses
					.Include(c => c.Faculty)
					.ThenInclude(c => c.University)
					.Where(c => c.Faculty.University.Url == universityUrl && c.Faculty.Url == facultyUrl &&
					            c.Url == courseUrl)
					.Select(c => new { c.Id, c.LongName })
					.FirstOrDefaultAsync();

				if (value == null)
					throw new NotFoundException("The course has not been found.");

				return
					value.Apply(val => (val.Id, val.LongName));
			};


		public static Func<UniwikiContext, Task<Guid[]>> GetPostIdsLikedByUser(Guid courseId, Guid userId)
			=> uniwikiContext =>
				uniwikiContext
					.Posts
					.Include(p => p.Likes)
					.Where(p => p.CourseId == courseId && p.Likes.Any(l => l.ProfileId == userId))
					.Select(p => p.Id)
					.ToArrayAsync();


		public static Func<UniwikiContext, Task<PostItemViewModel[]>> GetPostItems(Guid courseId,
			Guid? userId, DateTime currentTime)
			=> async uniwikiContext =>
			{
				var postsQuery = uniwikiContext.Posts
					.Include(p => p.Author)
					.Include(p => p.PostFiles)
					.Include(p => p.Course)
					.ThenInclude(p => p.Faculty)
					.ThenInclude(p => p.University)
					.Where(p => p.CourseId == courseId)
					.Select(p =>
						new PostItemViewModel(
							p.Course.Faculty.University.Url, p.Course.Faculty.Url,
							p.Course.Url,
							p.Url,
							p.Author.FirstName,
							p.Author.FamilyName,
							p.Author.Url,
							p.Author.ProfileImageUrl,
							p.Category,
							false, // TODO: Notifications
							p.Text,
							p.PostFiles.Count,
							p.PostFiles
								.Where(pf => pf.IsImage)
								.Select(pf => new PostFileItemViewModel(
									pf.OriginalFullName,
									pf.Size,
									pf.Url
								))
								//.Take(2)
								.ToArray(),
							p.Likes.Count(l => l.IsLiked),
							p.Comments.Count,
							p.Comments.Any(c => c.AuthorId == userId),
							p.Likes.Any(l => l.ProfileId == userId && l.IsLiked),
							currentTime,
							p.CreationTime
						))
					.Take(Constants.MaxPostsToFetch);

				Console.WriteLine(postsQuery.ToQueryString());

				var posts = await postsQuery.ToArrayAsync();

				return posts;
			};
	}
}