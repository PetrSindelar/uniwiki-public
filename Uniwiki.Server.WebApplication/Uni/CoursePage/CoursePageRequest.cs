﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.CoursePage
{
	public class CoursePageRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public Guid? UserId { get; }

		public CoursePageRequest(string universityUrl, string facultyUrl, string courseUrl, Guid? userId)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			UserId = userId;
		}
	}
}