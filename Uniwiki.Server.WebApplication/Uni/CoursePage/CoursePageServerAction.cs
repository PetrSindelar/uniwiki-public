﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Uni.CoursePage
{
	[ScopedService]
	public class CoursePageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public CoursePageServerAction(UniwikiContext uniwikiContext, ITimeService timeService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task<CoursePageViewModel> ExecuteAsync(CoursePageRequest request)
		{
			// Standardize
			request = CoursePageStandardizer.Standardize(request);

			var course =
				await CoursePageAccessors.GetCourseIdFromUrl(request.UniversityUrl, request.FacultyUrl,
					request.CourseUrl)(
					_uniwikiContext);

			var postItems =
				await CoursePageAccessors.GetPostItems(course.CourseId, request.UserId, _timeService.Now)(
					_uniwikiContext);

			var viewModel =
				new CoursePageViewModel(course.CourseId, request.UniversityUrl, request.FacultyUrl, request.CourseUrl,
					course.CourseName,
					postItems);


			return viewModel;
		}
	}
}