﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.CoursePage
{
	public static class CoursePageStandardizer
	{
		public static CoursePageRequest Standardize(CoursePageRequest request)
			=> new CoursePageRequest(
				request.UniversityUrl.Apply(Standardizers.StandardizeUrl),
				request.FacultyUrl.Apply(Standardizers.StandardizeUrl),
				request.CourseUrl.Apply(Standardizers.StandardizeUrl),
				request.UserId
			);
	}
}