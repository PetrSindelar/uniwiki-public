﻿using System;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.RenameCourseRequestPage;
using Uniwiki.Server.WebApplication.Uni.ReportCourseRequestPage;
using Uniwiki.Server.WebApplication.Uni.ReportPostRequestPage;
using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;

namespace Uniwiki.Server.WebApplication.Uni.CoursePage
{
	public class CoursePageViewModel : PageLayoutViewModel
	{
		public AddPostPageRoute AddPostRoute { get; }
		public ReportCourseRequestPageRoute ReportCourseRequestPageRoute { get; }
		public PostItemViewModel[] PostItems { get; }
		public RenameCourseRequestPageRoute RenameCourseRequestPageRoute { get; }

		public CoursePageViewModel(
			Guid courseId,
			string universityUrl,
			string facultyUrl,
			string courseUrl,
			string courseName,
			PostItemViewModel[] postItems)
			: base(
				new SelectCoursePageRoute(universityUrl, facultyUrl),
				"Study resources in " + courseName
			)
		{
			AddPostRoute = new AddPostPageRoute(universityUrl, facultyUrl, courseUrl);
			ReportCourseRequestPageRoute = new ReportCourseRequestPageRoute(courseId);
			PostItems = postItems;
			RenameCourseRequestPageRoute = new RenameCourseRequestPageRoute(courseId);
		}
	}
}