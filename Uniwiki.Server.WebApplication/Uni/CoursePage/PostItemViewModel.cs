﻿using System;
using System.Linq;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.Uni.PostPage;
using Uniwiki.Server.WebApplication.Uni.Shared;

namespace Uniwiki.Server.WebApplication.Uni.CoursePage
{
	public class PostItemViewModel
	{
		public string AuthorFullName { get; }
		public string? Category { get; }
		public bool NotificationsOn { get; }
		public string Text { get; }
		public bool TextIsTrimmed { get; }
		public int FilesCount { get; }
		public PostFileItemViewModel[] PostImages { get; }
		public PostBarViewModel PostBarViewModel { get; set; }
		public PostPageRoute PostPageRoute { get; }
		public PostPageRoute PostPageRouteComments { get; }
		public PostPageRoute PostPageRouteFiles { get; }
		public ProfilePageRoute AuthorRoute { get; }
		public ProfileImageRoute? AuthorImageRoute { get; }
		public string CreationTime { get; }

		public PostItemViewModel(string universityUrl, string facultyUrl, string courseUrl, string postUrl,
			string authorFirstName, string authorFamilyName, string authorUrl, string? authorImageUrl, string? category,
			bool notificationsOn, string text, int filesCount, PostFileItemViewModel[] postImages,
			int likesCount, int commentsCount, bool isCommentedByUser, bool isLikedByUser, DateTime currentTime,
			DateTime creationTime)
		{
			AuthorFullName = authorFirstName + " " + authorFamilyName;
			Category = category;
			NotificationsOn = notificationsOn;
			Text = TrimPostText(text);
			TextIsTrimmed = text.Length != Text.Length;
			FilesCount = filesCount;
			PostImages = postImages;
			CreationTime = Converters.ToPassedDayTime(currentTime, creationTime);
			PostBarViewModel = new(
				isCommentedByUser,
				commentsCount,
				isLikedByUser,
				likesCount,
				universityUrl,
				facultyUrl,
				courseUrl,
				postUrl);
			PostPageRoute = new(universityUrl, facultyUrl, courseUrl, postUrl);
			PostPageRouteComments = new(universityUrl, facultyUrl, courseUrl, postUrl, true);
			PostPageRouteFiles = new(universityUrl, facultyUrl, courseUrl, postUrl);
			AuthorRoute = new(authorUrl);
			AuthorImageRoute = authorImageUrl == null ? null : new(authorImageUrl);
		}

		private static string TrimPostText(string originalText)
		{
			var modifiedText = originalText;

			// Try to trim to maximum lines
			var linesCountMax = 4;
			var allowedNewLineCharactersCount = linesCountMax - 1;
			var newLineCharactersCount = modifiedText.Count(ch => ch == '\n');

			if (newLineCharactersCount > allowedNewLineCharactersCount)
			{
				var newLinesCount = 0;
				for (var i = 0; i < modifiedText.Length; i++)
				{
					if (modifiedText[i] == '\n')
					{
						newLinesCount++;
						if (newLinesCount == allowedNewLineCharactersCount + 1)
						{
							modifiedText = modifiedText.Substring(0, i);
						}
					}
				}
			}

			var maxLength = 350;

			if (modifiedText.Length <= maxLength)
			{
				// Check if the text has been modified
				if (originalText.Length == modifiedText.Length)
				{
					return modifiedText;
				}

				return modifiedText + "...";
			}

			var lastSpaceBeforeMax = modifiedText.LastIndexOfAny(new[] { ' ', '\t', '\n' }, maxLength);
			if (lastSpaceBeforeMax == -1)
			{
				return modifiedText.Substring(0, maxLength) + "...";
			}

			return modifiedText.Substring(0, lastSpaceBeforeMax) + "...";
		}
	}
}