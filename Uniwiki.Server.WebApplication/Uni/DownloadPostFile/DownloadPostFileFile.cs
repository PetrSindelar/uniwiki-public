﻿namespace Uniwiki.Server.WebApplication.Uni.DownloadPostFile
{
	public class DownloadPostFileFile
	{
		public string FilePath { get; }
		public string FileName { get; }
		public string ContentType { get; }

		public DownloadPostFileFile(string filePath, string fileName, string contentType)
		{
			FilePath = filePath;
			FileName = fileName;
			ContentType = contentType;
		}
	}
}