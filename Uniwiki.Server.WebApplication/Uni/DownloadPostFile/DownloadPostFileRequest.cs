﻿namespace Uniwiki.Server.WebApplication.Uni.DownloadPostFile
{
	public class DownloadPostFileRequest
	{
		public string PostFileUrl { get; }
		public string FileStoragePath { get; }

		public DownloadPostFileRequest(string postFileUrl, string fileStoragePath)
		{
			PostFileUrl = postFileUrl;
			FileStoragePath = fileStoragePath;
		}
	}
}