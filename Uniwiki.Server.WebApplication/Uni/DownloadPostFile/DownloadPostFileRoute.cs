﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.DownloadPostFile
{
	public class DownloadPostFileRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/DownloadPostFile";
		public const string PostFileUrlRouteParameter = "postFileUrl";

		public const string RouteAttribute = BaseRoute + "/{" + PostFileUrlRouteParameter + "}";

		public string PostFileUrl { get; }

		public DownloadPostFileRoute(string postFileUrl)
		{
			PostFileUrl = postFileUrl;
		}

		public override string Build() => $"{BaseRoute}/{PostFileUrl}";
	}
}