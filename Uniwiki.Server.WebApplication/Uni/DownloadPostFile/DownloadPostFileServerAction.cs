﻿using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Uni.DownloadPostFile
{
	[ScopedService]
	public class DownloadPostFileServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public DownloadPostFileServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		// Get and serve the file
		public async Task<DownloadPostFileFile> ExecuteAsync(DownloadPostFileRequest request)
		{
			// Find the file in DB
			var storagePath = (await _uniwikiContext
					.PostFiles
					.Include(f => f.Course)
					.ThenInclude(f => f.Faculty)
					.ThenInclude(f => f.University)
					.Where(pf => pf.Url == request.PostFileUrl)
					.Select(pf => new
					{
						CourseStoragePath = pf.Course.StoragePath,
						FacultyStoragePath = pf.Course.Faculty.StoragePath,
						UniversityStoragePath = pf.Course.Faculty.University.StoragePath,
						FileName = pf.NameWithoutExtension + pf.Extension
					})
					.FirstAsync())
				.Apply(storage
					=> (storage.CourseStoragePath, storage.FacultyStoragePath, storage.UniversityStoragePath,
						storage.FileName));

			// Construct full path for the file
			var path = Path.Combine(request.FileStoragePath, storagePath.UniversityStoragePath,
				storagePath.FacultyStoragePath, storagePath.CourseStoragePath, request.PostFileUrl);

			// If the file does not exist
			if (!File.Exists(path))
			{
				// Throw error
				throw new NotFoundException("File not found.");
			}

			var viewModel = new DownloadPostFileFile(path, storagePath.FileName, MediaTypeNames.Application.Octet);

			return viewModel;
		}
	}
}