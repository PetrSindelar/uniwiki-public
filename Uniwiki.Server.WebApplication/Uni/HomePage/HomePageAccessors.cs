﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;

namespace Uniwiki.Server.WebApplication.Uni.HomePage
{
	internal static class HomePageAccessors
	{
		public static Func<UniwikiContext, Task<UniversityItemViewModel[]>> GetUniversityItems
			=> uniwikiContext
				=> uniwikiContext.Universities
					.Select(u => new UniversityItemViewModel(u.Url, u.LongName, u.ShortName))
					.ToArrayAsync();
	}
}