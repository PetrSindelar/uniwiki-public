﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.HomePage
{
	public class HomePageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Home";
		public const string RouteAttribute = BaseRoute;

		public override string Build() => BaseRoute;
	}
}