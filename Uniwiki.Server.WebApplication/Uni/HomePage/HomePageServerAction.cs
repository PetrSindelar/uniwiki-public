﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.Uni.HomePage
{
	[ScopedService]
	public class HomePageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public HomePageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<HomePageViewModel> ExecuteAsync()
		{
			var universityItems = await HomePageAccessors.GetUniversityItems(_uniwikiContext);

			return new HomePageViewModel(universityItems);
		}
	}
}