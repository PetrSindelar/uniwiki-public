﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.AddUniversityRequestPage;

namespace Uniwiki.Server.WebApplication.Uni.HomePage
{
	public class HomePageViewModel : LayoutViewModel
	{
		public UniversityItemViewModel[] Universities { get; }
		public AddUniversityRequestPageRoute AddUniversityRequestPageRoute { get; }

		public HomePageViewModel(UniversityItemViewModel[] universities)
		{
			Universities = universities;
			AddUniversityRequestPageRoute = new();
		}
	}
}