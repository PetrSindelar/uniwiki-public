﻿using Uniwiki.Server.WebApplication.Uni.SelectFacultyPage;

namespace Uniwiki.Server.WebApplication.Uni.HomePage
{
	public class UniversityItemViewModel
	{
		private readonly string _longName;
		private readonly string _shortName;
		public SelectFacultyPageRoute Url { get; }
		public string DisplayName => _shortName + " - " + _longName;

		public UniversityItemViewModel(string url, string longName, string shortName)
		{
			Url = new SelectFacultyPageRoute(url);
			_longName = longName;
			_shortName = shortName;
		}
	}
}