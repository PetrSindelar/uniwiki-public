﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;

namespace Uniwiki.Server.WebApplication.Uni.LikePost
{
	internal static class LikePostAccessors
	{
		public static Func<UniwikiContext, Task<Guid>> GetPostIdFromUrl(string universityUrl, string facultyUrl,
			string courseUrl, string postUrl)
			=> uniwikiContext
				=> uniwikiContext
					.Posts
					.Include(p => p.Course)
					.ThenInclude(c => c.Faculty)
					.ThenInclude(f => f.University)
					.Where(p => p.Course.Faculty.University.Url == universityUrl &&
					            p.Course.Faculty.Url == facultyUrl &&
					            p.Course.Url == courseUrl &&
					            p.Url == postUrl
					)
					.Select(p => p.Id)
					.FirstAsync();
	}
}