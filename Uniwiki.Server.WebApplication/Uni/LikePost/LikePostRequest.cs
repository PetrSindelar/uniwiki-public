﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.LikePost
{
	public class LikePostRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string PostUrl { get; }
		public bool IsLike { get; }
		public Guid UserId { get; }

		public LikePostRequest(string universityUrl, string facultyUrl, string courseUrl, string postUrl, bool isLike,
			Guid userId)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostUrl = postUrl;
			IsLike = isLike;
			UserId = userId;
		}
	}
}