﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.LikePost
{
	public class LikePostRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/LikePost";
		public const string UniversityRouteParameter = "universityUrl";
		public const string FacultyRouteParameter = "facultyUrl";
		public const string CourseRouteParameter = "courseUrl";
		public const string PostRouteParameter = "postUrl";
		public const string IsLikeRouteParameter = "isLike";

		public const string RouteAttribute = BaseRoute + "/{" +
		                                     UniversityRouteParameter + "}/{" +
		                                     FacultyRouteParameter + "}/{" +
		                                     CourseRouteParameter + "}/{" +
		                                     PostRouteParameter + "}/{" +
		                                     IsLikeRouteParameter + "}";

		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string PostUrl { get; }
		public bool IsLike { get; }

		public LikePostRoute(string universityUrl, string facultyUrl, string courseUrl, string postUrl, bool isLike)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostUrl = postUrl;
			IsLike = isLike;
		}

		public override string Build() => $"{BaseRoute}/{UniversityUrl}/{FacultyUrl}/{CourseUrl}/{PostUrl}/{IsLike}";
	}
}