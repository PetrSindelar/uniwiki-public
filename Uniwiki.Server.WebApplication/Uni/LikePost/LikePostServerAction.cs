﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.ModelIds;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Uni.LikePost
{
	[ScopedService]
	public class LikePostServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public LikePostServerAction(UniwikiContext uniwikiContext, ITimeService timeService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task ExecuteAsync(LikePostRequest request)
		{
			// Find the post to like
			var postId = await LikePostAccessors.GetPostIdFromUrl(request.UniversityUrl, request.FacultyUrl,
				request.CourseUrl, request.PostUrl)(_uniwikiContext);

			// Check is there is a like in the DB
			var existingLike =
				await _uniwikiContext.PostLikes.FindAsync(new PostLikeModelId(postId, request.UserId).GetKeyValues());

			// If user wants to like the post
			if (request.IsLike)
			{
				// If the post is already liked
				if (existingLike != null && existingLike.IsLiked)
				{
					// Do nothing
					return;
				}

				// If there is no like yet
				if (existingLike == null)
				{
					// Create a new like
					existingLike = new(postId, request.UserId, _timeService.Now, true);
					_uniwikiContext.PostLikes.Add(existingLike);
				}
				else
				{
					// Set IsLiked to true
					existingLike.Like();
				}
			}
			// If user wants to remove the like from the post
			else
			{
				// If the post is already not liked
				if (existingLike == null || !existingLike.IsLiked)
				{
					// Do nothing
					return;
				}

				// Set the like to IsLiked = false
				existingLike.IsLiked = false;
			}

			// Save the changes
			await _uniwikiContext.SaveChangesAsync();
		}
	}
}