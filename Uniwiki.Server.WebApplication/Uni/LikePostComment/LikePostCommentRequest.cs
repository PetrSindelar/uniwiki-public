﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.LikePostComment
{
	public class LikePostCommentRequest
	{
		public Guid PostCommentId { get; }
		public bool IsLike { get; }
		public Guid UserId { get; }

		public LikePostCommentRequest(Guid postCommentId,
			bool isLike,
			Guid userId)
		{
			PostCommentId = postCommentId;
			IsLike = isLike;
			UserId = userId;
		}
	}
}