﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.LikePostComment
{
	public class LikePostCommentRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/LikePostComment";
		public const string PostCommentIdRouteParameter = "postCommentId";
		public const string IsLikeRouteParameter = "isLike";

		public const string RouteAttribute = BaseRoute + "/{" +
		                                     PostCommentIdRouteParameter + "}/{" +
		                                     IsLikeRouteParameter + "}";

		public Guid PostCommentId { get; }
		public bool IsLike { get; }

		public LikePostCommentRoute(Guid postCommentId, bool isLike)
		{
			PostCommentId = postCommentId;
			IsLike = isLike;
		}

		public override string Build() => $"{BaseRoute}/{PostCommentId}/{IsLike}";
	}
}