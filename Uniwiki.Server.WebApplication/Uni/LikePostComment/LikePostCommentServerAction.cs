﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.ModelIds;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Uni.LikePostComment
{
	[ScopedService]
	public class LikePostCommentServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public LikePostCommentServerAction(UniwikiContext uniwikiContext, ITimeService timeService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task ExecuteAsync(LikePostCommentRequest request)
		{
			// Check is there is a like in the DB
			var existingLike =
				await _uniwikiContext.PostCommentLikes.FindAsync(
					new PostCommentLikeModelId(request.PostCommentId, request.UserId).GetKeyValues());

			// If user wants to like the post
			if (request.IsLike)
			{
				// If the post is already liked
				if (existingLike != null && existingLike.IsLiked)
				{
					// Do nothing
					return;
				}

				// If there is no like yet
				if (existingLike == null)
				{
					// Create a new like
					existingLike = new(request.PostCommentId, request.UserId, _timeService.Now, true);
					_uniwikiContext.PostCommentLikes.Add(existingLike);
				}
				else
				{
					// Set IsLiked to true
					existingLike.IsLiked = true;
				}
			}
			// If user wants to remove the like from the post
			else
			{
				// If the post is not already liked
				if (existingLike == null || !existingLike.IsLiked)
				{
					// Do nothing
					return;
				}

				// Set the like to IsLiked = false
				existingLike.IsLiked = false;
			}

			// Save the changes
			await _uniwikiContext.SaveChangesAsync();
		}
	}
}