﻿using System;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.Uni.LikePostComment;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public class PostCommentViewModel
	{
		public bool IsUsersComment { get; }
		public string AuthorDisplayName { get; }

		public string CreationTime { get; }
		public Guid? AuthorId { get; }
		public string Text { get; }
		public int LikesCount { get; }
		public bool IsLikedByUser { get; }
		public bool IsTemplate { get; set; }
		public ProfilePageRoute AuthorPageRoute { get; }
		public ProfileImageRoute? AuthorImageRoute { get; }

		// Nullable when used as a template
		public LikePostCommentRoute? LikePostCommentRouteTrue { get; }

		// Nullable when used as a template
		public LikePostCommentRoute? LikePostCommentRouteFalse { get; }

		public PostCommentViewModel(
			Guid? authorId,
			string authorFirstName,
			string authorFamilyName,
			string authorUrl,
			string? authorImageUrl,
			DateTime? creationDate,
			string text,
			int likesCount,
			bool isLikedByUser,
			Guid? postCommentId,
			DateTime currentTime,
			Guid? userId)
		{
			AuthorDisplayName = authorFirstName + " " + authorFamilyName;
			CreationTime = Converters.ToPassedDayTime(currentTime, creationDate ?? currentTime);
			AuthorId = authorId;
			Text = text;
			LikesCount = likesCount;
			IsLikedByUser = isLikedByUser;
			AuthorPageRoute = new(authorUrl);
			AuthorImageRoute = authorImageUrl == null ? null : new(authorImageUrl);
			LikePostCommentRouteTrue = postCommentId.HasValue ? new(postCommentId.Value, true) : null;
			LikePostCommentRouteFalse = postCommentId.HasValue ? new(postCommentId.Value, false) : null;
			IsTemplate = creationDate == null;
			IsUsersComment = authorId == userId;
		}
	}
}