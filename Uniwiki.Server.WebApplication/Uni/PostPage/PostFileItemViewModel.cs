﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.Uni.DownloadPostFile;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public class PostFileItemViewModel
	{
		public string FileName { get; }
		public string Size { get; }
		public DownloadPostFileRoute DownloadPostFileRoute { get; }

		public PostFileItemViewModel(string fileName, long sizeInBytes, string postFileUrl)
		{
			FileName = fileName;
			Size = sizeInBytes.Apply(Converters.ToReadableFileSize);
			DownloadPostFileRoute = new(postFileUrl);
		}
	}
}