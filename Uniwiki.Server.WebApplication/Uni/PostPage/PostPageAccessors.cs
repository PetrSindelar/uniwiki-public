﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Post.EditPostPage;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public static class PostPageAccessors
	{
		public static Func<UniwikiContext, Task<Guid>> GetPostIdFromUrl(string universityUrl, string facultyUrl,
			string courseUrl, string postUrl)
			=> async uniwikiContext
				=>
			{
				var postId = await uniwikiContext.Posts
					.Include(p => p.Course)
					.ThenInclude(p => p.Faculty)
					.ThenInclude(p => p.University)
					.Where(p => p.Url == postUrl &&
					            p.Course.Url == courseUrl &&
					            p.Course.Faculty.Url == facultyUrl &&
					            p.Course.Faculty.University.Url == universityUrl)
					.Select(p => p.Id)
					.FirstOrDefaultAsync();


				if (postId == default)
				{
					Console.WriteLine("Throwing!");
					throw new NotFoundException("The post has not been found");
				}

				return postId;
			};

		public static Func<UniwikiContext, Task<PostPageViewModel>> GetPostPageViewModel(Guid postId,
			UserInfo? userInfo,
			DateTime currentTime)
			=> uniwikiContext =>
			{
				var userId = userInfo?.UserClaims.UserId;
				return uniwikiContext.Posts
					.Include(p => p.Course)
					.ThenInclude(p => p.Faculty)
					.ThenInclude(p => p.University)
					.Include(p => p.PostFiles)
					.Include(p => p.Comments)
					.ThenInclude(p => p.Author)
					.Where(p => p.Id == postId)
					.Select(p => new PostPageViewModel(
							p.Text,
							p.Category,
							p.Likes.Any(l => l.ProfileId == userId && l.IsLiked),
							p.Likes.Count(l => l.IsLiked),
							p.CreationTime,
							p.Comments
								.OrderBy(c => c.CreationTime)
								.Select(c => new PostCommentViewModel(
										c.AuthorId,
										c.Author.FirstName,
										c.Author.FamilyName,
										c.Author.Url,
										c.Author.ProfileImageUrl,
										c.CreationTime,
										c.Text,
										c.Likes.Count(l => l.IsLiked),
										c.Likes.Any(
											l => l.ProfileId == userId && l.IsLiked),
										c.Id,
										currentTime,
										// ReSharper disable once MergeConditionalExpression
										userId
									)
								)
								.ToArray(),
							p.Author.FirstName,
							p.Author.FamilyName,
							p.Author.Url,
							p.Course.LongName,
							p.PostFiles.Where(f => !f.IsImage)
								.Select(f => new PostFileItemViewModel(f.OriginalFullName, f.Size, f.Url)).ToArray(),
							p.Course.Faculty.University.LongName,
							p.Course.Faculty.University.Url,
							p.Course.Faculty.LongName,
							p.Course.Faculty.Url,
							p.Course.Url,
							p.Comments.Any(c => c.AuthorId == userId),
							p.Comments.Count,
							p.Author.ProfileImageUrl,
							p.Url,
							userInfo,
							p.Id,
							currentTime,
							p.PostFiles
								.Where(f => f.IsImage)
								.Select(f => new PostFileItemViewModel(f.OriginalFullName, f.Size, f.Url))
								.ToArray()
						)
					).FirstAsync();
			};
	}
}