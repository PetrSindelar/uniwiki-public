﻿using System;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Shared.Requests;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public class PostPageRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string PostUrl { get; }

		public PostPageRequest(string universityUrl, string facultyUrl, string courseUrl, string postUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostUrl = postUrl;
		}
	}
}