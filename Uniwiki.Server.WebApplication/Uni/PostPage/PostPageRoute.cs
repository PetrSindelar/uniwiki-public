﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public class PostPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Uni";
		public const string UniversityRouteParameter = "universityUrl";
		public const string FacultyRouteParameter = "facultyUrl";
		public const string CourseRouteParameter = "courseUrl";
		public const string PostRouteParameter = "postUrl";

		public const string RouteAttribute =
			BaseRoute + "/{" +
			UniversityRouteParameter + "}/{" +
			FacultyRouteParameter + "}/{" +
			CourseRouteParameter + "}/{" +
			PostRouteParameter + "}";

		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public string PostUrl { get; }
		public bool ScrollToComments { get; }

		public PostPageRoute(string universityUrl, string facultyUrl, string courseUrl, string postUrl,
			bool scrollToComments = false)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			PostUrl = postUrl;
			ScrollToComments = scrollToComments;
		}

		public override string Build() => $"{BaseRoute}/{UniversityUrl}/{FacultyUrl}/{CourseUrl}/{PostUrl}" +
		                                  (ScrollToComments ? $"#{PostPageViewModel.CommentSectionId}" : string.Empty);
	}
}