﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	[ScopedService]
	public class PostPageServerAction
	{
		private readonly ITimeService _timeService;
		private readonly IUserInfoService _userInfoService;
		private readonly UniwikiContext _uniwikiContext;

		public PostPageServerAction(UniwikiContext uniwikiContext, ITimeService timeService,
			IUserInfoService userInfoService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
			_userInfoService = userInfoService;
		}

		public async Task<PostPageViewModel> ExecuteAsync(PostPageRequest request)
		{
			var userInfo = _userInfoService.GetUserInfo();

			request = PostPageStandardizer.Standardize(request);

			var postId = await PostPageAccessors.GetPostIdFromUrl(request.UniversityUrl, request.FacultyUrl,
				request.CourseUrl, request.PostUrl)(_uniwikiContext);

			var viewModel =
				await PostPageAccessors.GetPostPageViewModel(postId, userInfo, _timeService.Now)(
					_uniwikiContext);

			return viewModel;
		}
	}
}