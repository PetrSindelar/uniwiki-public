﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public static class PostPageStandardizer
	{
		public static PostPageRequest Standardize(PostPageRequest request)
			=> new(
				request.UniversityUrl.Apply(Standardizers.StandardizeUrl),
				request.FacultyUrl.Apply(Standardizers.StandardizeUrl),
				request.CourseUrl.Apply(Standardizers.StandardizeUrl),
				request.PostUrl.Apply(Standardizers.StandardizeUrl)
			);
	}
}