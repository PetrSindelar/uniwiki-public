﻿using System;
using System.Collections.Generic;
using System.Linq;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Profile.ProfileImage;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.AddPostComment;
using Uniwiki.Server.WebApplication.Uni.CoursePage;
using Uniwiki.Server.WebApplication.Uni.ReportPostRequestPage;
using Uniwiki.Server.WebApplication.Uni.Shared;

namespace Uniwiki.Server.WebApplication.Uni.PostPage
{
	public class PostPageViewModel : PageLayoutViewModel
	{
		public const string CommentSectionId = "commentsContainer";

		public string Text { get; }
		public string? Category { get; }
		public string CreationTime { get; }
		public PostCommentViewModel[][] Comments { get; }
		public string AuthorFullName { get; }
		public PostFileItemViewModel[] PostFiles { get; }
		public string UniversityName { get; }
		public string FacultyName { get; }
		public PostCommentViewModel? CommentTemplate { get; }
		public AddPostCommentForm AddPostCommentForm { get; }
		public PostBarViewModel PostBarViewModel { get; }
		public ProfilePageRoute AuthorRoute { get; }
		public ProfileImageRoute? ProfileImageRoute { get; }
		public AddPostCommentRoute AddPostCommentRoute { get; }
		public EditPostPageRoute EditPostPageRoute { get; }
		public PostFileItemViewModel[] PostImages { get; }
		public ReportPostRequestPageRoute ReportPostRequestPageRoute { get; }

		public PostPageViewModel(
			string text,
			string? category,
			bool likedByUser,
			int likesCount,
			DateTime creationTime,
			PostCommentViewModel[] postComments,
			string authorFirstName,
			string authorFamilyName,
			string authorUrl,
			string courseName,
			PostFileItemViewModel[] postFiles,
			string universityName,
			string universityUrl,
			string facultyName,
			string facultyUrl,
			string courseUrl,
			bool commentedByUser,
			int commentsCount,
			string? authorImageUrl,
			string postUrl,
			UserInfo? userInfo,
			Guid postId,
			DateTime currentTime,
			PostFileItemViewModel[] postImages
		)
			: base(
				new CoursePageRoute(universityUrl, facultyUrl, courseUrl),
				"A post in " + courseName
			)
		{
			Text = text;
			Category = category;
			CreationTime = Converters.ToPassedDayTime(currentTime, creationTime);

			// Group the comments by the author
			Comments = postComments
				.Aggregate(new List<List<PostCommentViewModel>>(), (accumulative, comment)
					=>
				{
					if (!accumulative.Any() || accumulative.Last().Last().AuthorId != comment.AuthorId)
					{
						accumulative.Add(new());
					}

					accumulative.Last().Add(comment);

					return accumulative;
				})
				.Select(list => list.ToArray())
				.ToArray();

			AuthorFullName = authorFirstName + " " + authorFamilyName;
			PostFiles = postFiles;
			UniversityName = universityName;
			FacultyName = facultyName;
			PostImages = postImages;
			AddPostCommentForm = new();
			PostBarViewModel = new(
				commentedByUser,
				commentsCount,
				likedByUser,
				likesCount,
				universityUrl,
				facultyUrl,
				courseUrl,
				postUrl
			);

			CommentTemplate = userInfo == null
				? null
				: new(userInfo.UserClaims.UserId, userInfo.UserClaims.FirstName, userInfo.UserClaims.FamilyName,
					userInfo
						.UserClaims.ProfileUrl,
					userInfo.UserClaims.FullProfileImageUrl, null, string.Empty, 0,
					false,
					null, currentTime, userInfo.UserClaims.UserId);
			AuthorRoute = new(authorUrl);
			ProfileImageRoute = authorImageUrl == null ? null : new(authorImageUrl);
			AddPostCommentRoute = new(postId);
			EditPostPageRoute = new EditPostPageRoute(universityUrl, facultyUrl, courseUrl, postUrl);
			ReportPostRequestPageRoute = new ReportPostRequestPageRoute(postId);
		}
	}
}