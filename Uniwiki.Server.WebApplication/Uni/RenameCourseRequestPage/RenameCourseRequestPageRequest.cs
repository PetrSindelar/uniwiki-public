﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.RenameCourseRequestPage
{
	public class RenameCourseRequestPageRequest
	{
		public Guid CourseId { get; }

		public RenameCourseRequestPageRequest(Guid courseId)
		{
			CourseId = courseId;
		}
	}
}