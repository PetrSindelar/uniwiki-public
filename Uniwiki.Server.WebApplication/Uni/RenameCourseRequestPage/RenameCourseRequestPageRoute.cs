﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.RenameCourseRequestPage
{
	public class RenameCourseRequestPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/RenameCourse";

		public const string CourseIdRouteParameter = "CourseId";
		public const string RouteAttribute = BaseRoute + "/{" + CourseIdRouteParameter + "}";

		public Guid CourseId { get; }

		public RenameCourseRequestPageRoute(Guid courseId)
		{
			CourseId = courseId;
		}

		public override string Build() => BaseRoute + "/" + CourseId;
	}
}