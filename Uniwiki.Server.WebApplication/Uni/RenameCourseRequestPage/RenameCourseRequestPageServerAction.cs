﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Uni.RenameCourseRequestPage
{
	[ScopedService]
	public class RenameCourseRequestPageServerAction
	{
		private readonly IOptions<FormsOptions> _formsOptions;
		private readonly UniwikiContext _uniwikiContext;

		public RenameCourseRequestPageServerAction(IOptions<FormsOptions> formsOptions, UniwikiContext uniwikiContext)
		{
			_formsOptions = formsOptions;
			_uniwikiContext = uniwikiContext;
		}

		public async Task<GoogleFormPageViewModel> ExecuteAsync(RenameCourseRequestPageRequest request)
		{
			var formUrl = _formsOptions.Value.GetRenameCourseRequestForm(request.CourseId);

			// Get the course long name
			var courseLongName = await _uniwikiContext.Courses.Where(c => c.Id == request.CourseId)
				.Select(c => c.LongName).FirstOrDefaultAsync();

			if (courseLongName == null)
			{
				throw new NotFoundException("Course not found.");
			}

			var viewModel = new GoogleFormPageViewModel(null, "Rename the course ", formUrl);

			return viewModel;
		}
	}
}