﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.ReportCourseRequestPage
{
	public class ReportCourseRequestPageRequest
	{
		public Guid CourseId { get; }

		public ReportCourseRequestPageRequest(Guid courseId)
		{
			CourseId = courseId;
		}
	}
}