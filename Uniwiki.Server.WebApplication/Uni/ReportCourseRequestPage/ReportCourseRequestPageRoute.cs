﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.ReportCourseRequestPage
{
	public class ReportCourseRequestPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/ReportCourse";

		public const string CourseIdRouteParameter = "CourseId";
		public const string RouteAttribute = BaseRoute + "/{" + CourseIdRouteParameter + "}";

		public Guid CourseId { get; }

		public ReportCourseRequestPageRoute(Guid courseId)
		{
			CourseId = courseId;
		}

		public override string Build() => $"{BaseRoute}" + "/" + CourseId;
	}
}