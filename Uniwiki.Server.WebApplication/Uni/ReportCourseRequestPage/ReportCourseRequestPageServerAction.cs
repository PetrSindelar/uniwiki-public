﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Uni.ReportCourseRequestPage
{
	[ScopedService]
	public class ReportCourseRequestPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IOptions<FormsOptions> _formsOptions;

		public ReportCourseRequestPageServerAction(UniwikiContext uniwikiContext, IOptions<FormsOptions> formsOptions)
		{
			_uniwikiContext = uniwikiContext;
			_formsOptions = formsOptions;
		}

		public async Task<GoogleFormPageViewModel> ExecuteAsync(ReportCourseRequestPageRequest request)
		{
			var courseLongName = await _uniwikiContext.Courses.Where(c => c.Id == request.CourseId)
				.Select(c => c.LongName).FirstOrDefaultAsync();

			if (courseLongName == null)
			{
				throw new NotFoundException("Course not found.");
			}

			var viewModel = new GoogleFormPageViewModel(
				null,
				"Report the course " + courseLongName,
				_formsOptions.Value.GetReportCourseRequestForm(request.CourseId));

			return viewModel;
		}
	}
}