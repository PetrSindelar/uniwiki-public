﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.ReportPostRequestPage
{
	public class ReportPostRequestPageRequest
	{
		public Guid PostId { get; }

		public ReportPostRequestPageRequest(Guid postId)
		{
			PostId = postId;
		}
	}
}