﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.ReportPostRequestPage
{
	public class ReportPostRequestPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/ReportPost";

		public const string PostIdRouteParameter = "PostIdRouteParameter";
		public const string RouteAttribute = BaseRoute + "/{" + PostIdRouteParameter + "}";

		public Guid PostId { get; }

		public ReportPostRequestPageRoute(Guid postId)
		{
			PostId = postId;
		}

		public override string Build() => $"{BaseRoute}" + "/" + PostId;
	}
}