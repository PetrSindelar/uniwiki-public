﻿using System;

namespace Uniwiki.Server.WebApplication.Uni.ReportUserRequestPage
{
	public class ReportUserRequestPageRequest
	{
		public Guid UserId { get; }

		public ReportUserRequestPageRequest(Guid userId)
		{
			UserId = userId;
		}
	}
}