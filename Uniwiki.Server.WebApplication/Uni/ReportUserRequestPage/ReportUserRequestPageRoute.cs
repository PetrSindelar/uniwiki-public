﻿using System;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.ReportUserRequestPage
{
	public class ReportUserRequestPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/ReportUser";

		public const string UserIdRouteParameter = "UserIdRouteParameter";
		public const string RouteAttribute = BaseRoute + "/{" + UserIdRouteParameter + "}";

		public Guid UserId { get; }

		public ReportUserRequestPageRoute(Guid userId)
		{
			UserId = userId;
		}

		public override string Build() => $"{BaseRoute}" + "/" + UserId;
	}
}