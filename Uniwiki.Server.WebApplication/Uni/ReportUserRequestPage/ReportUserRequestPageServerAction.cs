﻿using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.Uni.ReportUserRequestPage
{
	[ScopedService]
	public class ReportUserRequestPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly IOptions<FormsOptions> _formsOptions;

		public ReportUserRequestPageServerAction(UniwikiContext uniwikiContext, IOptions<FormsOptions> formsOptions)
		{
			_uniwikiContext = uniwikiContext;
			_formsOptions = formsOptions;
		}

		public Task<GoogleFormPageViewModel> ExecuteAsync(ReportUserRequestPageRequest request)
		{
			var viewModel = new GoogleFormPageViewModel(
				null,
				"Report a post",
				_formsOptions.Value.GetReportUserRequestForm(request.UserId));

			return Task.FromResult(viewModel);
		}
	}
}