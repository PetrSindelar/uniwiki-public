﻿using Uniwiki.Server.WebApplication.Uni.CoursePage;
using Uniwiki.Server.WebApplication.Uni.SelectFacultyPage;
using Uniwiki.Server.WebApplication.Uni.SetCourseNotifications;

namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	public class CourseItemViewModel
	{
		public string DisplayName => string.IsNullOrWhiteSpace(Code) ? LongName : Code + " - " + LongName;
		public string LongName { get; }
		public string? Code { get; }
		public SelectFacultyPageRoute UniversityUrl { get; }
		public SelectCoursePageRoute FacultyUrl { get; }
		public CoursePageRoute CourseRoute { get; }
		public bool NotificationsOn { get; }
		public SetCourseNotificationsRoute SubscribeToNotificationsRoute { get; }
		public SetCourseNotificationsRoute UnsubscribeToNotificationsRoute { get; }
		public string SearchText { get; }

		public CourseItemViewModel(string longName, string? code, string universityUrl, string facultyUrl,
			string courseUrl,
			bool notificationsOn,
			string standardizedLongName,
			string? standardizedCode)
		{
			LongName = longName;
			Code = code;
			UniversityUrl = new SelectFacultyPageRoute(universityUrl);
			FacultyUrl = new SelectCoursePageRoute(universityUrl, facultyUrl);
			CourseRoute = new CoursePageRoute(universityUrl, facultyUrl, courseUrl);
			NotificationsOn = notificationsOn;
			SubscribeToNotificationsRoute =
				new SetCourseNotificationsRoute(universityUrl, facultyUrl, courseUrl, true);
			UnsubscribeToNotificationsRoute =
				new SetCourseNotificationsRoute(universityUrl, facultyUrl, courseUrl, false);
			SearchText = $"{standardizedCode} {standardizedLongName}";
		}
	}
}