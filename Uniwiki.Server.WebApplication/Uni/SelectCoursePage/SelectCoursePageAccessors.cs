﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	internal static class SelectCoursePageAccessors
	{
		public static Func<UniwikiContext, Task<SelectCoursePageViewModel>> GetViewModel(string universityUrl,
			string facultyUrl, bool showTip)
			=> async uniwikiContext => await uniwikiContext
				                           .Faculties
				                           .Include(f => f.University)
				                           .Include(f => f.Courses)
				                           .Where(f => f.University.Url == universityUrl && f.Url == facultyUrl)
				                           .Select(f => new SelectCoursePageViewModel(
					                           showTip,
					                           f.University.ShortName,
					                           f.University.LongName,
					                           f.ShortName,
					                           f.LongName,
					                           f.University.Url,
					                           f.Url,
					                           uniwikiContext
						                           .Courses
						                           .Where(c => c.FacultyId == f.Id)
						                           .OrderBy(c => c.LongName)
						                           .Select(c => new CourseItemViewModel(
							                           c.LongName,
							                           c.Code,
							                           f.University.Url,
							                           f.Url,
							                           c.Url,
							                           false, // TODO: Figuring if notifications are on
							                           c.LongNameStandardized,
							                           c.CodeStandardized
						                           ))
						                           .ToArray()
				                           ))
				                           .SingleOrDefaultAsync()
			                           ?? throw new NotFoundException();
	}
}