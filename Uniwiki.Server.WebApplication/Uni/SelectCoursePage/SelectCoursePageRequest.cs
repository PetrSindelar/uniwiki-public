﻿namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	public class SelectCoursePageRequest
	{
		public string UniversityUrl { get; }
		public string FacultyUrl { get; }

		public SelectCoursePageRequest(string universityUrl, string facultyUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
		}
	}
}