﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	public class SelectCoursePageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Uni";
		public const string UniversityRouteParameter = "universityUrl";
		public const string FacultyRouteParameter = "facultyUrl";

		public const string RouteAttribute =
			BaseRoute + "/{" + UniversityRouteParameter + "}/{" + FacultyRouteParameter + "}";

		public string FacultyUrl { get; }
		public string UniversityUrl { get; }

		public SelectCoursePageRoute(string universityUrl, string facultyUrl)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
		}

		public override string Build() => $"{BaseRoute}/{UniversityUrl}/{FacultyUrl}";
	}
}