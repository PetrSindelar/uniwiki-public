﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	[ScopedService]
	public class SelectCoursePageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public SelectCoursePageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<SelectCoursePageViewModel> ExecuteAsync(SelectCoursePageRequest request)
		{
			request = SelectCoursePageStandardizer.Standardize(request);

			var showTip = false; // TODO: Implement showing tip for setting a home university

			var viewModel =
				await SelectCoursePageAccessors.GetViewModel(request.UniversityUrl, request.FacultyUrl, showTip)(
					_uniwikiContext);

			return viewModel;
		}
	}
}