﻿using Uniwiki.Server.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	internal static class SelectCoursePageStandardizer
	{
		public static SelectCoursePageRequest Standardize(SelectCoursePageRequest request)
			=> new SelectCoursePageRequest(
				request.UniversityUrl.Apply(Standardizers.StandardizeUrl),
				request.FacultyUrl.Apply(Standardizers.StandardizeUrl)
			);
	}
}