﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.AddCoursePage;
using Uniwiki.Server.WebApplication.Uni.SelectFacultyPage;

namespace Uniwiki.Server.WebApplication.Uni.SelectCoursePage
{
	public class SelectCoursePageViewModel : PageLayoutViewModel
	{
		public bool ShowTipComponent { get; }
		public string UniversityShortName { get; }
		public string UniversityLongName { get; }
		public string FacultyShortName { get; }
		public string FacultyLongName { get; }
		public CourseItemViewModel[] Courses { get; }
		public AddCoursePageRoute AddCoursePageRoute { get; }

		public SelectCoursePageViewModel(
			bool showTipComponent,
			string universityShortName,
			string universityLongName,
			string facultyFacultyShortName,
			string facultyFacultyLongName,
			string universityUrl,
			string facultyUrl,
			CourseItemViewModel[] courses)
			: base(new SelectFacultyPageRoute(universityUrl),
				$"Select a course from {universityShortName} - {facultyFacultyShortName}")
		{
			ShowTipComponent = showTipComponent;
			UniversityShortName = universityShortName;
			UniversityLongName = universityLongName;
			FacultyShortName = facultyFacultyShortName;
			FacultyLongName = facultyFacultyLongName;
			Courses = courses;
			AddCoursePageRoute = new AddCoursePageRoute(universityUrl, facultyUrl);
		}
	}
}