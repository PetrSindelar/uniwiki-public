﻿using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;

namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	public class FacultyItemViewModel
	{
		public string DisplayName => $"{ShortName} - {LongName}";
		public string ShortName { get; }
		public string LongName { get; }
		public SelectCoursePageRoute Url { get; }

		public FacultyItemViewModel(string shortName, string longName, string universityUrl, string facultyUrl)
		{
			ShortName = shortName;
			LongName = longName;
			Url = new SelectCoursePageRoute(universityUrl, facultyUrl);
		}
	}
}