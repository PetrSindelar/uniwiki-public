﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.Shared.Extensions;

namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	public static class SelectFacultyPageAccessors
	{
		public static Func<UniwikiContext, Task<(Guid id, string ShortName)>> GetUniversityFromUrl(string universityUrl)
			=> async uniwikiContext
				=>
			{
				var uni = (await uniwikiContext
					.Universities
					.Select(u => new { u.Url, u.Id, u.ShortName })
					.FirstOrDefaultAsync(u => u.Url == universityUrl));

				if (uni == null)
					throw new NotFoundException("The university has not been found.");

				return uni.Apply(u => (u.Id, u.ShortName));
			};

		public static Func<UniwikiContext, Task<FacultyItemViewModel[]>> GetFacultyItems(Guid universityId)
		{
			return uniwikiContext
				=> uniwikiContext.Faculties
					.Include(f => f.University)
					.Where(f => f.UniversityId == universityId)
					.Select(faculty => new FacultyItemViewModel(
						faculty.ShortName,
						faculty.LongName,
						faculty.University.Url,
						faculty.Url))
					.ToArrayAsync();
		}
	}
}