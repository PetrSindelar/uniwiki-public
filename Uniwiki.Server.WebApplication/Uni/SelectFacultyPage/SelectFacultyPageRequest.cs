﻿namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	public class SelectFacultyPageRequest
	{
		public string UniversityUrl { get; }

		public SelectFacultyPageRequest(string universityUrl)
		{
			UniversityUrl = universityUrl;
		}
	}
}