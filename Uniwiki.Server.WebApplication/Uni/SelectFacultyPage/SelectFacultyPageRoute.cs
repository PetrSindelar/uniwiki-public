﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	public class SelectFacultyPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/Uni";
		public const string UniversityRouteParameter = "universityUrl";
		public const string RouteAttribute = BaseRoute + "/{" + UniversityRouteParameter + "}";

		public string UniversityUrl { get; }

		public SelectFacultyPageRoute(string universityUrl) => UniversityUrl = universityUrl;

		public override string Build() => $"{BaseRoute}/{UniversityUrl}";
	}
}