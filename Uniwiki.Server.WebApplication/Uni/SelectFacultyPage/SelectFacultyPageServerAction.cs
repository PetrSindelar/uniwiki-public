﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	[ScopedService]
	public class SelectFacultyPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public SelectFacultyPageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<SelectFacultyPageViewModel> ExecuteAsync(SelectFacultyPageRequest request)
		{
			// Standardize
			request = SelectFacultyPageStandardizer.Standardize(request);

			// Get Id of the university
			var university =
				await SelectFacultyPageAccessors.GetUniversityFromUrl(request.UniversityUrl)(_uniwikiContext);

			// Get faculties for the university
			var facultyItems = await SelectFacultyPageAccessors.GetFacultyItems(university.id)(_uniwikiContext);

			return new SelectFacultyPageViewModel(
				request.UniversityUrl,
				university.ShortName,
				facultyItems);
		}
	}
}