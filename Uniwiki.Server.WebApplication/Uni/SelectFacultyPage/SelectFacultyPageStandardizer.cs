﻿using Uniwiki.Server.WebApplication.Shared.Functions;

namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	public static class SelectFacultyPageStandardizer
	{
		public static SelectFacultyPageRequest Standardize(SelectFacultyPageRequest request)
			=>
				new SelectFacultyPageRequest(
					Standardizers.StandardizeUrl(request.UniversityUrl)
				);
	}
}