﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.HomePage;

namespace Uniwiki.Server.WebApplication.Uni.SelectFacultyPage
{
	public class SelectFacultyPageViewModel : PageLayoutViewModel
	{
		public FacultyItemViewModel[] FacultyItems { get; }

		public AddFacultyRequestPageRoute.AddFacultyRequestPageRoute AddFacultyRequestPageRoute { get; }

		public SelectFacultyPageViewModel(string universityUrl, string universityShortName,
			FacultyItemViewModel[] facultyItems)
			: base(new HomePageRoute(),
				$"Select a faculty from {universityShortName}")
		{
			FacultyItems = facultyItems;
			AddFacultyRequestPageRoute = new AddFacultyRequestPageRoute.AddFacultyRequestPageRoute(universityUrl);
		}
	}
}