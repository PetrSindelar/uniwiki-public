﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.Uni.SetCourseNotifications
{
	public class SetCourseNotificationsRoute : RouteBase
	{
		public const string BaseRoute = ApiRoute + "/Uni";
		public const string UniversityRouteParameter = "universityUrl";
		public const string FacultyRouteParameter = "facultyUrl";
		public const string CourseRouteParameter = "courseUrl";
		public const string SetOnRouteParameter = "setOn";
		public const string SetNotifications = "setNotifications";

		public const string RouteAttribute = BaseRoute + "/{" + UniversityRouteParameter + "}/{" +
		                                     FacultyRouteParameter + "}/{" + CourseRouteParameter + "}/" +
		                                     SetNotifications + "/{" + SetOnRouteParameter + "}";

		public string UniversityUrl { get; }
		public string FacultyUrl { get; }
		public string CourseUrl { get; }
		public bool SetOn { get; }

		public SetCourseNotificationsRoute(string universityUrl, string facultyUrl, string courseUrl,
			bool setOn)
		{
			UniversityUrl = universityUrl;
			FacultyUrl = facultyUrl;
			CourseUrl = courseUrl;
			SetOn = setOn;
		}

		public override string Build()
			=> $"{BaseRoute}/{UniversityUrl}/{FacultyUrl}/{CourseUrl}/{SetNotifications}/{SetOn}";
	}
}