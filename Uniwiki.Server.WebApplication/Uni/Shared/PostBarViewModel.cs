﻿using Uniwiki.Server.WebApplication.Uni.LikePost;
using Uniwiki.Server.WebApplication.Uni.PostPage;

namespace Uniwiki.Server.WebApplication.Uni.Shared
{
	public class PostBarViewModel
	{
		public bool IsCommentedByUser { get; }
		public int CommentsCount { get; }
		public bool IsLikedByUser { get; }
		public int LikesCount { get; }

		public LikePostRoute LikePostRouteTrue { get; }
		public LikePostRoute LikePostRouteFalse { get; }
		public PostPageRoute PostPageRoute { get; }

		public PostBarViewModel(bool isCommentedByUser, int commentsCount, bool isLikedByUser, int likesCount,
			string universityUrl, string facultyUrl, string courseUrl, string postUrl)
		{
			IsCommentedByUser = isCommentedByUser;
			CommentsCount = commentsCount;
			IsLikedByUser = isLikedByUser;
			LikesCount = likesCount;
			LikePostRouteTrue = new(universityUrl, facultyUrl, courseUrl, postUrl, true);
			LikePostRouteFalse = new(universityUrl, facultyUrl, courseUrl, postUrl, false);
			PostPageRoute = new(universityUrl, facultyUrl, courseUrl, postUrl, true);
		}
	}
}