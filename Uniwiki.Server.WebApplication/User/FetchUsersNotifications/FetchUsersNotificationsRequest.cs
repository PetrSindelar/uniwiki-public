﻿using System;

namespace Uniwiki.Server.WebApplication.User.FetchUsersNotifications
{
	public class FetchUsersNotificationsRequest
	{
		public long? LastTimestamp { get; }
		public Guid UserId { get; }

		public FetchUsersNotificationsRequest(long? lastTimestamp, Guid userId)
		{
			LastTimestamp = lastTimestamp;
			UserId = userId;
		}
	}
}