﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.FetchUsersNotifications
{
	public class FetchUsersNotificationsRoute : RouteBase
	{
		public const string BaseRoute = PartialViewRoute + "/FetchUsersNotifications";

		public const string LastTimestampRouteParameter = "LastTimestamp";
		public const string RouteAttribute = BaseRoute + "/{" + LastTimestampRouteParameter + "?}";

		public long? LastTimestamp { get; }

		public FetchUsersNotificationsRoute(long? lastTimestamp)
		{
			LastTimestamp = lastTimestamp;
		}

		public override string Build() => BaseRoute + (LastTimestamp == null ? string.Empty : "/" + LastTimestamp);
	}
}