﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebApplication.Uni.CoursePage;
using Uniwiki.Server.WebApplication.Uni.PostPage;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.FetchUsersNotifications
{
	[ScopedService]
	public class FetchUsersNotificationsServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;

		public FetchUsersNotificationsServerAction(UniwikiContext uniwikiContext, ITimeService timeService,
			IUserInfoService userInfoService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
		}

		public async Task<FetchUsersNotificationsViewModel> ExecuteAsync(FetchUsersNotificationsRequest request)
		{
			// Get the caling user
			var profile = await _uniwikiContext.Users.FindAsync(request.UserId);

			// Get the last fetched timestamp
			var lastNotificationsCheck = profile.LastNotificationsCheck;

			// TODO: Implement actually getting the notifications
			var course = _uniwikiContext.Courses.Include(c => c.Faculty).ThenInclude(f => f.University).First();
			var post = _uniwikiContext.Posts.Include(p => p.Course).ThenInclude(c => c.Faculty)
				.ThenInclude(f => f.University).First();

			// TODO: Create real notifications
			var exampleNotifications = new UsersNotificationsItemViewModel[]
			{
				new(new CoursePageRoute(course.Faculty.University.Url, course.Faculty.Url, course.Url),
					"New post in " + course.LongName),
				new(new PostPageRoute(post.Course.Faculty.University.Url, post.Course.Faculty.Url, post.Course.Url,
						post.Url, true),
					"New comments in a post in " + course.LongName)
			};

			// Update the last notification check
			profile.LastNotificationsCheck = _timeService.Now.Ticks;
			await _uniwikiContext.SaveChangesAsync();

			var viewModel = new FetchUsersNotificationsViewModel(exampleNotifications);

			return viewModel;
		}
	}
}