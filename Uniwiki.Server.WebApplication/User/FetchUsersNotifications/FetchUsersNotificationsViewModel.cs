﻿namespace Uniwiki.Server.WebApplication.User.FetchUsersNotifications
{
	public class FetchUsersNotificationsViewModel
	{
		public UsersNotificationsItemViewModel[] NotificationsItems { get; }

		public FetchUsersNotificationsViewModel(UsersNotificationsItemViewModel[] notificationsItems)
		{
			NotificationsItems = notificationsItems;
		}
	}
}