﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.FetchUsersNotifications
{
	public class UsersNotificationsItemViewModel
	{
		public RouteBase LinkRoute { get; }
		public string Text { get; }

		public UsersNotificationsItemViewModel(RouteBase linkRoute, string text)
		{
			LinkRoute = linkRoute;
			Text = text;
		}
	}
}