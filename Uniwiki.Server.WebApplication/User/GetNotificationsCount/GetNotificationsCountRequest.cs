﻿using System;

namespace Uniwiki.Server.WebApplication.User.GetNotificationsCount
{
	public class GetNotificationsCountRequest
	{
		public Guid UserId { get; }

		public GetNotificationsCountRequest(Guid userId)
		{
			UserId = userId;
		}
	}
}