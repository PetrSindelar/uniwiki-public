﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.User.GetNotificationsCount
{
	[ScopedService]
	public class GetNotificationsCountServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public GetNotificationsCountServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public async Task<int> ExecuteAsync(GetNotificationsCountRequest request)
		{
			// Get the last time the user checked for the notifications
			var lastCheckTime = await _uniwikiContext
				.Users
				.Where(u => u.Id == request.UserId)
				.Select(u => u.LastNotificationsCheck)
				.FirstAsync();

			// TODO: Check how many new posts are in the subscribed courses

			// TODO: Check how many posts in subscribed courses contain new comments

			// TODO: Return the real amount

			return lastCheckTime == 0 ? 2 : 0;
		}
	}
}