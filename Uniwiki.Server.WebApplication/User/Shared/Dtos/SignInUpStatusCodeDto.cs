﻿namespace Uniwiki.Server.WebApplication.User.Shared.Dtos
{
	public enum SignInUpStatusCodeDto
	{
		SignedIn = 1,
		SignUp = 2,
	}
}