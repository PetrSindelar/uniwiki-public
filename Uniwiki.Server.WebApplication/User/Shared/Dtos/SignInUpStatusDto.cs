﻿namespace Uniwiki.Server.WebApplication.User.Shared.Dtos
{
	public class SignInUpStatusDto
	{
		public SignInUpStatusCodeDto Code { get; }

		public SignInUpStatusDto(SignInUpStatusCodeDto code)
		{
			Code = code;
		}
	}
}