﻿namespace Uniwiki.Server.WebApplication.User.Shared.Dtos
{
	public class SignInUpViaEmailDto
	{
		public string Email { get; set; } = null!;
		public string Code { get; set; } = null!;
	}
}