﻿namespace Uniwiki.Server.WebApplication.User.Shared.Dtos
{
	public class SignUpViaEmailDto
	{
		public string Email { get; }
		public string Code { get; }
		public string FirstName { get; }
		public string Surname { get; }

		public SignUpViaEmailDto(string email, string code, string firstName, string surname)
		{
			Email = email;
			Code = code;
			FirstName = firstName;
			Surname = surname;
		}
	}
}