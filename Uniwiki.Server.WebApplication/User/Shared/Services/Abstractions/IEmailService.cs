﻿namespace Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions
{
	public interface IEmailService
	{
		void SendEmailConfirmation(string code, string email);
	}
}