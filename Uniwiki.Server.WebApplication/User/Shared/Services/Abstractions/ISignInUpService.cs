﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence.Models;

namespace Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions
{
	public interface ISignInUpService
	{
		Task<bool> DoesUserExistAsync(string email);
		Task SignInAsync(string email);
		Task<ProfileModel> SignUpAsync(string email, string firstName, string familyName, bool emailConfirmed);
		Task SignOutAsync();
	}
}