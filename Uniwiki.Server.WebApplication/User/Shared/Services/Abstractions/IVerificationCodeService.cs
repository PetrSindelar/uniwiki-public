﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence.Models;

namespace Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions
{
	public interface IVerificationCodeService
	{
		Task<ConfirmationCodeModel> CreateEmailVerificationCodeAsync(string email);
	}
}