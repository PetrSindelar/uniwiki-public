﻿using System;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	[ScopedService]
	public class CodeGeneratorService : ICodeGeneratorService
	{
		private readonly IRandomService _randomService;

		public CodeGeneratorService(IRandomService randomService)
		{
			_randomService = randomService;
		}

		public string GenerateVerificationCode(int codeLength)
			=> _randomService.Random.Next(0, (int) Math.Pow(10, codeLength)).ToString("D" + codeLength);
	}
}