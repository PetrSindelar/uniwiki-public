﻿using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	[ScopedService]
	public class EmailService : IEmailService
	{
		public void SendEmailConfirmation(string code, string email)
		{
			// TODO: Send the actual email
		}
	}
}