﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	[ScopedService]
	public class EmailVerificationService
	{
		private readonly ITimeService _timeService;
		private readonly UniwikiContext _uniwikiContext;
		private readonly IEmailService _emailService;
		private readonly IVerificationCodeService _verificationCodeService;
		private readonly AuthenticationOptions _authenticationOptions;

		public EmailVerificationService(ITimeService timeService, UniwikiContext uniwikiContext,
			IEmailService emailService,
			IOptions<AuthenticationOptions> authenticationOptions, IVerificationCodeService verificationCodeService)
		{
			_timeService = timeService;
			_uniwikiContext = uniwikiContext;
			_emailService = emailService;
			_verificationCodeService = verificationCodeService;
			_authenticationOptions = authenticationOptions.Value;
		}

		public async Task SendVerificationCodeAsync(string email)
		{
			var verificationCode = await _verificationCodeService.CreateEmailVerificationCodeAsync(email);

			// Send the code via email
			_emailService.SendEmailConfirmation(verificationCode.Code, verificationCode.Email);
		}

		public async Task<bool> IsVerificationCodeValid(string code, string email)
		{
			var confirmationCode = await _uniwikiContext.ConfirmationCodes.Where(c
					=> c.Email == email &&
					   c.Code == code &&
					   c.IsValid &&
					   c.Expiry >= _timeService.Now)
				.FirstOrDefaultAsync();

			return confirmationCode != null;
		}

		/// <param name="code"></param>
		/// <param name="email"></param>
		/// <returns>True - if the code has been invalidated. False - if there was no code to invalidate found.</returns>
		public async Task<bool> InvalidateCode(string code, string email)
		{
			var confirmationCode = await _uniwikiContext.ConfirmationCodes.Where(c
					=> c.Email == email &&
					   c.Code == code &&
					   c.IsValid &&
					   c.Expiry >= _timeService.Now)
				.FirstOrDefaultAsync();

			if (confirmationCode == null)
				return false;

			confirmationCode.IsValid = false;

			await _uniwikiContext.SaveChangesAsync();

			return true;
		}
	}
}