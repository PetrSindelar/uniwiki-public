﻿namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	public interface ICodeGeneratorService
	{
		string GenerateVerificationCode(int codeLength);
	}
}