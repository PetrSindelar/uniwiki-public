﻿using System;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	public interface IRandomService
	{
		Random Random { get; }
	}
}