﻿using System;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	[ScopedService]
	internal class RandomService : IRandomService
	{
		private static readonly Random Instance = new Random();

		public Random Random => Instance;
	}
}