﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Authentication;
using Uniwiki.Server.WebApplication.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;
using Uniwiki.Server.WebShared.Services.Abstractions;
using AuthenticationOptions = Uniwiki.Server.WebApplication.Shared.Configurations.AuthenticationOptions;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	[ScopedService]
	public class SignInUpService : ISignInUpService
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;
		private readonly SignInManager<ProfileModel> _signInManager;
		private readonly UserManager<ProfileModel> _userManager;
		private readonly AuthenticationOptions _authenticationOptions;

		public SignInUpService(UniwikiContext uniwikiContext, ITimeService timeService,
			IOptions<AuthenticationOptions> authenticationOptions, SignInManager<ProfileModel> signInManager,
			UserManager<ProfileModel> userManager)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
			_signInManager = signInManager;
			_userManager = userManager;
			_authenticationOptions = authenticationOptions.Value;
		}

		public Task<bool> DoesUserExistAsync(string email)
		{
			return _uniwikiContext.Users.AnyAsync(u => u.NormalizedEmail == email);
		}

		public async Task SignInAsync(string email)
		{
			var normalizedEmail = _userManager.NormalizeEmail(email);

			// Get the existing user
			var user = await _uniwikiContext.Users.Where(u => u.NormalizedEmail == normalizedEmail).FirstAsync();

			// Prepare the options for the token
			var authenticationProperties = new AuthenticationProperties()
			{
				AllowRefresh = true,
				ExpiresUtc = _timeService.Now.AddDays(_authenticationOptions.SignInTokenDurationDays),
				IsPersistent = true,
				IssuedUtc = _timeService.Now
			};

			// Sign the user in
			await _signInManager.SignInAsync(user, authenticationProperties);
		}

		public async Task<ProfileModel> SignUpAsync(string email, string firstName, string familyName,
			bool emailConfirmed)
		{
			// Generate URL for the user
			var url = await Standardizers.CreateUrl(firstName + " " + familyName,
				u => _uniwikiContext.Users.AllAsync(user => user.Url != u));

			// Create the profile
			var newUser = new ProfileModel(
				Guid.NewGuid(),
				email,
				firstName,
				familyName,
				url,
				null,
				_timeService.Now,
				AuthenticationLevel.RegularUser,
				null,
				0 // TODO: Set last notifications check to _timeService.Now
			);

			newUser.EmailConfirmed = emailConfirmed;


			// Save the profile to the DB
			var result = await _userManager.CreateAsync(newUser);

			if (!result?.Succeeded ?? false)
			{
				// TODO: Throw a better excetion
				throw new RequestException("Unable to sign in");
			}

			// If the user iss supposed to be an admin
			if (newUser.NormalizedEmail == _userManager.NormalizeEmail(_authenticationOptions.AdministratorEmail))
			{
				// Check that there is nobody, who have an admin claim
				var existingAdmins = await _userManager.GetUsersInRoleAsync(CustomRoles.Administrator);

				// Check if there is already an admin
				if (!existingAdmins.Any())
				{
					var addAdminRoleResult = await _userManager.AddToRoleAsync(newUser, CustomRoles.Administrator);

					if (!addAdminRoleResult.Succeeded)
					{
						// TODO: Use logger or remove
						Console.WriteLine(addAdminRoleResult.Errors.First().PropertiesToString());

						// TODO: Throw a better exception
						throw new RequestException("Could not sign up");
					}
				}
			}

			// Add him a user claim
			var addUserRoleResult = await _userManager.AddToRoleAsync(newUser, CustomRoles.User);

			if (!addUserRoleResult.Succeeded)
			{
				Console.WriteLine(addUserRoleResult.Errors.First().PropertiesToString());

				// TODO: Throw a better exception
				throw new RequestException("Could not sign up");
			}

			return newUser;
		}

		public Task SignOutAsync()
		{
			return _signInManager.SignOutAsync();
		}
	}
}