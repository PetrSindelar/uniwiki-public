﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.Shared.Services
{
	[ScopedService]
	public class VerificationCodeService : IVerificationCodeService
	{
		private readonly ITimeService _timeService;
		private readonly UniwikiContext _uniwikiContext;
		private readonly ICodeGeneratorService _codeGeneratorService;
		private readonly AuthenticationOptions _authenticationOptions;

		public VerificationCodeService(ITimeService timeService, IOptions<AuthenticationOptions> authenticationOptions,
			UniwikiContext uniwikiContext, ICodeGeneratorService codeGeneratorService)
		{
			_timeService = timeService;
			_uniwikiContext = uniwikiContext;
			_codeGeneratorService = codeGeneratorService;
			_authenticationOptions = authenticationOptions.Value;
		}


		public async Task<ConfirmationCodeModel> CreateEmailVerificationCodeAsync(string email)
		{
			var expiry = _timeService.Now.AddMinutes(_authenticationOptions.ConfirmationCodeDurationMinutes);

			Console.WriteLine("GENERATING CODE of duration: " + _authenticationOptions.ConfirmationCodeDurationMinutes);
			// Generate the code
			var code = _codeGeneratorService.GenerateVerificationCode(
				_authenticationOptions.EmailVerificationCodeLength);
			Console.WriteLine("GENERATED CODE: " + code);

			// Create the code model for the user
			var codeModel = new ConfirmationCodeModel(Guid.NewGuid(), code, email, expiry, true);

			// Save the code to the DB
			_uniwikiContext.ConfirmationCodes.Add(codeModel);

			// Save changes
			await _uniwikiContext.SaveChangesAsync();

			return codeModel;
		}
	}
}