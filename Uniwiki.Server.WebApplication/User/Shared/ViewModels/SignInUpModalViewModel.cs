﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.User.SignInUpViaEmail;
using Uniwiki.Server.WebApplication.User.SignInUpViaEmailRequestCode;
using Uniwiki.Server.WebApplication.User.SignInUpViaFacebook;
using Uniwiki.Server.WebApplication.User.SignUpViaEmail;

namespace Uniwiki.Server.WebApplication.User.Shared.ViewModels
{
	public class SignInUpModalViewModel : ModalViewModel
	{
		public string SubmitEmailButtonId { get; }

		public SignInUpViaFacebookRoute SignInUpViaFacebookRoute { get; }

		public SignInUpViaEmailForm SignInUpViaEmailForm { get; }
		public SignInUpViaEmailEnterCodeForm SignInUpViaEmailEnterCodeForm { get; }
		public SignInUpViaEmailRequestCodeRoute SignInUpViaEmailRequestCodeRoute { get; }
		public SignInUpViaEmailRoute SignInUpViaEmailRoute { get; }
		public SignUpViaEmailRoute SignUpViaEmailRoute { get; }
		public SignUpViaEmailForm SignUpViaEmailForm { get; }

		public SignInUpModalViewModel() : base("Sign In / Up", "signInUpModal")
		{
			SignInUpViaFacebookRoute = new SignInUpViaFacebookRoute();
			SignInUpViaEmailForm = new SignInUpViaEmailForm();
			SubmitEmailButtonId = "SubmitViaEmailButtonId";
			SignInUpViaEmailEnterCodeForm = new SignInUpViaEmailEnterCodeForm();
			SignInUpViaEmailRequestCodeRoute = new SignInUpViaEmailRequestCodeRoute();
			SignInUpViaEmailRoute = new SignInUpViaEmailRoute();
			SignUpViaEmailRoute = new SignUpViaEmailRoute();
			SignUpViaEmailForm = new SignUpViaEmailForm();
		}
	}
}