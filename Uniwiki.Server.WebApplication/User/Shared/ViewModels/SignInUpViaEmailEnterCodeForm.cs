﻿namespace Uniwiki.Server.WebApplication.User.Shared.ViewModels
{
	public class SignInUpViaEmailEnterCodeForm
	{
		public string Code { get; set; } = null!;
	}
}