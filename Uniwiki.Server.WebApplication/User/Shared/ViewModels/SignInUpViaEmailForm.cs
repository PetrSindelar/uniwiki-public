﻿namespace Uniwiki.Server.WebApplication.User.Shared.ViewModels
{
	public class SignInUpViaEmailForm
	{
		public string Email { get; set; } = null!;
	}
}