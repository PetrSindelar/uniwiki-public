﻿namespace Uniwiki.Server.WebApplication.User.Shared.ViewModels
{
	public class SignUpViaEmailForm
	{
		public string FirstName { get; set; }
		public string FamilyName { get; set; }
	}
}