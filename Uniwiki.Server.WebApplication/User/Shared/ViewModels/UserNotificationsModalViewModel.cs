﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.User.FetchUsersNotifications;

namespace Uniwiki.Server.WebApplication.User.Shared.ViewModels
{
	public class UserNotificationsModalViewModel : ModalViewModel
	{
		public string NotificationsContainerId { get; }
		public string FetchNotificationsButtonId { get; }
		public FetchUsersNotificationsRoute FetchUsersNotificationsRoute { get; }

		public UserNotificationsModalViewModel()
			: base("Notifications", "UserNotificationsModal")
		{
			NotificationsContainerId = "NotificationsContainerId";
			FetchNotificationsButtonId = "FetchNotificationsButtonId";
			FetchUsersNotificationsRoute = new FetchUsersNotificationsRoute(null);
		}
	}
}