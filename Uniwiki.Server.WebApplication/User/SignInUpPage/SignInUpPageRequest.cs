﻿namespace Uniwiki.Server.WebApplication.User.SignInUpPage
{
	public class SignInUpPageRequest
	{
		public string? OriginUrl { get; }

		public SignInUpPageRequest(string? originUrl)
		{
			OriginUrl = originUrl;
		}
	}
}