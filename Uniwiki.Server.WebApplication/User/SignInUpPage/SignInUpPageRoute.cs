﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.WebUtilities;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignInUpPage
{
	public class SignInUpPageRoute : RouteBase
	{
		public const string RouteAttribute = PageRoute + "/SignInUp";

		public const string BackUrlQueryParameter = "BackUrl";
		public const string RedirectUrlQueryParameter = "RedirectUrl";

		public RouteBase? BackRoute { get; }
		public RouteBase? RedirectRoute { get; }

		public SignInUpPageRoute(RouteBase? backRoute = null, RouteBase? redirectRoute = null)
		{
			BackRoute = backRoute;
			RedirectRoute = redirectRoute;
		}

		public override string Build()
			=> new UriBuilder(string.Empty, string.Empty)
			{
				Path = RouteAttribute,
				Query = QueryHelpers.AddQueryString(string.Empty, new Dictionary<string, string?>
				{
					{ BackUrlQueryParameter, BackRoute?.Build() },
					{ RedirectUrlQueryParameter, RedirectRoute?.Build() },
				})
			}.ToString();
	}
}