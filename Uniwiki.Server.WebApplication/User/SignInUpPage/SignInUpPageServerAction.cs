﻿using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignInUpPage
{
	[ScopedService]
	public class SignInUpPageServerAction
	{
		public SignInUpPageViewModel Execute(SignInUpPageRequest request)
		{
			var originRoute = request.OriginUrl == null ? null : new GenericRoute(request.OriginUrl);

			return new SignInUpPageViewModel(originRoute);
		}
	}
}