﻿using Uniwiki.Server.WebApplication.Shared.Routes;
using Uniwiki.Server.WebApplication.Shared.ViewModels;
using Uniwiki.Server.WebApplication.Uni.HomePage;

namespace Uniwiki.Server.WebApplication.User.SignInUpPage
{
	public class SignInUpPageViewModel : PageLayoutViewModel
	{
		public SignInUpPageViewModel(GenericRoute? originRoute) : base(
			originRoute ?? (RouteBase) new HomePageRoute(),
			"Sign In / Up")
		{
		}
	}
}