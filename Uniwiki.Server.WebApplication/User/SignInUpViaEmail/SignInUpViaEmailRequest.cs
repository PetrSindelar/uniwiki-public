﻿namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmail
{
	public class SignInUpViaEmailRequest
	{
		public string Email { get; }
		public string Code { get; }

		public SignInUpViaEmailRequest(string email, string code)
		{
			Email = email;
			Code = code;
		}
	}
}