﻿namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmail
{
	public class SignInUpViaEmailResponse
	{
		public SignInUpViaEmailStatus Status { get; }
		public string? LoginToken { get; }

		public SignInUpViaEmailResponse(SignInUpViaEmailStatus status, string? loginToken = null)
		{
			Status = status;
			LoginToken = loginToken;
		}
	}
}