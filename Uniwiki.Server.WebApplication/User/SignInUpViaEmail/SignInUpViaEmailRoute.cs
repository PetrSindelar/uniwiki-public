﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmail
{
	public class SignInUpViaEmailRoute : RouteBase
	{
		private const string BaseRoute = ApiRoute + "/SignInUpViaEmail";
		public const string RouteAttribute = BaseRoute;

		public SignInUpViaEmailRoute()
		{
		}

		public override string Build() => $"{BaseRoute}";
	}
}