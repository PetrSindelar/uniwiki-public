﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebShared.Services.Abstractions;
using AuthenticationOptions = Uniwiki.Server.WebApplication.Shared.Configurations.AuthenticationOptions;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmail
{
	// TODO: Refresh token a week/month before expiry

	[ScopedService]
	public class SignInUpViaEmailServerAction
	{
		private readonly SignInManager<ProfileModel> _signInManager;
		private readonly UserManager<ProfileModel> _userManager;
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;
		private readonly AuthenticationOptions _authenticationOptions;

		public SignInUpViaEmailServerAction(SignInManager<ProfileModel> signInManager,
			UserManager<ProfileModel> userManager, UniwikiContext uniwikiContext,
			ITimeService timeService, IOptions<AuthenticationOptions> authenticationOptions)
		{
			_signInManager = signInManager;
			_userManager = userManager;
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
			_authenticationOptions = authenticationOptions.Value;
		}

		public async Task<SignInUpViaEmailResponse> ExecuteAsync(SignInUpViaEmailRequest request)
		{
			// TODO: Standardize

			// TODO: Validate
			// TODO: Validate that the code contains only numerical values

			// TODO: Verify code with email

			// TODO: Remove the code from the DB

			// TODO: Create some housework job - clean all the expired codes

			var confirmationCode = await _uniwikiContext.ConfirmationCodes.Where(c
					=> c.Email == request.Email && c.Code == request.Code && c.IsValid && c.Expiry >= _timeService.Now)
				.FirstOrDefaultAsync();

			if (confirmationCode == null)
			{
				throw new RequestException("The code is not valid.");
			}

			// Check if the user is registered
			var user = await _userManager.FindByEmailAsync(request.Email);

			// TODO: Handle the case if the user is disabled

			// If user is already registered
			if (user != null)
			{
				// Sign the user in
				var authenticationProperties = new AuthenticationProperties()
				{
					AllowRefresh = true,
					ExpiresUtc = _timeService.Now.AddMinutes(_authenticationOptions.ConfirmationCodeDurationMinutes),
					IsPersistent = true,
					IssuedUtc = _timeService.Now
				};

				await _signInManager.SignInAsync(user, authenticationProperties);

				await _uniwikiContext.SaveChangesAsync();
				// TODO: Invalidate the code


				return new SignInUpViaEmailResponse(SignInUpViaEmailStatus.SignedIn, "AWESOME TOKEN");
			}

			// Request the user to sign up
			return new SignInUpViaEmailResponse(SignInUpViaEmailStatus.SignUp);
		}
	}
}