﻿namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmail
{
	public enum SignInUpViaEmailStatus
	{
		SignedIn = 1,
		SignUp = 2,
	}
}