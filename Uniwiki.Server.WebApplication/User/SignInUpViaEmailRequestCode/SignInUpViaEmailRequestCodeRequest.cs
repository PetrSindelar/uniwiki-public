﻿namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmailRequestCode
{
	public class SignInUpViaEmailRequestCodeRequest
	{
		public string Email { get; }

		public SignInUpViaEmailRequestCodeRequest(string email)
		{
			Email = email;
		}
	}
}