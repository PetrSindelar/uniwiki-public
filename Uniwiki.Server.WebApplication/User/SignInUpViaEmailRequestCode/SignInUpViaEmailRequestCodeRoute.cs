﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmailRequestCode
{
	public class SignInUpViaEmailRequestCodeRoute : RouteBase
	{
		private const string BaseRoute = ApiRoute + "/SignInUpViaEmailRequestCode";
		public const string RouteAttribute = BaseRoute;

		public SignInUpViaEmailRequestCodeRoute()
		{
		}

		public override string Build() => $"{BaseRoute}";
	}
}