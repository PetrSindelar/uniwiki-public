﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.User.Shared.Services;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaEmailRequestCode
{
	[ScopedService]
	public class SignInUpViaEmailRequestCodeServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;
		private readonly UserManager<ProfileModel> _userManager;
		private readonly EmailVerificationService _emailVerificationService;
		private readonly AuthenticationOptions _authenticationOptions;

		public SignInUpViaEmailRequestCodeServerAction(UniwikiContext uniwikiContext, ITimeService timeService,
			IOptions<AuthenticationOptions> authenticationOptions, UserManager<ProfileModel> userManager,
			EmailVerificationService emailVerificationService)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
			_userManager = userManager;
			_emailVerificationService = emailVerificationService;
			_authenticationOptions = authenticationOptions.Value;
		}

		public async Task ExecuteAsync(SignInUpViaEmailRequestCodeRequest request)
		{
			// TODO: Standardize request
			var normalizedEmail = _userManager.NormalizeEmail(request.Email);

			// TODO: Validate request

			// TODO: Check if the user is disabled
			if (request.Email == "e@e.cz")
			{
				throw new RequestException("Your account has been suspended, you cannot sign in");
			}

			// TODO: Send the code to the email
			await _emailVerificationService.SendVerificationCodeAsync(normalizedEmail);
		}
	}
}