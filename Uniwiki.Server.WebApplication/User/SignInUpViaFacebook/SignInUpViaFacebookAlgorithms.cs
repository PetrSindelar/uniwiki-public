﻿using System;
using System.Linq;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Text;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaFacebook
{
	public class SignInUpViaFacebookAlgorithms
	{
		//public static string GenerateJwtToken(DateTime issueTime, TimeSpan duration, UserInfo user, string appSecret)
		//{
		//	// generate token that is valid for 7 days
		//	var tokenHandler = new JwtSecurityTokenHandler();
		//	var key = Encoding.ASCII.GetBytes(appSecret);
		//	var tokenDescriptor = new SecurityTokenDescriptor
		//	{
		//		Subject = new ClaimsIdentity(new[]
		//		{
		//			new Claim("id", user.UserId.ToString()),
		//			new Claim("firstName", user.FirstName),
		//			new Claim("familyName", user.FamilyName),
		//			new Claim("profileImageUrl", user.FullProfileImageUrl),
		//			new Claim("profileUrl", user.FullProfileUrl),
		//		}),
		//		IssuedAt = issueTime,
		//		Expires = issueTime.Add(duration),
		//		SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
		//			SecurityAlgorithms.HmacSha256Signature)
		//	};
		//	var token = tokenHandler.CreateToken(tokenDescriptor);
		//	return tokenHandler.WriteToken(token);
		//}

		public static string DecodeSignedRequest(string signedRequest, string appSecret)
		{
			if (!signedRequest.Contains("."))
			{
				throw new RequestException("The given request is invalid.");
			}

			string[] split = signedRequest.Split('.');

			string signatureRaw = FixBase64String(split[0]);
			string dataRaw = FixBase64String(split[1]);

			// the decoded signature
			byte[] signature = Convert.FromBase64String(signatureRaw);

			byte[] dataBuffer = Convert.FromBase64String(dataRaw);

			// JSON object
			string data = Encoding.UTF8.GetString(dataBuffer);

			// Calculate the expected hash
			byte[] appSecretBytes = Encoding.UTF8.GetBytes(appSecret);
			HMAC hmac = new HMACSHA256(appSecretBytes);
			byte[] expectedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(split[1]));

			// Compare hashes to check the validity
			if (!expectedHash.SequenceEqual(signature))
			{
				throw new AuthenticationException("This is not a valid Facebook token.");
			}

			return data;
		}

		private static string FixBase64String(string str)
		{
			while (str.Length % 4 != 0)
			{
				str = str.PadRight(str.Length + 1, '=');
			}

			return str.Replace("-", "+").Replace("_", "/");
		}
	}
}