﻿namespace Uniwiki.Server.WebApplication.User.SignInUpViaFacebook
{
	public class SignInUpViaFacebookRequest
	{
		public string SignedRequest { get; }

		public SignInUpViaFacebookRequest(string signedRequest)
		{
			SignedRequest = signedRequest;
		}
	}
}