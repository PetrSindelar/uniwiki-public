﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaFacebook
{
	public class SignInUpViaFacebookRoute : RouteBase
	{
		private const string BaseRoute = ApiRoute + "/SignInUpViaFacebook";
		public const string RouteAttribute = BaseRoute;
		public override string Build() => BaseRoute;
	}
}