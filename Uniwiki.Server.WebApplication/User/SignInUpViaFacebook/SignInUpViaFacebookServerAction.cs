﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.User.Shared.Services;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaFacebook
{
	[ScopedService]
	public class SignInUpViaFacebookServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ITimeService _timeService;
		private readonly ISignInUpService _signInUpService;
		private readonly UserManager<ProfileModel> _userManager;
		private readonly AuthenticationOptions _authenticationOptions;

		public SignInUpViaFacebookServerAction(UniwikiContext uniwikiContext, ITimeService timeService,
			IOptions<AuthenticationOptions> authenticationOptions, ISignInUpService signInUpService,
			UserManager<ProfileModel> userManager)
		{
			_uniwikiContext = uniwikiContext;
			_timeService = timeService;
			_signInUpService = signInUpService;
			_userManager = userManager;
			_authenticationOptions = authenticationOptions.Value;
		}

		public async Task ExecuteAsync(SignInUpViaFacebookRequest request)
		{
			// Decode the signed request
			var token = SignInUpViaFacebookAlgorithms.DecodeSignedRequest(request.SignedRequest,
				_authenticationOptions.FacebookAppSecret);

			Console.WriteLine(token);

			// Get the email of the user
			var email = "f@f.cz"; //TODO: use token.Email;

			// Try to get the user
			var user = await _uniwikiContext.Users.Where(p => p.Email == email).FirstOrDefaultAsync();

			// If the user does not exist - register him/her
			if (user == null)
			{
				var firstName = "Alena"; // TODO: Token.FirstName
				var familyName = "Veselá"; // TODO: Token.FamilyName

				var url = await Standardizers.CreateUrl(firstName + " " + familyName,
					u => _uniwikiContext.Users.AllAsync(p => p.Url != u));

				// Create a new user
				user = new ProfileModel(Guid.NewGuid(), email, firstName, familyName, url, "profile.jpg",
					_timeService.Now, AuthenticationLevel.RegularUser, null, 0);

				// Add the user to the DB
				var result = await _userManager.CreateAsync(user);

				// TODO: Handle the error better and display it to the user
				if (!result.Succeeded)
				{
					throw new RequestException("Could not sign up");
				}

				await _uniwikiContext.SaveChangesAsync();
			}

			// Login the user
			// Create a new login token
			var tokenDuration = TimeSpan.FromDays(_authenticationOptions.SignInTokenDurationDays);

			await _signInUpService.SignInAsync(email);
		}
	}
}