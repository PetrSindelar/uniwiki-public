﻿using System;

namespace Uniwiki.Server.WebApplication.User.SignInUpViaFacebook
{
	public class UserDto
	{
		public Guid Id { get; }
		public string FirstName { get; }
		public string FamilyName { get; }
		public string ProfileUrl { get; }
		public string ProfileImageUrl { get; }

		public UserDto(string familyName, string firstName, Guid id, string profileUrl, string profileImageUrl)
		{
			FamilyName = familyName;
			FirstName = firstName;
			Id = id;
			ProfileUrl = profileUrl;
			ProfileImageUrl = profileImageUrl;
		}
	}
}