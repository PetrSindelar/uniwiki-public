﻿namespace Uniwiki.Server.WebApplication.User.SignOut
{
	public class SignOutResponse
	{
		public bool ShouldRemoveSignInCookie { get; }

		public SignOutResponse(bool shouldRemoveSignInCookie)
		{
			ShouldRemoveSignInCookie = shouldRemoveSignInCookie;
		}
	}
}