﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignOut
{
	public class SignOutRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/SignOut";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => BaseRoute;
	}
}