﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;

namespace Uniwiki.Server.WebApplication.User.SignOut
{
	[ScopedService]
	public class SignOutServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly ISignInUpService _signInUpService;

		public SignOutServerAction(UniwikiContext uniwikiContext, ISignInUpService signInUpService)
		{
			_uniwikiContext = uniwikiContext;
			_signInUpService = signInUpService;
		}

		public async Task<SignOutResponse> ExecuteAsync(SignOutRequest request)
		{
			var shouldRemoveSignInCookie = true;

			await _signInUpService.SignOutAsync();

			var viewModel = new SignOutResponse(shouldRemoveSignInCookie);

			return viewModel;
		}
	}
}