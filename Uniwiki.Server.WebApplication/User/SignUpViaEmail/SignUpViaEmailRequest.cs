﻿namespace Uniwiki.Server.WebApplication.User.SignUpViaEmail
{
	public class SignUpViaEmailRequest
	{
		public string Email { get; }
		public string Code { get; }
		public string Name { get; }
		public string Surname { get; }

		public SignUpViaEmailRequest(string email, string code, string name, string surname)
		{
			Email = email;
			Code = code;
			Name = name;
			Surname = surname;
		}
	}
}