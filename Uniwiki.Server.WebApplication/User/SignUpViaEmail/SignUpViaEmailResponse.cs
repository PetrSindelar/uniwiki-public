﻿namespace Uniwiki.Server.WebApplication.User.SignUpViaEmail
{
	public class SignUpViaEmailResponse
	{
		public string Token { get; }

		public SignUpViaEmailResponse(string token)
		{
			Token = token;
		}
	}
}