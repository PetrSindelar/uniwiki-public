﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.SignUpViaEmail
{
	public class SignUpViaEmailRoute : RouteBase
	{
		private const string BaseRoute = ApiRoute + "/SignUpViaEmail";
		public const string RouteAttribute = BaseRoute;
		public override string Build() => BaseRoute;
	}
}