﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.User.Shared.Services;
using Uniwiki.Server.WebApplication.User.Shared.Services.Abstractions;

// TODO: Refactoring - unite Profile and Account modules into User
namespace Uniwiki.Server.WebApplication.User.SignUpViaEmail
{
	[ScopedService]
	public class SignUpViaEmailServerAction
	{
		private readonly UniwikiContext _uniwikiContext;
		private readonly SignInManager<ProfileModel> _signInManager;
		private readonly UserManager<ProfileModel> _userManager;
		private readonly ISignInUpService _signInUpService;
		private readonly EmailVerificationService _emailVerificationService;

		public SignUpViaEmailServerAction(UniwikiContext uniwikiContext, SignInManager<ProfileModel> signInManager,
			UserManager<ProfileModel> userManager, ISignInUpService signInUpService,
			EmailVerificationService emailVerificationService)
		{
			_uniwikiContext = uniwikiContext;
			_signInManager = signInManager;
			_userManager = userManager;
			_signInUpService = signInUpService;
			_emailVerificationService = emailVerificationService;
		}

		public async Task<SignUpViaEmailResponse> ExecuteAsync(SignUpViaEmailRequest request)
		{
			// TODO: Standardize
			var normalizedEmail = _userManager.NormalizeEmail(request.Email);

			// TODO: Validate

			// Try to get the code
			var isCodeValid = await _emailVerificationService.InvalidateCode(request.Code, normalizedEmail);

			// If the code does not exist, throw error
			if (!isCodeValid)
			{
				throw new RequestException("The code is not valid");
			}

			// Check if the user exists
			var userExists = _uniwikiContext.Users.Any(u => u.NormalizedEmail == normalizedEmail);

			// If the user does exists
			if (userExists)
			{
				throw new Exception("The email " + request.Email + " is already taken.");
			}

			// Create a new user
			await _signInUpService.SignUpAsync(normalizedEmail, request.Name, request.Surname, true);


			// Sign the user in
			await _signInUpService.SignInAsync(normalizedEmail);

			return new SignUpViaEmailResponse("");
		}
	}
}