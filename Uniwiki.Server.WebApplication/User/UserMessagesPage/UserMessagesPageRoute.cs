﻿using Uniwiki.Server.WebApplication.Shared.Routes;

namespace Uniwiki.Server.WebApplication.User.UserMessagesPage
{
	public class UserMessagesPageRoute : RouteBase
	{
		public const string BaseRoute = PageRoute + "/UserMessages";

		public const string RouteAttribute = BaseRoute;

		public override string Build() => BaseRoute;
	}
}