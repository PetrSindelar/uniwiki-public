﻿using System.Threading.Tasks;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Shared.DependencyInjection;

namespace Uniwiki.Server.WebApplication.User.UserMessagesPage
{
	[ScopedService]
	public class UserMessagesPageServerAction
	{
		private readonly UniwikiContext _uniwikiContext;

		public UserMessagesPageServerAction(UniwikiContext uniwikiContext)
		{
			_uniwikiContext = uniwikiContext;
		}

		public Task<UserMessagesPageViewModel> ExecuteAsync(UserMessagesPageRequest request)
		{
			var viewModel = new UserMessagesPageViewModel();

			return Task.FromResult(viewModel);
		}
	}
}