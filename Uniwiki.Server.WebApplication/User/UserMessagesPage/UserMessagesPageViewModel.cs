﻿using Uniwiki.Server.WebApplication.Shared.ViewModels;

namespace Uniwiki.Server.WebApplication.User.UserMessagesPage
{
	public class UserMessagesPageViewModel : PageLayoutViewModel
	{
		public UserMessagesPageViewModel() : base(null, "Messages")
		{
		}
	}
}