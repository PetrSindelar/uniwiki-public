﻿using System;

namespace Uniwiki.Server.WebShared.Services.Abstractions
{
	public interface ITimeService
	{
		public DateTime Now { get; }
	}
}