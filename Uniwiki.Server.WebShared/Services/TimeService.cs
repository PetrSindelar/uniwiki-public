﻿using System;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebShared.Services
{
	public class TimeService : ITimeService
	{
		public DateTime Now => DateTime.Now;
	}
}