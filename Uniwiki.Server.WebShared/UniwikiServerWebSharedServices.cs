﻿using Microsoft.Extensions.DependencyInjection;
using Uniwiki.Server.WebShared.Services;
using Uniwiki.Server.WebShared.Services.Abstractions;

namespace Uniwiki.Server.WebShared
{
	public static class UniwikiServerWebSharedServices
	{
		public static IServiceCollection AddUniwikiServerWebSharedServices(this IServiceCollection services)
			=> services.AddTransient<ITimeService, TimeService>();
	}
}