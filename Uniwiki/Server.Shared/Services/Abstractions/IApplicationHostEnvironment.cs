﻿namespace Shared.Services.Abstractions
{
    public interface IApplicationHostEnvironment
    {
        bool IsDevelopment();
        bool IsStaging();
        bool IsProduction();
        string Environment { get; }
    }
}
