﻿using System.Threading.Tasks;

namespace Uniwiki.Server.Host.Mvc
{
    public interface IMvcProcessor
    {
        Task<DataForClient<IResponse>> Process(IRequest request, InputContext inputContext);

    }
}