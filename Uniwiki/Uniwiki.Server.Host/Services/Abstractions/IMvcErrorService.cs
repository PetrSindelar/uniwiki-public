﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Uniwiki.Server.Host.Services.Abstractions
{
    public interface IMvcRequestExceptionHandlerService
    {
        ActionResult HandleRequestException(RequestException exception, ControllerBase controller);
        ActionResult HandleException(Exception exception, ControllerBase controller);
    }
}