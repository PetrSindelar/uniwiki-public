﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Uniwiki.Server.Persistence.Tests.Extensions
{
	public static class ObjectExtensions
	{
		public static void InitializeCollections(this object objectWithCollections)
			=> objectWithCollections
			   .GetType()
			   .GetProperties()
			   .Where(
				   p =>
					   p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(ICollection<>)
			   )
			   .ToList()
			   .ForEach(
				   p =>
					   {
						   // Get the generic type (specific T) in ICollection<T>
						   var genericType = p.PropertyType
											  .GetGenericArguments()[0];

						   // General generic type of list
						   Type genericListType = typeof(List<>);

						   // Specify the generic argument in the generic list type
						   Type concreteListType = genericListType.MakeGenericType(genericType);

						   // Create an instance of the list with genericType
						   var collection = Activator
							   .CreateInstance(
								   concreteListType
							   );

						   // Set the list as the value of the property
						   p.SetValue(objectWithCollections, collection);
					   }
			   );
	}
}
