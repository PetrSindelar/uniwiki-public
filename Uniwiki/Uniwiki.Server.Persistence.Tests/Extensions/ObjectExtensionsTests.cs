﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Uniwiki.Server.Persistence.Tests.Extensions
{
	[TestClass]
	public class ObjectExtensionsTests
	{
		public class ObjectWithCollections
		{
			public ICollection<string> Strings { get; set; } = null!;
			public string Value1 { get; set; } = null!;
			public int Value2 { get; set; }
			public ICollection<int> Numbers { get; set; } = null!;
			public ICollection<ModelBuilder> Builders { get; set; } = null!;
		}

		[TestMethod]
		public void InitializeCollections_InitializesCollectionsInObjectWithCollections()
		{
			// Arrange
			var obj = new ObjectWithCollections();

			// Act
			obj.InitializeCollections();

			// Assert
			Assert.IsNull(obj.Value1);
			Assert.IsNotNull(obj.Builders);
			Assert.IsNotNull(obj.Strings);
			Assert.IsNotNull(obj.Numbers);
		}
	}
}
