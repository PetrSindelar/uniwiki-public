﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class CourseModelBuilder : ModelBuilderBase<CourseModel>
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string?> Code { get; set; }
		public Optional<string?> CodeStandardized { get; set; }
		public Optional<string> LongName { get; set; }
		public Optional<string> LongNameStandardized { get; set; }
		public Optional<string> Url { get; set; }
		public Optional<string> StoragePath { get; set; }

		public Optional<FacultyModel> Faculty { get; set; }
		public Optional<ProfileModel> Author { get; set; }

		public override Func<Random, CourseModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					Faculty = Faculty.GetOr(() => Faculty.GetOr(() => new FacultyModelBuilder().Build(uniwikiContext)(random)));
					Author = Author.GetOr(() => Author.GetOr(() => new ProfileModelBuilder().Build(uniwikiContext)(random)));

					var course = new CourseModel(
						Id.GetOr(Guid.NewGuid),
						Code.GetOr(() => Generators.RandomName(Constants.Validations.CourseCodeMaxLength)(random)),
						CodeStandardized.GetOr(() => Generators.RandomNameStandardized(Constants.Validations.CourseCodeMaxLength)(random)),
						LongName.GetOr(() => Generators.RandomName(Constants.Validations.CourseNameMaxLength)(random)),
						LongNameStandardized.GetOr(() => Generators.RandomNameStandardized(Constants.Validations.CourseNameMaxLength)(random)),
						Author.Value.Id,
						Faculty.Value.Id,
						Url.GetOr(() => Generators.RandomUrl()(random)),
						StoragePath.GetOr(() => Generators.RandomUrl()(random))
					);

					course.Faculty = Faculty.Value;
					course.Faculty.Courses.Add(course);

					course.Author = Author.Value;
					course.Author.Courses.Add(course);

					course.InitializeCollections();

					// Add model to the DB
					uniwikiContext
						.Courses
						.Add(course)
						.Context
						.SaveChanges();

					return course;
				};
	}
}
