﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class FacultyModelBuilder : ModelBuilderBase<FacultyModel>
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string> LongName { get; set; }
		public Optional<string> ShortName { get; set; }
		public Optional<string> Url { get; set; }
		public Optional<Language> Language { get; set; }
		public Optional<string> StoragePath { get; set; }

		public Optional<UniversityModel> University { get; set; }

		public override Func<Random, FacultyModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					University = University.GetOr(() => new UniversityModelBuilder().Build(uniwikiContext)(random));

					var faculty = new FacultyModel(
						Id.GetOr(Guid.NewGuid),
						University.Value.Id,
						ShortName.GetOr(() => Generators.RandomName(Constants.Validations.FacultyShortNameMaxLength)(random)),
						LongName.GetOr(() => Generators.RandomName(Constants.Validations.FacultyLongNameMaxLength)(random)),
						Url.GetOr(() => Generators.RandomUrl()(random)),
						Language.GetOr(() => Shared.Language.Czech),
						StoragePath.GetOr(() => Generators.RandomUrl()(random))
					);

					faculty.University = University.Value;
					faculty.University.Faculties.Add(faculty);

					faculty.InitializeCollections();

					uniwikiContext.Faculties.Add(faculty);
					uniwikiContext.SaveChanges();

					return faculty;
				};
	}
}
