﻿using System;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public abstract class ModelBuilderBase<T>
	{
		public abstract Func<Random, T> Build(UniwikiContext uniwikiContext);
	}
}
