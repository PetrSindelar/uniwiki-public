﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class PostCommentLikeModelBuilder : ModelBuilderBase<PostCommentLikeModel>
	{
		public Optional<bool> IsLiked { get; set; }
		public Optional<DateTime> LikeTime { get; set; }

		public Optional<ProfileModel> Profile { get; set; }
		public Optional<PostCommentModel> PostComment { get; set; }

		public override Func<Random, PostCommentLikeModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					Profile = Profile.GetOr(() => new ProfileModelBuilder().Build(uniwikiContext)(random));
					PostComment = PostComment.GetOr(() => new PostCommentModelBuilder().Build(uniwikiContext)(random));

					var postCommentLike = new PostCommentLikeModel(
						PostComment.Value.Id,
						Profile.Value.Id,
						LikeTime.GetOr(() => Generators.RandomDate()(random)),
						IsLiked.GetOr(() => Generators.RandomBool()(random))
					);

					postCommentLike.Profile = Profile.Value;
					postCommentLike.Profile.PostCommentLikes.Add(postCommentLike);

					postCommentLike.PostComment = PostComment.Value;
					postCommentLike.PostComment.Likes.Add(postCommentLike);

					postCommentLike.InitializeCollections();

					uniwikiContext.PostCommentLikes.Add(postCommentLike);
					uniwikiContext.SaveChanges();

					return postCommentLike;
				};
	}
}
