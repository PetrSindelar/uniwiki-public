﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class PostCommentModelBuilder : ModelBuilderBase<PostCommentModel>
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string> Text { get; set; }
		public Optional<DateTime> CreationTime { get; set; }
		public Optional<ProfileModel> Author { get; set; }
		public Optional<PostModel> Post { get; set; }

		public override Func<Random, PostCommentModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					Author = Author.GetOr(() => new ProfileModelBuilder().Build(uniwikiContext)(random));
					Post = Post.GetOr(() => new PostModelBuilder().Build(uniwikiContext)(random));

					var postComment = new PostCommentModel(
						Id.GetOr(Guid.NewGuid),
						Author.Value.Id,
						Post.Value.Id,
						Text.GetOr(() => Generators.RandomText(Constants.Validations.PostCommentTextMinLength, Constants.Validations.PostCommentTextMaxLength)(random)),
						CreationTime.GetOr(() => Generators.RandomDate()(random))
					);

					postComment.Author = Author.Value;
					postComment.Author.PostComments.Add(postComment);

					postComment.Post = Post.Value;
					postComment.Post.Comments.Add(postComment);

					uniwikiContext.PostComments.Add(postComment);
					uniwikiContext.SaveChanges();

					return postComment;
				};
	}
}
