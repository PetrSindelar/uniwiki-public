﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class PostFileModelBuilder : ModelBuilderBase<PostFileModel>
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string> NameWithoutExtension { get; set; }
		public Optional<DateTime> CreationTime { get; set; }
		public Optional<long> Size { get; set; }
		public Optional<bool> IsImage { get; set; }
		public Optional<string> Url { get; set; }
		public Optional<ProfileModel> Profile { get; set; }
		public Optional<PostModel?> Post { get; set; }
		public Optional<CourseModel> Course { get; set; }

		public override Func<Random, PostFileModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					Profile = Profile.GetOr(() => new ProfileModelBuilder().Build(uniwikiContext)(random));
					Post = Post.GetOr(() => null);
					Course = Course.GetOr(() => new CourseModelBuilder().Build(uniwikiContext)(random));

					var postFile = new PostFileModel(
						Id.GetOr(Guid.NewGuid),
						NameWithoutExtension.GetOr(() => Generators.RandomName(Constants.Validations.FileNameMaxLength)(random)),
						NameWithoutExtension.GetOr(() => Generators.RandomName(Constants.Validations.FileExtensionMaxLength)(random)),
						true,
						Profile.Value.Id,
						Course.Value.Id,
						Post.Value?.Id,
						CreationTime.GetOr(() => Generators.RandomDate()(random)),
						Size.GetOr(() => Generators.RandomNumber((int)Constants.Validations.MaxFileSizeInBytes)(random)),
						IsImage.GetOr(() => Generators.RandomBool()(random)),
						Url.GetOr(() => Generators.RandomUrl()(random))
					);


					postFile.Profile = Profile.Value;
					postFile.Profile.PostFiles.Add(postFile);

					postFile.Post = Post.Value;
					postFile.Post?.PostFiles.Add(postFile);

					postFile.Course = Course.Value;
					postFile.CourseId = Course.Value.Id;

					postFile.InitializeCollections();

					uniwikiContext.PostFiles.Add(postFile);
					uniwikiContext.SaveChanges();

					return postFile;
				};
	}
}
