﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class PostLikeModelBuilder : ModelBuilderBase<PostLikeModel>
	{
		public Optional<DateTime> CreationTime { get; set; }
		public Optional<bool> IsLiked { get; set; }

		public Optional<PostModel> Post { get; set; }
		public Optional<ProfileModel> Profile { get; set; }

		public override Func<Random, PostLikeModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					Post = Post.GetOr(() => new PostModelBuilder().Build(uniwikiContext)(random));
					Profile = Profile.GetOr(() => new ProfileModelBuilder().Build(uniwikiContext)(random));

					var postLike = new PostLikeModel(
						Post.Value.Id,
						Profile.Value.Id,
						CreationTime.GetOr(() => Generators.RandomDate()(random)),
						IsLiked.GetOr(() => Generators.RandomBool()(random))
					);

					postLike.Profile = Profile.Value;
					postLike.Profile.PostLikes.Add(postLike);

					postLike.Post = Post.Value;
					postLike.Post.Likes.Add(postLike);


					postLike.InitializeCollections();

					uniwikiContext.PostLikes.Add(postLike);
					uniwikiContext.SaveChanges();

					return postLike;
				};
	}
}
