﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class PostModelBuilder
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string?> Category { get; set; }
		public Optional<string> Text { get; set; }
		public Optional<DateTime> CreationTime { get; set; }
		public Optional<string> Url { get; set; }

		public Optional<ProfileModel> Author { get; set; }
		public Optional<CourseModel> Course { get; set; }
		public Optional<bool> IsAnonymous { get; set; }

		public Func<Random, PostModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					Author = Author.GetOr(() => new ProfileModelBuilder().Build(uniwikiContext)(random));
					Course = Course.GetOr(() => new CourseModelBuilder().Build(uniwikiContext)(random));

					var post = new PostModel(
						Id.GetOr(Guid.NewGuid),
						Category.GetOr(() => Generators.RandomName(Constants.Validations.PostCategoryMaxLength)(random)),
						Author.Value.Id,
						Text.GetOr(() => Generators.RandomText(Constants.Validations.PostTextMinLength, Constants.Validations.PostTextMinLength)(random)),
						Course.Value.Id,
						CreationTime.GetOr(() => Generators.RandomDate()(random)),
						Url.GetOr(() => Generators.RandomUrl()(random)),
						IsAnonymous.GetOr(() => Generators.RandomBool()(random))
					);


					post.Author = Author.Value;
					post.Author.Posts.Add(post);

					post.Course = Course.Value;
					post.Course.Posts.Add(post);

					post.InitializeCollections();

					uniwikiContext.Posts.Add(post);
					uniwikiContext.SaveChanges();

					return post;
				};
	}
}
