﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class ProfileModelBuilder
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string> Email { get; set; }

		public Optional<DateTime> CreationDate { get; set; }

		public Optional<AuthenticationLevel> AuthenticationLevel { get; set; }

		public Optional<string> FirstName { get; set; }

		public Optional<string> FamilyName { get; set; }

		public Optional<string> Url { get; set; }

		public Optional<string> ProfilePictureSrc { get; set; }

		public Optional<FacultyModel?> HomeFaculty { get; set; }

		public Func<Random, ProfileModel> Build(UniwikiContext uniwikiContext)
			=> random =>
			{
				var profile = new ProfileModel(
					Id.GetOr(Guid.NewGuid),
					Email.GetOr(() => Generators.RandomEmail()(random)),
					FirstName.GetOr(
						() => Generators.RandomName(Constants.Validations.ProfileFirstNameMaxLength)(random)),
					FamilyName.GetOr(()
						=> Generators.RandomName(Constants.Validations.ProfileFamilyNameMaxLength)(random)),
					Url.GetOr(() => Generators.RandomUrl()(random)),
					ProfilePictureSrc.GetOr(() => Generators.RandomUrl()(random)),
					CreationDate.GetOr(() => Generators.RandomDate()(random)),
					AuthenticationLevel.GetOr(() => Persistence.AuthenticationLevel.RegularUser),
					HomeFaculty.GetOr(() => (FacultyModel?) null)?.Id,
					0 // TODO: Last notifications check should be some normal value...
				);

				profile.InitializeCollections();

				uniwikiContext.Users.Add(profile);
				uniwikiContext.SaveChanges();

				return profile;
			};
	}
}