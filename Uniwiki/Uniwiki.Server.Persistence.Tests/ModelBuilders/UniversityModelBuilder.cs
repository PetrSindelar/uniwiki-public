﻿using System;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Persistence.Tests.Extensions;
using Uniwiki.Server.Shared;
using Uniwiki.Server.Shared.Tests;

namespace Uniwiki.Server.Persistence.Tests.ModelBuilders
{
	public class UniversityModelBuilder
	{
		public Optional<Guid> Id { get; set; }
		public Optional<string> LongName { get; set; }
		public Optional<string> ShortName { get; set; }
		public Optional<string> Url { get; set; }
		public Optional<string> StoragePath { get; set; }

		public Func<Random, UniversityModel> Build(UniwikiContext uniwikiContext)
			=> random =>
				{
					var university = new UniversityModel(
						Id.GetOr(Guid.NewGuid),
						LongName.GetOr(() => Generators.RandomName(Constants.Validations.UniversityLongNameMaxLength)(random)),
						ShortName.GetOr(() => Generators.RandomName(Constants.Validations.UniversityShortNameMaxLength)(random)),
						Url.GetOr(() => Generators.RandomUrl()(random)),
						StoragePath.GetOr(() => Generators.RandomUrl()(random))
					);

					university.InitializeCollections();

					uniwikiContext.Universities.Add(university);
					uniwikiContext.SaveChanges();

					return university;
				};
	}
}
