﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Uniwiki.Server.Persistence.Models.Base;

namespace Uniwiki.Server.Persistence.Models
{
	public class ConfirmationCodeModel : ModelBase<Guid>
	{
		public string Code { get; set; } = null!;
		public string Email { get; set; } = null!;
		public DateTime Expiry { get; set; }
		public bool IsValid { get; set; }

		public ConfirmationCodeModel(Guid id, string code, string email, DateTime expiry, bool isValid)
			: base(id)
		{
			Code = code;
			Email = email;
			Expiry = expiry;
			IsValid = isValid;
		}

		protected ConfirmationCodeModel()
		{
		}

		public class Map : IEntityTypeConfiguration<ConfirmationCodeModel>
		{
			public void Configure(EntityTypeBuilder<ConfirmationCodeModel> builder) => builder.Property(m => m.IsValid).IsConcurrencyToken();

			//builder
			//	.HasMany(m => m.CourseVisits)
			//	.WithOne(v => v.Course)
			//	.OnDelete(DeleteBehavior.Cascade);
		}
	}
}
