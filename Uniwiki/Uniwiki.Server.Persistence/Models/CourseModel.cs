﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Uniwiki.Server.Persistence.Models.Base;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.Persistence.Models
{
	public class CourseModel : ModelBase<Guid>
	{
		[MaxLength(Constants.Validations.CourseCodeMaxLength)]
		public string? Code { get; set; }

		[MaxLength(Constants.Validations.CourseCodeMaxLength)]
		public string? CodeStandardized { get; set; }

		[MaxLength(Constants.Validations.CourseNameMaxLength)]
		public string LongName { get; set; } = null!;

		[MaxLength(Constants.Validations.CourseNameMaxLength)]
		public string LongNameStandardized { get; set; } = null!;

		public Guid FacultyId { get; set; }
		public FacultyModel Faculty { get; set; } = null!;
		public Guid AuthorId { get; set; }
		public ProfileModel Author { get; set; } = null!;

		[MaxLength(Constants.Validations.UrlMaxLength)]
		public string Url { get; set; } = null!;

		public string StoragePath { get; set; } = null!;

		public ICollection<PostModel> Posts { get; set; } = null!;
		public ICollection<CourseVisitModel> CourseVisits { get; set; } = null!;

		public CourseModel(
			Guid id,
			string? code,
			string? codeStandardized,
			string longName,
			string longNameStandardized,
			Guid authorId,
			Guid facultyId,
			string url,
			string storagePath
		)
			: base(id)
		{
			Code = code ?? string.Empty;
			CodeStandardized = codeStandardized ?? string.Empty;
			LongName = longName;
			LongNameStandardized = longNameStandardized;
			AuthorId = authorId;
			FacultyId = facultyId;
			Url = url;
			StoragePath = storagePath;
			Posts = new List<PostModel>();
			CourseVisits = new List<CourseVisitModel>();
		}

		protected CourseModel()
		{
		}

		public class Map : IEntityTypeConfiguration<CourseModel>
		{
			public void Configure(EntityTypeBuilder<CourseModel> builder)
			{
				builder
					.HasMany(m => m.CourseVisits)
					.WithOne(v => v.Course)
					.OnDelete(DeleteBehavior.Cascade);


				// TODO: I might want to have the index here...
				//builder
				//	.HasIndex(
				//		e => new {
				//			e.Url,
				//			e.StudyGroupUrl,
				//			e.UniversityUrl
				//		}
				//	);
			}
		}
	}
}
