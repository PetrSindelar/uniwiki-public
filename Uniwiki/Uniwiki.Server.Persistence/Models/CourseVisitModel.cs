﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Toolbelt.ComponentModel.DataAnnotations.Schema.V5;
using Uniwiki.Server.Persistence.Models.Base;

namespace Uniwiki.Server.Persistence.Models
{
	public class CourseVisitModel : ModelBase<Guid>
	{
		public class Map : IEntityTypeConfiguration<CourseVisitModel>
		{
			public void Configure(EntityTypeBuilder<CourseVisitModel> builder)
			{
				//builder
				//	.HasOne(m => m.Profile)
				//	.WithMany(v => v.CourseVisits)
				//	.OnDelete(DeleteBehavior.Cascade);
			}
		}

		public Guid CourseId { get; set; }
		public CourseModel Course { get; set; } = null!;
		public Guid ProfileId { get; set; }
		public ProfileModel Profile { get; set; } = null!;

		[IndexColumn]
		public DateTime VisitDateTime { get; set; }

		public CourseVisitModel(Guid id, Guid courseId, Guid profileId, DateTime visitDateTime)
			: base(id)
		{
			CourseId = courseId;
			ProfileId = profileId;
			VisitDateTime = visitDateTime;
		}

		protected CourseVisitModel()
		{
		}
	}
}
