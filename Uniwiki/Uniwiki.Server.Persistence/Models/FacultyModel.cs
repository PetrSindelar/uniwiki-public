﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Toolbelt.ComponentModel.DataAnnotations.Schema.V5;
using Uniwiki.Server.Persistence.Models.Base;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.Persistence.Models
{
	public class FacultyModel : ModelBase<Guid>
	{
		public Guid UniversityId { get; set; }
		public UniversityModel University { get; set; } = null!;

		[MaxLength(Constants.Validations.FacultyShortNameMaxLength)]
		public string ShortName { get; set; } = null!;

		[MaxLength(Constants.Validations.FacultyLongNameMaxLength)]
		public string LongName { get; set; } = null!;

		[IndexColumn(IsUnique = true)]
		[MaxLength(Constants.Validations.UrlMaxLength)]
		public string Url { get; set; } = null!;

		public string StoragePath { get; set; } = null!;

		public ICollection<CourseModel> Courses { get; set; } = null!;
		public Language PrimaryLanguage { get; set; }

		public FacultyModel(
			Guid id,
			Guid universityId,
			string shortName,
			string longName,
			string url,
			Language primaryLanguage,
			string storagePath
		)
			: base(id)
		{
			UniversityId = universityId;
			ShortName = shortName;
			LongName = longName;
			Url = url;
			PrimaryLanguage = primaryLanguage;
			StoragePath = storagePath;
			Courses = new List<CourseModel>();
		}

		protected FacultyModel()
		{
		}
	}
}
