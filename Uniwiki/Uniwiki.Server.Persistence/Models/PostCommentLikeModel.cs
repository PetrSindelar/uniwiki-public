﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Uniwiki.Server.Persistence.ModelIds;

namespace Uniwiki.Server.Persistence.Models
{
	public class PostCommentLikeModel
	{
		public class Map : IEntityTypeConfiguration<PostCommentLikeModel>
		{
			public void Configure(EntityTypeBuilder<PostCommentLikeModel> builder) => builder.HasKey(e => new PostCommentLikeModelId(e.PostCommentId, e.ProfileId));
		}

		public Guid PostCommentId { get; set; }
		public PostCommentModel PostComment { get; set; } = null!;
		public Guid ProfileId { get; set; }
		public ProfileModel Profile { get; set; } = null!;
		public DateTime LikeTime { get; set; }
		public bool IsLiked { get; set; }

		public PostCommentLikeModel(Guid postCommentId, Guid profileId, DateTime likeTime, bool isLiked)
		{
			PostCommentId = postCommentId;
			ProfileId = profileId;
			LikeTime = likeTime;
			IsLiked = isLiked;
		}

		protected PostCommentLikeModel()
		{
		}
	}
}
