﻿using System;
using System.ComponentModel.DataAnnotations;
using Uniwiki.Server.Persistence.Models.Base;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.Persistence.Models
{
	public class PostFileModel : ModelBase<Guid>
	{
		[MaxLength(Constants.Validations.FileNameMaxLength + 1 + Constants.Validations.FileExtensionMaxLength)]
		public string OriginalFullName => $"{NameWithoutExtension}{Extension}";

		[MaxLength(Constants.Validations.FileNameMaxLength)]
		public string NameWithoutExtension { get; set; } = null!;

		[MaxLength(Constants.Validations.FileExtensionMaxLength)]
		public string Extension { get; set; } = null!;

		public bool IsSaved { get; set; }
		public Guid ProfileId { get; set; }
		public ProfileModel Profile { get; set; } = null!;
		public Guid CourseId { get; set; }
		public CourseModel Course { get; set; } = null!;
		public DateTime CreationTime { get; set; }
		public long Size { get; set; }

		public bool IsImage { get; set; }

		// At the beginning its uninitialized! Because files are uploaded before a post is created
		public Guid? PostId { get; set; }
		public PostModel? Post { get; set; }
		public string Url { get; set; } = null!;

		public PostFileModel(
			Guid id,
			string nameWithoutExtension,
			string extension,
			bool isSaved,
			Guid profileId,
			Guid courseId,
			Guid? postId,
			DateTime creationTime,
			long size,
			bool isImage,
			string url
		)
			: base(id)
		{
			NameWithoutExtension = nameWithoutExtension;
			Extension = extension;
			IsSaved = isSaved;
			ProfileId = profileId;
			CourseId = courseId;
			PostId = postId;
			CreationTime = creationTime;
			Size = size;
			IsImage = isImage;
			Url = url;
		}

		protected PostFileModel()
		{
		}

		internal void FileSaved()
		{
			IsSaved = true;
		}

		internal void SetFileNameWithoutExtension(string newFileNameWithoutExtension)
		{
			NameWithoutExtension = newFileNameWithoutExtension;
		}

		internal void SetPostId(Guid postId)
		{
			PostId = postId;
		}
	}
}
