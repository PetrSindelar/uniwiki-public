﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.Persistence.Models
{
	public class ProfileModel : IdentityUser<Guid>
	{
		public DateTime CreationDate { get; set; }

		public AuthenticationLevel AuthenticationLevel { get; set; }
		public Guid? HomeFacultyId { get; set; }
		public FacultyModel? HomeFaculty { get; set; }

		[MaxLength(Constants.Validations.ProfileFirstNameMaxLength)]
		public string FirstName { get; set; } = null!;

		[MaxLength(Constants.Validations.ProfileFamilyNameMaxLength)]
		public string FamilyName { get; set; } = null!;

		[MaxLength(Constants.Validations.UrlMaxLength)]
		public string Url { get; set; } = null!;

		[MaxLength(Constants.Validations.UrlMaxLength)]
		public string? ProfileImageUrl { get; set; }

		public long LastNotificationsCheck { get; set; }

		public ICollection<CourseVisitModel> CourseVisits { get; set; } = null!;
		public ICollection<FeedbackModel> Feedbacks { get; set; } = null!;
		public ICollection<CourseModel> Courses { get; set; } = null!;
		public ICollection<PostModel> Posts { get; set; } = null!;
		public ICollection<PostLikeModel> PostLikes { get; set; } = null!;
		public ICollection<PostCommentModel> PostComments { get; set; } = null!;
		public ICollection<PostCommentLikeModel> PostCommentLikes { get; set; } = null!;
		public ICollection<PostFileModel> PostFiles { get; set; } = null!;

		//public ICollection<EmailConfirmationSecretModel> EmailConfirmationSecrets { get; set; } = null!;
		//public ICollection<NewPasswordSecretModel> NewPasswordSecrets { get; set; } = null!;

		public ProfileModel(Guid id,
			string email,
			string firstName,
			string familyName,
			string url,
			string? profileImageUrl,
			DateTime creationDate,
			AuthenticationLevel authenticationLevel,
			Guid? homeFacultyId,
			long lastNotificationsCheck)
		{
			Id = id;
			UserName = Email = email;
			LastNotificationsCheck = lastNotificationsCheck;
			//UserName = email;

			//Password = password;
			//PasswordSalt = passwordSalt;
			CreationDate = creationDate;

			//IsConfirmed = isConfirmed;
			AuthenticationLevel = authenticationLevel;
			HomeFacultyId = homeFacultyId;
			ProfileImageUrl = profileImageUrl;
			FirstName = firstName;
			FamilyName = familyName;
			Url = url;
			CourseVisits = new List<CourseVisitModel>();
			Feedbacks = new List<FeedbackModel>();
			Courses = new List<CourseModel>();
			Posts = new List<PostModel>();
			PostLikes = new List<PostLikeModel>();
			PostComments = new List<PostCommentModel>();
			PostCommentLikes = new List<PostCommentLikeModel>();


			//EmailConfirmationSecrets = new List<EmailConfirmationSecretModel>();
			//NewPasswordSecrets = new List<NewPasswordSecretModel>();
			//EmailConfirmationSecrets = new List<EmailConfirmationSecretModel>();
			//NewPasswordSecrets = new List<NewPasswordSecretModel>();
		}

		// For Entity framework
		protected ProfileModel()
		{
		}

		internal void SetAuthenticationLevel(AuthenticationLevel authenticationLevel)
			=> AuthenticationLevel = authenticationLevel;

		internal void SetHomeFaculty(Guid? homeFacultyId) => HomeFacultyId = homeFacultyId;

		public class Map : IEntityTypeConfiguration<ProfileModel>
		{
			public void Configure(EntityTypeBuilder<ProfileModel> builder)
			{
				builder
					.HasMany(m => m.CourseVisits)
					.WithOne(v => v.Profile)
					.OnDelete(DeleteBehavior.Restrict);

				builder
					.HasMany(m => m.PostCommentLikes)
					.WithOne(p => p.Profile)
					.OnDelete(DeleteBehavior.Restrict);

				builder
					.HasMany(m => m.PostComments)
					.WithOne(c => c.Author)
					.OnDelete(DeleteBehavior.Restrict);

				builder
					.HasMany(m => m.PostLikes)
					.WithOne(p => p.Profile)
					.OnDelete(DeleteBehavior.Restrict);

				builder
					.HasMany(m => m.Posts)
					.WithOne(p => p.Author)
					.OnDelete(DeleteBehavior.Restrict);

				builder
					.HasMany(m => m.PostFiles)
					.WithOne(p => p.Profile)
					.OnDelete(DeleteBehavior.Restrict);
			}
		}
	}
}