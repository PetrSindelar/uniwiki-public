﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Uniwiki.Server.Persistence.Models.Base;
using Uniwiki.Server.Shared;

namespace Uniwiki.Server.Persistence.Models
{
	public class UniversityModel : ModelBase<Guid>
	{
		/// <summary>
		///     e. g. 'IT University of Copenhagen'
		/// </summary>
		[MaxLength(Constants.Validations.UniversityLongNameMaxLength)]
		public string LongName { get; set; } = null!;

		/// <summary>
		///     e. g. 'ITU'
		/// </summary>
		[MaxLength(Constants.Validations.UniversityShortNameMaxLength)]
		public string ShortName { get; set; } = null!;

		[MaxLength(Constants.Validations.UrlMaxLength)]
		public string Url { get; set; } = null!;

		public string StoragePath { get; set; } = null!;

		public ICollection<FacultyModel> Faculties { get; set; } = null!;

		public UniversityModel(Guid id, string longName, string shortName, string url, string storagePath)
			: base(id)
		{
			LongName = longName;
			ShortName = shortName;
			Url = url;
			StoragePath = storagePath;
			Faculties = new List<FacultyModel>();
		}

		protected UniversityModel()
		{
		}
	}
}
