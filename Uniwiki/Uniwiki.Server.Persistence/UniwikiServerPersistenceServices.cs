﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Uniwiki.Server.Persistence.Services;
using Uniwiki.Server.WebShared;

[assembly: InternalsVisibleTo("Uniwiki.Tests")]
[assembly: InternalsVisibleTo("Uniwiki.Server.Application.Tests")]
[assembly: InternalsVisibleTo("Uniwiki.Server.Persistence.Tests")]

namespace Uniwiki.Server.Persistence
{
	public static class UniwikiServerPersistenceServices
	{
		public static IServiceCollection AddUniwikiServerPersistence(this IServiceCollection services, Action<DbContextOptionsBuilder> databaseBuilder)
		{
			//services.AddSharedServices();
			services.AddUniwikiServerWebSharedServices();

			services.AddScoped<TextService>();
			services.AddDbContext<UniwikiContext>(databaseBuilder);

			return services;
		}
	}
}
