﻿using System;

namespace Uniwiki.Server.Shared
{
	public class Constants
	{
		//public Constants(IApplicationHostEnvironment applicationHostEnvironment) => _applicationHostEnvironment = applicationHostEnvironment;

		/// <summary>
		///     Determines how much time the user has to wait in order for him to be possible to have the confirmation email
		///     resent.
		/// </summary>
		public static TimeSpan ResendRegistrationEmailMinTime => TimeSpan.FromSeconds(30);

		public static TimeSpan RestorePasswordSecretExpiration => TimeSpan.FromHours(5);
		public static TimeSpan RestorePasswordSecretPause => TimeSpan.FromSeconds(30);
		public static TimeSpan LoginTokenLife => TimeSpan.FromDays(60);

		public static TimeSpan DownloadAgainTime = TimeSpan.FromSeconds(10);

		public const string FacebookLink = "https://www.facebook.com/UniwikiOfficial";

		public const int NumberOrRecentCourses = 4;

		public const string FileUploadDataField = "Data";

		//public const Language DefaultLanguage = Language.English;

		public const int MaxPostsToFetch = 10;

		//private readonly IApplicationHostEnvironment _applicationHostEnvironment;

		//public int PasswordMinLength => _applicationHostEnvironment.IsProduction() ? 6 : 1; // Require 6 digit password just for production

		public class Validations
		{
			public const int CourseCodeMaxLength = 10;
			public const int CourseCodeMinLength = 0;
			public const int CourseNameMaxLength = 95;
			public const int CourseNameMinLength = 3;

			public const int FacultyLongNameMaxLength = 70;
			public const int FacultyShortNameMaxLength = 10;
			public const int FacultyLongNameMinLength = 4;
			public const int FacultyShortNameMinLength = 1;
			public const int PostTextMaxLength = 50_000;
			public const int PostTextMinLength = 1;
			public const int FileNameMaxLength = 240;
			public const int FileExtensionMaxLength = 20;
			public const int PostCategoryMaxLength = 25;

			public const int ProfileFirstNameMaxLength = 35;
			public const int ProfileFamilyNameMaxLength = 35;
			public const int UserNameAndSurnameMinLength = 3;
			public const int UserNameAndSurnameMaxLength = ProfileFirstNameMaxLength + 1 + ProfileFamilyNameMaxLength;

			public const int UrlMaxLength = 40;
			public const int UrlMinLength = 3;
			public const int EmailMaxLength = 254;
			public const int PasswordMaxLength = 128;
			public const int UniversityLongNameMaxLength = 100;
			public const int UniversityShortNameMaxLength = 15;

			#if DEBUG
			public const long MaxFileSizeInBytesForUpload = 1_000_000_000;
			public const long MaxFileSizeInBytesInDatabase = 1_000_000_000;
			#else
			public const long MaxFileSizeInBytesForUpload = 50_000_000;
	        public const long MaxFileSizeInBytes = 50_000_000;
			#endif

			public readonly static char[] AllowedFileSpecialCharacters = {
				'<',
				'>',
				'(',
				')',
				'.',
				' ',
				'[',
				']',
				'{',
				'}',
				'-',
				'=',
				'*',
				'!',
				'@',
				'#',
				'%',
				'+',
				'_',
				',',
				'$'
			};

			public const int PostCommentTextMaxLength = 2000;

			public const int PostCommentTextMinLength = 1;
		}
	}
}
