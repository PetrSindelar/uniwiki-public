﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Uniwiki.Server.Shared.DependencyInjection
{
	public class ScopedServiceAttribute : Attribute
	{
		public Type? ServiceType { get; }
		public bool AutoDetectService { get; }
		public ServiceLifetime ServiceLifetime { get; }

		public ScopedServiceAttribute(Type? serviceType = null, bool autoDetectService = true, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
		{
			ServiceType = serviceType;
			AutoDetectService = autoDetectService;
			ServiceLifetime = serviceLifetime;
		}
	}
}
