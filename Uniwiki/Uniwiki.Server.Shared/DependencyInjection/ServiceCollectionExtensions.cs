﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace Uniwiki.Server.Shared.DependencyInjection
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddAttributeServices(this IServiceCollection services,
			Assembly? assembly = null)
		{
			assembly ??= Assembly.GetCallingAssembly();


			assembly
				.GetTypes()
				.Where( // Get all types, that are marked as services
					type => type.CustomAttributes
						.Any(att => att.AttributeType == typeof(ScopedServiceAttribute))
				)
				.ToList()
				.ForEach(
					implementationType =>
					{
						var attribute =
							(ScopedServiceAttribute?) Attribute.GetCustomAttribute(implementationType,
								typeof(ScopedServiceAttribute));

						if (attribute == null)
						{
							return;
						}

						var serviceType = attribute.ServiceType;

						if (serviceType == null && attribute.AutoDetectService)
						{
							// Auto detect the service 
							var interfaces = implementationType.GetInterfaces();

							// If it has more than one inteface
							if (interfaces.Length >= 2)
							{
								// Tell the developer to specify which interface should be used for registering
								throw new ArgumentException(
									$"The type '{implementationType.FullName}' implements multiple interfaces, so it not obvious, which should be used for the registration to DI. Use the '{nameof(ScopedServiceAttribute.ServiceType)}' property of the attribute '{nameof(ScopedServiceAttribute)}' to specify the service type."
								);
							}

							if (interfaces.Length == 1)
							{
								serviceType = interfaces.First();
							}
						}

						services.Add(new ServiceDescriptor(serviceType ?? implementationType, implementationType,
							attribute.ServiceLifetime));
					}
				);

			return services;
		}
	}
}