﻿namespace Uniwiki.Server.Shared.Exceptions
{
	public class NotFoundException : RequestException
	{
		public NotFoundException(string message = "Resource not found.")
			: base(message)
		{
		}
	}

	public class NotAuthorizedException : RequestException
	{
		public NotAuthorizedException(string message = "Resource not found.")
			: base(message)
		{
		}
	}
}
