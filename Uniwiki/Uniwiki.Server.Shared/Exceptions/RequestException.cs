﻿using System;

namespace Uniwiki.Server.Shared.Exceptions
{
	public class RequestException : Exception
	{
		public RequestException(string message)
			: base(message)
		{
		}
	}
}
