﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uniwiki.Server.Shared.Exceptions
{
	public class ServerException : Exception
	{
		public string DebugMessage { get; }

		public ServerException(string userMessage = "", string debugMessage = "")
			: base(userMessage)
		{
			DebugMessage = debugMessage;
		}
	}
}