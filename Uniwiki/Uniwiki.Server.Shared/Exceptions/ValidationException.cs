﻿using System;

namespace Uniwiki.Server.Shared.Exceptions
{
	public class ValidationException : Exception
	{
		public ValidationException(string message = "Validation failed.")
			: base(message)
		{
		}
	}
}
