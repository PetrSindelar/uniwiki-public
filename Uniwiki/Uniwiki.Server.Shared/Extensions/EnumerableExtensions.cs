﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Uniwiki.Server.Shared.Extensions
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<T> Empty<T>() => new T[0];

		public static void ForEach<T>(
			this IEnumerable<T> source,
			Action<T> action
		)
		{
			foreach (var element in source)
			{
				action(element);
			}
		}

		public static async Task ForEachAsync<T>(
			this IEnumerable<T> source,
			Func<T, Task> action
		)
		{
			foreach (var element in source)
			{
				await action(element);
			}
		}
	}
}
