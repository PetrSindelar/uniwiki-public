﻿using System;
using Uniwiki.Server.Shared.Exceptions;

namespace Uniwiki.Server.Shared.Extensions
{
	public static class FunctionalExtensions
	{
		public static TOut Apply<TIn, TOut>(this TIn obj, Func<TIn, TOut> transform)
			=> (obj?.Equals(default) ?? false ? default : transform(obj)) ?? throw new InvalidOperationException("Just got null here..");

		//public static U Apply<T, U>(this T obj, Func<T, U> transform)
		//	where T : class
		//	=> obj is null ? default : transform(obj);

		// Applies the transform function on the given object, but returns the original object
		public static T ApplySideEffect<T>(this T obj, Action<T> transform)
		{
			if (!(obj is null))
			{
				transform(obj);
			}

			return obj;
		}

		public static void IfFalseThrowRequestException(this bool value)
		{
			if (!value)
			{
				throw new RequestException("The condition was false");
			}
		}

		public static void IfTrueThrowRequestException(this bool value)
		{
			if (value)
			{
				throw new RequestException("The condition was true");
			}
		}
	}
}
