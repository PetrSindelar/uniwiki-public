﻿using System.Linq;

namespace Uniwiki.Server.Shared.Helpers
{
	public static class StringHelper
	{
		public static bool IsNullOrWhitespace(params string[] strings) => strings.Aggregate(false, (b, s) => b || string.IsNullOrWhiteSpace(s));
	}
}
