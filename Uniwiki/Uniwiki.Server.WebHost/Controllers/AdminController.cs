﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Uniwiki.Server.WebApplication.Admin.AddFaculty;
using Uniwiki.Server.WebApplication.Admin.AddFacultyPage;
using Uniwiki.Server.WebApplication.Admin.AddUniversity;
using Uniwiki.Server.WebApplication.Admin.AddUniversityPage;
using Uniwiki.Server.WebApplication.Admin.AdminDashboardPage;
using Uniwiki.Server.WebApplication.Shared.Authentication;

namespace Uniwiki.Server.WebHost.Controllers
{
	[Authorize(Roles = CustomRoles.Administrator)]
	public class AdminController : Controller
	{
		[Route(AdminDashboardPageRoute.RouteAttribute)]
		public async Task<IActionResult> AdminDashboardPage([FromServices] AdminDashboardPageServerAction serverAction)
		{
			var request = new AdminDashboardPageRequest();

			var viewModel = await serverAction.ExecuteAsync(request);

			return View(viewModel);
		}

		[Route(AddUniversityPageRoute.RouteAttribute)]
		public async Task<IActionResult> AddUniversityPage([FromServices] AddUniversityPageServerAction serverAction)
		{
			var request = new AddUniversityPageRequest();

			var viewModel = await serverAction.ExecuteAsync(request);

			return View(viewModel);
		}

		[Route(AddFacultyPageRoute.RouteAttribute)]
		public async Task<IActionResult> AddFacultyPage(
			[FromRoute(Name = AddFacultyPageRoute.UniversityUrlRouteParameter)]
			string universityUrl,
			[FromServices] AddFacultyPageServerAction serverAction
		)
		{
			var request = new AddFacultyPageRequest(universityUrl);

			var viewModel = await serverAction.ExecuteAsync(request);

			return View(viewModel);
		}

		[HttpPost]
		[Route(AddUniversityRoute.RouteAttribute)]
		public async Task<IActionResult> AddUniversity(
			[FromForm(Name = nameof(AddUniversityPageViewModel.AddUniversityPageForm))]
			AddUniversityPageForm addUniversityPageForm,
			[FromServices] AddUniversityServerAction serverAction
		)
		{
			var request = new AddUniversityRequest(
				addUniversityPageForm.LongName,
				addUniversityPageForm.ShortName,
				addUniversityPageForm.Url
			);

			var response = await serverAction.ExecuteAsync(request);

			return Redirect(response.RedirectRoute.Build());
		}

		[Route(AddFacultyRoute.RouteAttribute)]
		public async Task<IActionResult> AddFaculty(
			[FromForm] AddFacultyPageForm addFacultyPageForm,
			[FromServices] AddFacultyServerAction serverAction
		)
		{
			var request = new AddFacultyRequest(
				addFacultyPageForm.UniversityId,
				addFacultyPageForm.FacultyLongName,
				addFacultyPageForm.FacultyShortName,
				addFacultyPageForm.FacultyUrl,
				addFacultyPageForm.PrimaryLanguage
			);

			var response = await serverAction.ExecuteAsync(request);

			return Redirect(response.RedirectRoute.ToString());
		}
	}
}