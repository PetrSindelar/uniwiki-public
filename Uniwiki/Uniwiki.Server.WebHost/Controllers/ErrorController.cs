﻿using System;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Error.ErrorPage;
using Uniwiki.Server.WebHost.Services;

namespace Uniwiki.Server.WebHost.Controllers
{
	public class ErrorController : Controller
	{
		[AllowAnonymous]
		[HttpGet("/error")]
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult ErrorPage([FromServices] ErrorService errorService, int? statusCode = null)
		{
			if (errorService.IsApi())
			{
				return StatusCode(statusCode ?? StatusCodes.Status500InternalServerError);
			}

			// TODO: Create a custom page for every error
			if (!statusCode.HasValue)
			{
				return View("ErrorPage", new ErrorPageViewModel("There was an error :(", "Error"));
			}

			if (statusCode.Value == StatusCodes.Status404NotFound)
			{
				var vm = new ErrorPageViewModel("This page does not exist :)", "Not found");
				return View("ErrorPage", vm);
			}

			if (statusCode.Value == StatusCodes.Status500InternalServerError)
			{
				var vm = new ErrorPageViewModel("We ran into an error on the server :(", "Server error");
				return View("ErrorPage", vm);
			}

			if (statusCode.Value == StatusCodes.Status400BadRequest)
			{
				var vm = new ErrorPageViewModel("There was a problem with the request", "Request error");
				return View("ErrorPage", vm);
			}

			if (statusCode.Value == StatusCodes.Status403Forbidden)
			{
				var vm = new ErrorPageViewModel("You are not authorized to access this page", "Forbidden");
				return View("ErrorPage", vm);
			}

			return View("ErrorPage", new ErrorPageViewModel("There was an error :(", "Error"));
		}


		/// <summary>
		/// This route takes an unhandled exception and translates it to an error code
		/// </summary>
		/// <param name="logger"></param>
		/// <param name="errorService"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[Route("/HandleException")]
		public IActionResult HandleException([FromServices] ILogger<ErrorController> logger,
			[FromServices] ErrorService errorService)
		{
			logger.LogWarning("HandleException Started");

			var exceptionHandlerPathFeature =
				HttpContext.Features.Get<IExceptionHandlerPathFeature>();


			if (exceptionHandlerPathFeature.Error is NotFoundException ex)
			{
				if (errorService.IsApi())
				{
					return StatusCode(StatusCodes.Status400BadRequest, ex.Message);
				}

				return ErrorPage(errorService, StatusCodes.Status404NotFound);
			}

			if (exceptionHandlerPathFeature.Error is NotAuthorizedException ex2)
			{
				if (errorService.IsApi())
				{
					return StatusCode(StatusCodes.Status403Forbidden, ex2.Message);
				}

				return ErrorPage(errorService, StatusCodes.Status403Forbidden);
			}

			if (exceptionHandlerPathFeature.Error is RequestException ex3)
			{
				if (errorService.IsApi())
				{
					return StatusCode(StatusCodes.Status400BadRequest, ex3.Message);
				}

				return ErrorPage(errorService, StatusCodes.Status400BadRequest);
			}

			if (errorService.IsApi())
			{
				return StatusCode(StatusCodes.Status500InternalServerError, "Server error");
			}

			return ErrorPage(errorService, StatusCodes.Status400BadRequest);
		}
	}
}