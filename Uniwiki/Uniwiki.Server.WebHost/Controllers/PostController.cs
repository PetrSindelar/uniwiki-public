﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Uniwiki.Server.Shared;
using Uniwiki.Server.WebApplication.Post.AddPost;
using Uniwiki.Server.WebApplication.Post.AddPostPage;
using Uniwiki.Server.WebApplication.Post.EditPost;
using Uniwiki.Server.WebApplication.Post.EditPostPage;
using Uniwiki.Server.WebApplication.Post.RemovePostFile;
using Uniwiki.Server.WebApplication.Post.RenamePostFile;
using Uniwiki.Server.WebApplication.Post.UploadPostFile;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebApplication.Uni.AddPostComment;
using Uniwiki.Server.WebApplication.Uni.CoursePage;
using Uniwiki.Server.WebApplication.Uni.DownloadPostFile;
using Uniwiki.Server.WebApplication.Uni.LikePost;
using Uniwiki.Server.WebApplication.Uni.LikePostComment;
using Uniwiki.Server.WebApplication.Uni.PostPage;

namespace Uniwiki.Server.WebHost.Controllers
{
	public class PostController : Controller
	{
		[AllowAnonymous]
		[Route(PostPageRoute.RouteAttribute)]
		public async Task<IActionResult> PostPage(
			[FromRoute(Name = PostPageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = PostPageRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = PostPageRoute.CourseRouteParameter)]
			string courseUrl,
			[FromRoute(Name = PostPageRoute.PostRouteParameter)]
			string postUrl,
			[FromServices] PostPageServerAction postPageServerAction
		)
		{
			var request = new PostPageRequest(
				universityUrl,
				facultyUrl,
				courseUrl,
				postUrl
			);

			var vm = await postPageServerAction.ExecuteAsync(request);

			return View(vm);
		}

		[Authorize]
		[HttpPost]
		[Route(LikePostRoute.RouteAttribute)]
		public async Task<IActionResult> LikePost(
			[FromRoute(Name = LikePostRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = LikePostRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = LikePostRoute.CourseRouteParameter)]
			string courseUrl,
			[FromRoute(Name = LikePostRoute.PostRouteParameter)]
			string postUrl,
			[FromRoute(Name = LikePostRoute.IsLikeRouteParameter)]
			bool isLike,
			[FromServices] LikePostServerAction likePostServerAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserInfoOrThrow().UserClaims.UserId;

			var request = new LikePostRequest(
				universityUrl,
				facultyUrl,
				courseUrl,
				postUrl,
				isLike,
				userId
			);

			await likePostServerAction.ExecuteAsync(request);

			return Ok();
		}

		[Authorize]
		[HttpGet]
		[Route(AddPostPageRoute.RouteAttribute)]
		public async Task<IActionResult> AddPostPage(
			[FromRoute(Name = AddPostPageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = AddPostPageRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = AddPostPageRoute.CourseRouteParameter)]
			string courseUrl,
			[FromServices] AddPostPageServerAction addPostPageServerAction
		)
		{
			var request = new AddPostPageRequest(universityUrl, facultyUrl, courseUrl);

			var viewModel = await addPostPageServerAction.ExecuteAsync(request);

			return View("AddOrEditPostPage", viewModel);
		}

		[Authorize]
		[HttpPost]
		[Route(AddPostRoute.RouteAttribute)]
		public async Task<IActionResult> AddPost(
			[FromRoute(Name = AddPostRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = AddPostRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = AddPostRoute.CourseRouteParameter)]
			string courseUrl,
			[FromServices] AddPostServerAction addPostServerAction,
			[FromForm(Name = nameof(AddPostPageViewModel.AddOrEditPostForm))]
			AddOrEditPostForm addOrEditPostForm,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserIdOrThrow();

			var request = new AddPostRequest(
				universityUrl,
				facultyUrl,
				courseUrl,
				addOrEditPostForm.Category,
				addOrEditPostForm.Description,
				addOrEditPostForm.PostIsAnonymous,
				userId
			);

			var viewModel = await addPostServerAction.ExecuteAsync(request);

			return Redirect(viewModel.RedirectRoute.ToString());
		}

		[Authorize]
		[HttpPost]
		[Route(UploadPostFileRoute.RouteAttribute)]
		[RequestSizeLimit(Constants.Validations.MaxFileSizeInBytesForUpload)]
		[RequestFormLimits(MultipartBodyLengthLimit = Constants.Validations.MaxFileSizeInBytesForUpload)]
		public async Task<IActionResult> UploadPostFile(
			[FromRoute(Name = UploadPostFileRoute.CourseIdRouteParameter)]
			Guid courseId,
			[FromRoute(Name = UploadPostFileRoute.PostIdRouteParameter)]
			Guid? postId,
			[FromForm(Name = "File")] IFormFile file,
			[FromServices] UploadPostFileServerAction uploadPostFileServerAction,
			[FromServices] IOptions<StorageConfiguration> storageConfiguration,
			[FromServices] IUserInfoService userInfoService
		)
		{
			// Chack if there are some files
			if (HttpContext.Request.Form.Files.Count != 1)
			{
				return BadRequest();
			}

			var userId = userInfoService.GetUserIdOrThrow();

			var request = new UploadPostFileRequest(file, courseId, storageConfiguration.Value.StorageRootPath,
				userId, postId);

			var viewModel = await uploadPostFileServerAction.ExecuteAsync(request);

			return PartialView("Components/PostFileComponent", viewModel);
		}

		[Authorize]
		[HttpPost]
		[Route(RenamePostFileRoute.RouteAttribute)]
		public async Task<IActionResult> RenamePostFile(
			[FromRoute(Name = RenamePostFileRoute.PostFileIdParameter)]
			Guid postFileId,
			[FromServices] RenamePostFileServerAction renamePostFileServerAction,
			[FromForm] RenamePostFileForm renamePostFileForm,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserIdOrThrow();

			var request = new RenamePostFileRequest(postFileId, renamePostFileForm.FileName, userId, false);

			await renamePostFileServerAction.ExecuteAsync(request);

			return Ok();
		}

		[Authorize]
		[HttpPost]
		[Route(RemovePostFileRoute.RouteAttribute)]
		public async Task<IActionResult> RemovePostFile(
			[FromRoute(Name = RemovePostFileRoute.PostFileIdParameter)]
			Guid postFileId,
			[FromServices] RemovePostFileServerAction removePostFileServerAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserIdOrThrow();

			var request = new RemovePostFileRequest(postFileId, userId, false);

			await removePostFileServerAction.ExecuteAsync(request);

			return Ok();
		}

		[Authorize]
		[Route(EditPostPageRoute.RouteAttribute)]
		public async Task<IActionResult> EditPostPage(
			[FromRoute(Name = EditPostPageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = EditPostPageRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = EditPostPageRoute.CourseRouteParameter)]
			string courseUrl,
			[FromRoute(Name = EditPostPageRoute.PostRouteParameter)]
			string postUrl,
			[FromServices] EditPostPageServerAction editPostPageServerAction
		)
		{
			var request = new EditPostPageRequest(
				universityUrl,
				facultyUrl,
				courseUrl,
				postUrl
			);

			var viewModel = await editPostPageServerAction.ExecuteAsync(request);

			return View("AddOrEditPostPage", viewModel);
		}

		[Authorize]
		[Route(EditPostRoute.RouteAttribute)]
		public async Task<IActionResult> EditPostPage(
			[FromRoute(Name = EditPostRoute.PostIdRouteParameter)]
			Guid postId,
			[FromForm] AddOrEditPostForm addOrEditPostForm,
			[FromServices] EditPostServerAction editPostServerAction
		)
		{
			var request = new EditPostRequest(
				postId,
				addOrEditPostForm.Description,
				addOrEditPostForm.Category,
				addOrEditPostForm.PostIsAnonymous
			);

			var viewModel = await editPostServerAction.ExecuteAsync(request);

			return Redirect(viewModel.RedirectRoute.ToString());
		}

		[Authorize]
		[HttpPost]
		[Route(AddPostCommentRoute.RouteAttribute)]
		public async Task<IActionResult> AddPostComment(
			[FromRoute(Name = AddPostCommentRoute.PostIdRouteParameter)]
			Guid postId, // This might not work
			[FromServices] AddPostCommentServerAction addPostCommentServerAction,
			[FromForm(Name = nameof(PostPageViewModel.AddPostCommentForm))]
			AddPostCommentForm addPostCommentForm,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserIdOrThrow();

			var request = new AddPostCommentRequest(
				postId,
				addPostCommentForm.Text,
				userId
			);

			await addPostCommentServerAction.ExecuteAsync(request);

			return Ok();
		}

		[Authorize]
		[Route(DownloadPostFileRoute.RouteAttribute)]
		public async Task<IActionResult> DownloadPostFile(
			[FromRoute(Name = DownloadPostFileRoute.PostFileUrlRouteParameter)]
			string postFileUrl, // This might not work
			[FromServices] DownloadPostFileServerAction downloadPostFileServerAction,
			[FromServices] IOptions<StorageConfiguration> storageConfiguration
		)
		{
			var request = new DownloadPostFileRequest(postFileUrl, storageConfiguration.Value.StorageRootPath);

			var viewModel = await downloadPostFileServerAction.ExecuteAsync(request);

			return PhysicalFile(Path.GetFullPath(viewModel.FilePath), viewModel.ContentType, viewModel.FileName);
		}

		[Authorize]
		[HttpPost]
		[Route(LikePostCommentRoute.RouteAttribute)]
		public async Task<IActionResult> LikePostComment(
			[FromRoute(Name = LikePostCommentRoute.PostCommentIdRouteParameter)]
			Guid commentId,
			[FromRoute(Name = LikePostCommentRoute.IsLikeRouteParameter)]
			bool isLike,
			[FromServices] LikePostCommentServerAction likePostCommentServerAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserIdOrThrow();

			var request = new LikePostCommentRequest(
				commentId,
				isLike,
				userId
			);

			await likePostCommentServerAction.ExecuteAsync(request);

			return Ok();
		}

		[AllowAnonymous]
		[Route(CoursePageRoute.RouteAttribute)]
		public async Task<IActionResult> CoursePage(
			[FromRoute(Name = CoursePageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = CoursePageRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = CoursePageRoute.CourseRouteParameter)]
			string courseUrl,
			[FromServices] CoursePageServerAction courseServerAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserId();

			var request = new CoursePageRequest(universityUrl, facultyUrl, courseUrl, userId);

			var vm = await courseServerAction.ExecuteAsync(request);

			return View(vm);
		}
	}
}