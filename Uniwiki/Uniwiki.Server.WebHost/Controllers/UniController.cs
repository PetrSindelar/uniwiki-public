﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebApplication.Uni.AddCourse;
using Uniwiki.Server.WebApplication.Uni.AddCoursePage;
using Uniwiki.Server.WebApplication.Uni.AddFacultyRequestPageRoute;
using Uniwiki.Server.WebApplication.Uni.AddUniversityRequestPage;
using Uniwiki.Server.WebApplication.Uni.HomePage;
using Uniwiki.Server.WebApplication.Uni.RenameCourseRequestPage;
using Uniwiki.Server.WebApplication.Uni.ReportCourseRequestPage;
using Uniwiki.Server.WebApplication.Uni.ReportPostRequestPage;
using Uniwiki.Server.WebApplication.Uni.ReportUserRequestPage;
using Uniwiki.Server.WebApplication.Uni.SelectCoursePage;
using Uniwiki.Server.WebApplication.Uni.SelectFacultyPage;
using Uniwiki.Server.WebApplication.Uni.SetCourseNotifications;

namespace Uniwiki.Server.WebHost.Controllers
{
	public class UniController : Controller
	{
		[AllowAnonymous]
		[Route("/PrestigeUi")]
		public IActionResult PrestigeUi() => View("PrestigeUi");

		[AllowAnonymous]
		[Route("/")]
		[Route(HomePageRoute.RouteAttribute)]
		public async Task<IActionResult> HomePage([FromServices] HomePageServerAction homeServerAction)
		{
			var request = await homeServerAction.ExecuteAsync();

			return View(request);
		}

		[AllowAnonymous]
		[Route(SelectFacultyPageRoute.RouteAttribute)]
		public async Task<IActionResult> SelectFacultyPage(
			[FromRoute(Name = SelectFacultyPageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromServices] SelectFacultyPageServerAction selectFacultyServerAction
		)
		{
			var request = new SelectFacultyPageRequest(universityUrl);

			var vm = await selectFacultyServerAction.ExecuteAsync(request);

			return View(vm);
		}

		[AllowAnonymous]
		[Route(SelectCoursePageRoute.RouteAttribute)]
		public async Task<IActionResult> SelectCoursePage(
			[FromRoute(Name = SelectCoursePageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = SelectCoursePageRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromServices] SelectCoursePageServerAction serverAction
		)
		{
			var request = new SelectCoursePageRequest(universityUrl, facultyUrl);

			var vm = await serverAction.ExecuteAsync(request);

			return View(vm);
		}

		[Authorize]
		[HttpPost]
		[Route(SetCourseNotificationsRoute.RouteAttribute)]
		public IActionResult SetCourseNotifications(
			[FromRoute(Name = SetCourseNotificationsRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = SetCourseNotificationsRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromRoute(Name = SetCourseNotificationsRoute.CourseRouteParameter)]
			string courseUrl,
			[FromRoute(Name = SetCourseNotificationsRoute.SetOnRouteParameter)]
			bool setOn
		)
		{
			// TODO: Set notifications on/off
			return Ok();
		}

		[Authorize]
		[Route(AddCoursePageRoute.RouteAttribute)]
		public async Task<IActionResult> AddCoursePage(
			[FromRoute(Name = AddCoursePageRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = AddCoursePageRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromServices] AddCoursePageServerAction serverAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserInfoOrThrow().UserClaims.UserId;

			var request = new AddCoursePageRequest(universityUrl, facultyUrl, userId);

			var vm = await serverAction.ExecuteAsync(request);

			return View("AddCoursePage", vm);
		}

		[Authorize]
		[HttpPost]
		[Route(AddCourseRoute.RouteAttribute)]
		public async Task<IActionResult> AddCourse(
			[FromRoute(Name = AddCourseRoute.UniversityRouteParameter)]
			string universityUrl,
			[FromRoute(Name = AddCourseRoute.FacultyRouteParameter)]
			string facultyUrl,
			[FromForm(Name = nameof(AddCoursePageViewModel.AddCourseForm))]
			AddCourseForm addCourseForm,
			[FromServices] AddCourseServerAction serverAction,
			[FromServices] AddCoursePageServerAction pageServerAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			if (!ModelState.IsValid)
			{
				return await AddCoursePage(universityUrl, facultyUrl, pageServerAction, userInfoService);
			}

			var userId = userInfoService.GetUserInfoOrThrow().UserClaims.UserId;

			var request = new AddCourseRequest(universityUrl, facultyUrl, addCourseForm.CourseName,
				addCourseForm.CourseCode, userId);
			var vm = await serverAction.ExecuteAsync(request);

			return Redirect(vm.RedirectRoute.Build());
		}


		[AllowAnonymous]
		[Route(AddUniversityRequestPageRoute.RouteAttribute)]
		public async Task<IActionResult> AddUniversityRequestPage(
			[FromServices] AddUniversityRequestPageServerAction serverAction)
		{
			var request = new AddUniversityRequestPageRequest();

			var viewModel = await serverAction.ExecuteAsync(request);

			return View("GoogleFormPage", viewModel);
		}

		[AllowAnonymous]
		[Route(AddFacultyRequestPageRoute.RouteAttribute)]
		public async Task<IActionResult> AddFacultyRequestPage(
			[FromRoute(Name = AddFacultyRequestPageRoute.UniversityUrlRouteParameter)]
			string universityUrl,
			[FromServices] AddFacultyRequestPageServerAction addFacultyRequestPageServerAction
		)
		{
			var request = new AddFacultyRequestPageRequest(universityUrl);

			var viewModel = await addFacultyRequestPageServerAction.ExecuteAsync(request);

			return View("GoogleFormPage", viewModel);
		}

		[AllowAnonymous]
		[Route(RenameCourseRequestPageRoute.RouteAttribute)]
		public async Task<IActionResult> RenameCourseRequestPage(
			[FromRoute(Name = RenameCourseRequestPageRoute.CourseIdRouteParameter)]
			Guid courseId,
			[FromServices] RenameCourseRequestPageServerAction renameCourseRequestPageServerAction
		)
		{
			var request = new RenameCourseRequestPageRequest(courseId);

			var viewModel = await renameCourseRequestPageServerAction.ExecuteAsync(request);

			return View("GoogleFormPage", viewModel);
		}

		[AllowAnonymous]
		[Route(ReportCourseRequestPageRoute.RouteAttribute)]
		public async Task<IActionResult> RenameCourseRequestPage(
			[FromRoute(Name = ReportCourseRequestPageRoute.CourseIdRouteParameter)]
			Guid courseId,
			[FromServices] ReportCourseRequestPageServerAction reportCourseRequestPageServerAction
		)
		{
			var request = new ReportCourseRequestPageRequest(courseId);

			var viewModel = await reportCourseRequestPageServerAction.ExecuteAsync(request);

			return View("GoogleFormPage", viewModel);
		}

		[AllowAnonymous]
		[Route(ReportPostRequestPageRoute.RouteAttribute)]
		public async Task<IActionResult> ReportPostRequestPage(
			[FromRoute(Name = ReportPostRequestPageRoute.PostIdRouteParameter)]
			Guid postId,
			[FromServices] ReportPostRequestPageServerAction reportPostRequestPageServerAction
		)
		{
			var request = new ReportPostRequestPageRequest(postId);

			var viewModel = await reportPostRequestPageServerAction.ExecuteAsync(request);

			return View("GoogleFormPage", viewModel);
		}

		[AllowAnonymous]
		[Route(ReportUserRequestPageRoute.RouteAttribute)]
		public async Task<IActionResult> ReportUserRequestPage(
			[FromRoute(Name = ReportUserRequestPageRoute.UserIdRouteParameter)]
			Guid userId,
			[FromServices] ReportUserRequestPageServerAction reportUserRequestPageServerAction
		)
		{
			var request = new ReportUserRequestPageRequest(userId);

			var viewModel = await reportUserRequestPageServerAction.ExecuteAsync(request);

			return View("GoogleFormPage", viewModel);
		}
	}
}