﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Profile.ProfilePage;
using Uniwiki.Server.WebApplication.Shared.Extensions;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebApplication.Uni.HomePage;
using Uniwiki.Server.WebApplication.User.FetchUsersNotifications;
using Uniwiki.Server.WebApplication.User.Shared.Dtos;
using Uniwiki.Server.WebApplication.User.SignInUpPage;
using Uniwiki.Server.WebApplication.User.SignInUpViaEmail;
using Uniwiki.Server.WebApplication.User.SignInUpViaEmailRequestCode;
using Uniwiki.Server.WebApplication.User.SignInUpViaFacebook;
using Uniwiki.Server.WebApplication.User.SignOut;
using Uniwiki.Server.WebApplication.User.SignUpViaEmail;
using Uniwiki.Server.WebApplication.User.UserMessagesPage;

namespace Uniwiki.Server.WebHost.Controllers
{
	public class UserController : Controller
	{
		/// <summary>
		/// Displays all available claims and the userInfo For development purposes only.
		/// </summary>
		/// <param name="webHostEnvironment"></param>
		/// <param name="userInfoService"></param>
		/// <returns></returns>
		[Authorize]
		[Route("/UserInfo")]
		public IActionResult UserInfo([FromServices] IWebHostEnvironment webHostEnvironment,
			[FromServices] IUserInfoService userInfoService)
		{
			if (!webHostEnvironment.IsDevelopment())
			{
				return NotFound();
			}

			if (User.Identity == null)
			{
				return Ok("No user log in");
			}

			string userInfo = userInfoService.GetUserInfoOrThrow().PropertiesToString();

			string claims = User.Claims.Aggregate("Claims: ",
				(acc, claim) => acc + "\n> " + claim.Type + ": " + claim.Value);

			return Ok(userInfo + "\n" + claims);
		}

		[AllowAnonymous]
		[Route(SignInUpPageRoute.RouteAttribute)]
		public IActionResult SignInUpPage(
			[FromQuery(Name = SignInUpPageRoute.BackUrlQueryParameter)]
			string? backUrl,
			[FromQuery(Name = SignInUpPageRoute.RedirectUrlQueryParameter)]
			string? redirectUrl,
			[FromServices] SignInUpPageServerAction serverAction
		)
		{
			var request = new SignInUpPageRequest(backUrl);

			var viewModel = serverAction.Execute(request);

			return View(viewModel);
		}

		[AllowAnonymous]
		[HttpPost]
		[Route(SignInUpViaFacebookRoute.RouteAttribute)]
		public async Task<IActionResult> SignInUpViaFacebook([FromBody] string signedRequest,
			[FromServices] SignInUpViaFacebookServerAction serverAction)
		{
			if (string.IsNullOrWhiteSpace(signedRequest))
			{
				return BadRequest("Supply the signed request");
			}

			var request = new SignInUpViaFacebookRequest(signedRequest);

			await serverAction.ExecuteAsync(request);

			return Ok();
		}

		[AllowAnonymous]
		[HttpPost]
		[Route(SignInUpViaEmailRequestCodeRoute.RouteAttribute)]
		public async Task<IActionResult> SignInUpViaEmailRequestCode([FromBody] string email,
			[FromServices] ILogger<UserController> logger,
			[FromServices] SignInUpViaEmailRequestCodeServerAction serverAction)
		{
			if (string.IsNullOrWhiteSpace(email))
			{
				return BadRequest();
			}

			var request = new SignInUpViaEmailRequestCodeRequest(email);

			try
			{
				await serverAction.ExecuteAsync(request);
			}
			catch (RequestException requestException)
			{
				// TODO: Log this exception
				logger.LogError(requestException, "Error during requesting email confirmation code.");

				return BadRequest(requestException.Message);
			}

			return Ok();
		}

		[AllowAnonymous]
		[HttpPost]
		[Route(SignInUpViaEmailRoute.RouteAttribute)]
		public async Task<ActionResult<SignInUpStatusDto>> SignInUpViaEmail(
			[FromBody] SignInUpViaEmailDto signInUpViaEmailDto,
			[FromServices] SignInUpViaEmailServerAction serverAction)
		{
			if (string.IsNullOrWhiteSpace(signInUpViaEmailDto.Code) ||
			    string.IsNullOrWhiteSpace(signInUpViaEmailDto.Email))
			{
				return BadRequest();
			}

			try
			{
				var request = new SignInUpViaEmailRequest(signInUpViaEmailDto.Email, signInUpViaEmailDto.Code);

				var result = await serverAction.ExecuteAsync(request);

				var statusDto = result.Status switch
				{
					var status when status == SignInUpViaEmailStatus.SignUp => SignInUpStatusCodeDto.SignUp,
					var status when status == SignInUpViaEmailStatus.SignedIn => SignInUpStatusCodeDto.SignedIn,
					var status => throw new ArgumentException("Enum SignInUpViaEmailStatus does not contain value: " +
					                                          status)
				};

				return Ok(new SignInUpStatusDto(statusDto));
			}
			catch (RequestException requestException)
			{
				// TODO: Log this exception
				return BadRequest(requestException.Message);
			}
		}

		[AllowAnonymous]
		[HttpPost]
		[Route(SignUpViaEmailRoute.RouteAttribute)]
		public async Task<ActionResult> SignUpViaEmail([FromBody] SignUpViaEmailDto signUpViaEmailDto,
			[FromServices] SignUpViaEmailServerAction serverAction)
		{
			if (string.IsNullOrWhiteSpace(signUpViaEmailDto.Code) ||
			    string.IsNullOrWhiteSpace(signUpViaEmailDto.Email) ||
			    string.IsNullOrWhiteSpace(signUpViaEmailDto.FirstName) ||
			    string.IsNullOrWhiteSpace(signUpViaEmailDto.Surname))
			{
				return BadRequest();
			}

			try
			{
				var request = new SignUpViaEmailRequest(signUpViaEmailDto.Email, signUpViaEmailDto.Code,
					signUpViaEmailDto.FirstName, signUpViaEmailDto.Surname);

				_ = await serverAction.ExecuteAsync(request);

				return Ok();
			}
			catch (RequestException requestException)
			{
				// TODO: Log this exception
				return BadRequest(requestException.Message);
			}
		}

		[AllowAnonymous]
		[Route(ProfilePageRoute.RouteAttribute)]
		public async Task<ActionResult> ProfilePage(
			[FromRoute] string profileUrl,
			[FromServices] ProfilePageServerAction serverAction
		)
		{
			if (string.IsNullOrWhiteSpace(profileUrl))
			{
				return BadRequest();
			}

			var request = new ProfilePageRequest(profileUrl);

			var viewModel = await serverAction.ExecuteAsync(request);

			return View(viewModel);
		}

		[Authorize]
		[HttpPost]
		[Route(SignOutRoute.RouteAttribute)]
		public async Task<IActionResult> SignOut([FromServices] SignOutServerAction serverAction)
		{
			var request = new SignOutRequest();

			_ = await serverAction.ExecuteAsync(request);

			return Redirect(new HomePageRoute().Build());
		}


		[Authorize]
		[Route(FetchUsersNotificationsRoute.RouteAttribute)]
		public async Task<IActionResult> FetchUsersNotifications(
			[FromRoute(Name = FetchUsersNotificationsRoute.LastTimestampRouteParameter)]
			long? lastTimestamp,
			[FromServices] FetchUsersNotificationsServerAction serverAction,
			[FromServices] IUserInfoService userInfoService
		)
		{
			var userId = userInfoService.GetUserIdOrThrow();

			var request = new FetchUsersNotificationsRequest(lastTimestamp, userId);

			var viewModel = await serverAction.ExecuteAsync(request);


			if (viewModel.NotificationsItems.Any())
			{
				// Show the fetched notifications
				return PartialView("Partials/_NotificationItemsPartial", viewModel.NotificationsItems);
			}

			if (lastTimestamp == null)
			{
				// Tell the user, that he does not have any notifications
				return PartialView("Partials/_NoNotificationItemsAvailablePartial");
			}

			// Tell the user, that he has fetched all the notifications
			return PartialView("Partials/_NoMoreNotificationItemsAvailablePartial");
		}


		[Authorize]
		[Route(UserMessagesPageRoute.RouteAttribute)]
		public async Task<IActionResult> UserMessagesPage(
			[FromServices] UserMessagesPageServerAction serverAction
		)
		{
			var request = new UserMessagesPageRequest();

			var viewModel = await serverAction.ExecuteAsync(request);

			return View(viewModel);
		}
	}
}