﻿namespace Uniwiki.Server.WebHost
{
	public static class CssConstants
	{
		public const string Status = "status";
		public const string StatusReady = "status-ready";
		public const string StatusInProgress = "status-in-progress";
		public const string ShowOnStatusInProgress = "show-on-status-in-progress";
		public const string ShowOnStatusReady = "show-on-status-ready";
	}
}
