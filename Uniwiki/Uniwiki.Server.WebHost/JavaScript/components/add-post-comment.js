﻿function addPostCommentOnSubmit(event) {
	event.preventDefault();

	const attributeAddPostCommentUrl = "addPostCommentUrl";

	// Get the text inserted by the user
	const commentTextElement = event.target.elements.text;
	const commentText = commentTextElement.value;

	// Get the template for the new comment
	const template = document.getElementById("commentTemplate");

	let newCommentElement = template.content.cloneNode(true);

	const newCommentText = newCommentElement.querySelector('[name="commentText"]');
	newCommentText.innerText = commentText;

	// Try to find the container for the comment
	let commentsContainer = document.getElementById("commentsContainer").lastElementChild;

	// If there is no container or the container is for comments of another user
	if (!commentsContainer || !commentsContainer.classList.contains("users-comments")) {
		const containerTemplate = document.getElementById("commentContainerTemplate");
		commentsContainer = containerTemplate.content.cloneNode(true);
		document.getElementById("commentsContainer").appendChild(commentsContainer);
	}

	// Try to find the container for the comment
	commentsContainer = document.getElementById("commentsContainer").lastElementChild;
	
	// Add the comment to the right container
	commentsContainer.appendChild(newCommentElement);

	newCommentElement = commentsContainer.lastElementChild;
	
	// Empty the input for commenting
	commentTextElement.value = "";

	sendAddPostCommentRequest(newCommentElement, commentText);
}

function repeatAddPostComment(event) {
	const commentElement = event.currentTarget.parentElement.parentElement;
	commentElement.classList.remove("sending-failed");

	const commentText = commentElement.getAttribute("text-to-send");

	sendAddPostCommentRequest(commentElement, commentText);
}

function sendAddPostCommentRequest(newCommentElement, text) {
	const url = document.getElementById("addCommentForm").getAttribute("addPostCommentUrl");
	
	const formData = new FormData();
	formData.append("AddPostCommentForm.Text", text);

	// Send the comment to the server
	sendPostFormData(url, formData, function () {
		console.log(newCommentElement);
		newCommentElement.setAttribute("text-to-send", text);
		newCommentElement.classList.add("sending-failed");
	});
}