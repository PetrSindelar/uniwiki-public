﻿var constants = {
	classDNone: "d-none",
	classStatus: "status",
	classStatusReady: "status-ready",
	classStatusInProgress: "status-in-progress",
	classShowOnStatusInProgress: "show-on-status-in-progress",
	classShowOnStatusReady: "show-on-status-ready",
}