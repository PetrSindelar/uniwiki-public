﻿function sendPost(url, onError, body, onSuccess) {
	let xhttp = new XMLHttpRequest();
	let fullUrl = new URL(url, window.location.origin);

	xhttp.open("POST", fullUrl, true);
	xhttp.setRequestHeader("Content-Type", "application/json");

	// Handle errors
	xhttp.onreadystatechange = function () {
		if (this.readyState === 4 && this.status !== 200) {
			console.log(`Encountered error (${this.status}), while sending: '${url}'`);
			const errorHandled = onError(this);
			if (!errorHandled) {
				Snackbar.show({ text: 'Conection error.', actionTextColor: 'red' });
			}
		}

		if (this.readyState === 4 && this.status === 200) {
			if (onSuccess)
				onSuccess(this);
		}
	};

	xhttp.send(body);

	return xhttp;
}

function sendPostFormData(url, formData, onError) {
	let xhttp = new XMLHttpRequest();
	let fullUrl = new URL(url, window.location.origin);

	xhttp.open("POST", fullUrl, true);

	// Handle errors
	xhttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status !== 200) {
			console.log(`Encountered error (${this.status}), while sending: '${url}'`);
			onError(this);
			console.log(Snackbar);
			Snackbar.show({ text: 'Conection error.', actionTextColor: 'red' });
		}
	};

	xhttp.send(formData);
}

function courseItemClick(event) {
	// Get the parent node
	const courseItem = event.currentTarget.parentNode.parentNode;

	const classNotificationsOn = "course-item-notification-on";
	const classNotificationsOff = "course-item-notification-off";
	const attributeSetNotificationsUrlOn = "set-notifications-on-url";
	const attributeSetNotificationsUrlOff = "set-notifications-off-url";

	// Makes it look like notifications are on
	const setNotificationsOn = function () {
		courseItem.classList.remove(classNotificationsOff);
		courseItem.classList.add(classNotificationsOn);
	}

	// Makes it look like notifications are off
	const setNotificationsOff = function () {
		courseItem.classList.remove(classNotificationsOn);
		courseItem.classList.add(classNotificationsOff);
	}

	// If notifications are on
	if (courseItem.classList.contains(classNotificationsOn)) {
		// Switch notifications off
		setNotificationsOff();
		sendPost(courseItem.getAttribute(attributeSetNotificationsUrlOff), setNotificationsOn);
	}
	// Else if notifications are off
	else if (courseItem.classList.contains(classNotificationsOff)) {
		// Switch notifications on
		setNotificationsOn();
		sendPost(courseItem.getAttribute(attributeSetNotificationsUrlOn), setNotificationsOff);
	}
	// Throw error in any other case
	else {
		throw Error(`The item does not have a specified ${classNotificationsOn} or ${classNotificationsOff} class`);
	}
}