﻿function likePostComment(event) {

	const classLiked = "post-comment-like-button-active";
	const attributeLikeRouteTrue = "likeRouteTrue";
	const attributeLikeRouteFalse = "likeRouteFalse";

	const button = event.currentTarget;

	const isLiked = button.classList.contains(classLiked);

	if (isLiked) {
		// Remove the liked class
		button.classList.remove(classLiked);

		// Increase the amount of likes by 1
		const likesCountSpan = button.getElementsByTagName('span')[0];
		likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) - 1;
		
		// Get url for calling to dislike
		const url = button.getAttribute(attributeLikeRouteFalse);

		// Like buttons of recently created comments do not have an url - so we just fake dislike it :)
		if (url) {

			// Call the ajax to dislike the post
			sendPost(url,
				function() {
					button.classList.add(classLiked);
					likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) + 1;
				});
		}
	}
	else {
		// Add the liked class
		button.classList.add(classLiked);

		// Decrease the amount of likes by 1
		const likesCountSpan = button.getElementsByTagName('span')[0];
		likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) + 1;

		// Get url for calling to dislike
		const url = button.getAttribute(attributeLikeRouteTrue);

		// Like buttons of recently created comments do not have an url - so we just fake like it :)
		if (url) {

			// Call the ajax to dislike the post
			sendPost(url,
				function() {
					button.classList.remove(classLiked);
					likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) - 1;
				}
			);
		}
	}

}