﻿function renamePostFileOnSubmit(event, {
	renamePostFileUrl,
	fileNameInputName,
	postFileComponentId,
	postFileExtension,
	postFileComponentNameElementName,
	modalId
}) {
	event.preventDefault();
	console.log('event');
	console.log(event);

	// Get the new file name
	const fileNameInput = document.getElementById(modalId).querySelector('[name="' + fileNameInputName + '"]');
	const fileName = fileNameInput.value;

	// Get the name before rename
	const postFileComponentElement = document.getElementById(postFileComponentId);
	const postFileComponentNameElement = postFileComponentElement.querySelector('[name="' +
		postFileComponentNameElementName + '"]');

	// Get the name with extension before rename
	const oldName = postFileComponentNameElement.innerText;

	// Change the name of the file in the UI	
	postFileComponentNameElement.innerText = fileName + postFileExtension;
	
	$('#' + modalId).modal('hide');
	
	// Prepare the request
	const formData = new FormData();
	formData.append(fileNameInputName, fileName);

	// Send the request
	sendPostFormData(renamePostFileUrl, formData, () => {
		// Change the name back if there is an error
		postFileComponentNameElement.innerText = oldName;
	});

	return false;
}

function removeFile({ postFileComponentId, removeFileUrl }) {
	
	const postFileComponentElement = document.getElementById(postFileComponentId);

	// Hide the file from the list of files
	postFileComponentElement.classList.add("d-none");

	sendPost(removeFileUrl,
		() => {
			// On error show back the file in the list of files
			postFileComponentElement.classList.remove("d-none");
		});


}