﻿function likePost({ event, likeUrlTrue, likeUrlFalse }) {

	const classLiked = "post-button-active";

	const button = event.currentTarget;

	const isLiked = button.classList.contains(classLiked);

	if (isLiked) {
		// Remove the liked class
		button.classList.remove(classLiked);

		// Increase the amount of likes by 1
		const likesCountSpan = button.getElementsByTagName('span')[0];
		likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) - 1;

		// Call the ajax to dislike the post
		sendPost(likeUrlFalse,
			function () {
				button.classList.add(classLiked);
				likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) + 1;
			});
	}
	else {
		// Add the liked class
		button.classList.add(classLiked);

		// Decrease the amount of likes by 1
		const likesCountSpan = button.getElementsByTagName('span')[0];
		likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) + 1;
		

		// Call the ajax to dislike the post
		sendPost(likeUrlTrue,
			function () {
				button.classList.remove(classLiked);
				likesCountSpan.innerHTML = parseInt(likesCountSpan.innerHTML) - 1;
			}
		);
	}
}