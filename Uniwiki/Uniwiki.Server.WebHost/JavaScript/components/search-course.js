﻿function searchCourse(text) {
	const lowercaseText = text.toLowerCase(); //.filter(ch => (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9'));

	const normalizedText = removeDiacritics(lowercaseText).replace(/[^0-9a-z]/gi, '');

	console.log(normalizedText);

	const attributeSearchText = "search-text";
	const classCourseItem = ".course-item";
	const classCourseItemList = ".course-item-list";

	// Get all course items
	const courseItems = document.querySelectorAll(classCourseItem);
	console.log(courseItems.length);

	// If there is no text
	if (!text) {
		// Show all courses
		courseItems.forEach(element => showElement(element));
	} else {
		// Show only courses containing the phrase
		courseItems.forEach(
			element => element.getAttribute(attributeSearchText).includes(normalizedText) ? showElement(element) : hideElement(element));
	}
}