﻿const classCategorySelected = "\category-selected";
const attributeCategory = "category";
const attributeCategoryInputId = "category-input-id";
const attributeSelectedCategoryTextId = "selected-category-text-id";
const attributeValue = 'value';
const attributeSelectCategoryInitialText = 'initial-text';

function selectCategoryByButton(event) {
	// Get the parent node
	const button = event.currentTarget;
	const parent = button.parentNode;

	unselectAllCategoryElements(parent);

	// Add the selected class to the clicked button
	if (!button.classList.contains(classCategorySelected))
		button.classList.add(classCategorySelected);

	// Get the category to set
	const category = button.getAttribute(attributeCategory);

	displaySelectedCategory(parent, category);
}

function selectCategoryByTextInput(event) {
	const input = event.currentTarget;
	const parent = input.parentNode;

	const category = input.value;

	console.log(category);

	const isInputSelectedCategory = input.classList.contains(classCategorySelected);

	// If there is no text in the input and its the selected category
	if (!category && isInputSelectedCategory) {
		// Unselect the input
		unselectAllCategoryElements(parent);

		// Display unselected text
		displaySelectedCategory(parent, category);
		return;
	}

	if (category) {

		// Unselect all
		unselectAllCategoryElements(parent);

		// Add the selected class to the input
		input.classList.add(classCategorySelected);

		// Display unselected text
		displaySelectedCategory(parent, category);

		return;
	}
}

function displaySelectedCategory(parent, category) {

	// Get the ID of the selected category text element
	const categoryTextElementId = parent.getAttribute(attributeSelectedCategoryTextId);

	// Find the selected category text element
	const categoryTextElement = document.getElementById(categoryTextElementId);

	// Id of the input in the form for setting the category
	const categoryInputId = parent.getAttribute(attributeCategoryInputId);

	// Get the input in the form for setting the category
	const categoryInput = document.getElementById(categoryInputId);

	// Set the value of the category input to the new value
	categoryInput.setAttribute(attributeValue, category);

	// If a category is selected
	if (category) {
		// Set the category to the field on the AddOrEditPostPage
		categoryTextElement.innerText = category;
	} else {
		// Set the category to the field on the AddOrEditPostPage
		categoryTextElement.innerText = categoryTextElement.getAttribute(
			attributeSelectCategoryInitialText);

		// Make the button of no selected category selected
		const noCategoryButton = parent.querySelector('button:not([category])');

		noCategoryButton.classList.add(classCategorySelected);
	}
}

function unselectAllCategoryElements(parent) {

	// Remove the selected class from all buttons in the parent element
	Array.from(parent.getElementsByClassName(classCategorySelected))
		.forEach(element => element.classList.remove(classCategorySelected));

}

function categoryInputFocused(event) {
	const input = event.currentTarget;

	// Focus the input if there is any value in it
	if (input.value) {
		selectCategoryByTextInput(event);
	}
}

