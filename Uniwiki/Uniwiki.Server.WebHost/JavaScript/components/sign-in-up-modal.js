﻿function handleEmailOnFocus({ stepsId, step0Class, step1Class }) {
	
	const stepsElement = document.getElementById(stepsId);

	// If the current step is 0
	if (stepsElement.classList.contains(step0Class)) {
		// Go to the 1st step
		stepsElement.classList.remove(step0Class);
		stepsElement.classList.add(step1Class);
	}
}

function handleEmailOnBlur({ event, stepsId, step0Class, step1Class }) {

	const stepsElement = document.getElementById(stepsId);

	const email = event.currentTarget.value;
	console.log(email);
	const emailHasValue = email && email.length > 0;

	// If the current step is 1
	if (stepsElement.classList.contains(step1Class) && !emailHasValue) {
		// Go to the step 0
		stepsElement.classList.remove(step1Class);
		stepsElement.classList.add(step0Class);
	}
}

function handleEmailOnInput({
	event,
	submitEmailButtonId,
	errorMessageId
}) {
	const email = event.currentTarget.value;
	const isEmailValid = validateEmail(email);
	const submitEmailButton = document.getElementById(submitEmailButtonId);
	hideErrorMessage(errorMessageId);

	if (isEmailValid) {
		submitEmailButton.disabled = false;
	} else {
		submitEmailButton.disabled = true;
	}
}

var signInUpPendingRequest = null;

function signInUpViaEmailSubmit({
	submitEmailButtonId,
	emailInputId,
	statusReadyClass,
	statusInProgressClass,
	sendVerificationCodeUrl,
	errorMessageId,
	stepsId,
	step1Class,
	step2Class,
	codeInputId
}) {
	const emailInput = document.getElementById(emailInputId);
	const emailValue = emailInput.value;
	const emailValueJson = JSON.stringify(emailValue);
	const submitButton = document.getElementById(submitEmailButtonId);

	const displayInProgress = () => {
		// Disable the emailInputId and change the button to "Submitting..."
		// Disable the button
		hideErrorMessage(errorMessageId);
		submitButton.disabled = true;
		emailInput.disabled = true;
		submitButton.classList.remove(statusReadyClass);
		submitButton.classList.add(statusInProgressClass);
	}

	const displayReady = () => {
		// Disable the emailInputId and change the button to "Submitting..."
		submitButton.disabled = false;
		submitButton.classList.add(statusReadyClass);
		submitButton.classList.remove(statusInProgressClass);
	}

	displayInProgress();

	// Abort any pending request
	if (signInUpPendingRequest) {
		signInUpPendingRequest.abort();
		signInUpPendingRequest = null;
	}

	// Send request to the server
	signInUpPendingRequest = sendPost(sendVerificationCodeUrl,
		(xhr) => {
			// Display the error message
			console.log(xhr.status);

			// Return the UI to the state before sending the request
			displayReady();

			// Allow the user to change the email
			emailInput.disabled = false;

			if (xhr.status === 400) {
				showErrorMessage(errorMessageId, xhr.response);
				return true;
			}

			return false;
		},
		emailValueJson,
		() => {
			displayReady();
			const stepsElement = document.getElementById(stepsId);
			stepsElement.classList.remove(step1Class);
			stepsElement.classList.add(step2Class);
			signInUpPendingRequest = null;

			// Clear the code input
			changeValueAndInvokeOninput(codeInputId, "");

			// TODO: Delete this!
			//location.reload();
		},
		false);
}

function handleSendCodeAgain({
	submitEmailButtonId,
	emailInputId,
	statusReadyClass,
	statusInProgressClass,
	sendVerificationCodeUrl,
	errorMessageId,
	stepsId,
	step1Class,
	step2Class,
	codeInputId
}) {
	const stepsElement = document.getElementById(stepsId);

	// Go from the step 2 to the step 1
	stepsElement.classList.remove(step2Class);
	stepsElement.classList.add(step1Class);

	signInUpViaEmailSubmit({
		submitEmailButtonId,
		emailInputId,
		statusReadyClass,
		statusInProgressClass,
		sendVerificationCodeUrl,
		errorMessageId,
		stepsId,
		step1Class,
		step2Class,
		codeInputId
	});
}

function hideErrorMessage(errorMessageId) {
	const element = document.getElementById(errorMessageId);
	element.classList.add("d-none");
}

function showErrorMessage(errorMessageId, errorMessage) {
	const element = document.getElementById(errorMessageId);
	element.innerText = errorMessage;
	element.classList.remove("d-none");
}

function signInUpViaEmailChangeEmail({
	emailInputId,
	stepsId,
	step0Class,
	step1Class,
	step2Class,
	errorMessageId
}) {
	const emailInput = document.getElementById(emailInputId);
	const stepsElement = document.getElementById(stepsId);

	// Go from the step 2 to the step 0
	stepsElement.classList.remove(step2Class);
	stepsElement.classList.add(step0Class);

	// Enable the email input
	emailInput.disabled = false;

	// Focus the email input
	emailInput.focus();

	// Clear the error
	hideErrorMessage(errorMessageId);
}

function handleCodeOnInput({ codeInputId, submitCodeButtonId, codeLengthMax }) {
	// Get the code
	const code = document.getElementById(codeInputId).value;

	// Get the submit code button
	const submitCodeButton = document.getElementById(submitCodeButtonId);

	// If the code have codeLengthMax characters
	if (code && code.length === codeLengthMax) {
		// Enable the submit code button
		submitCodeButton.disabled = false;
	} else {
		// Disable the submit code button
		submitCodeButton.disabled = true;
	}
}

function handleSubmitCode({
	sendCodeAgainId,
	codeInputId,
	changeEmailButtonId,
	submitCodeButtonId,
	statusReadyClass,
	statusInProgressClass,
	emailInputId,
	signInUpViaEmailUrl,
	errorMessageId,
	signInCode,
	signUpCode,
	step2Class,
	step3Class,
	stepsId
}) {
	const sendCodeAgainButton = document.getElementById(sendCodeAgainId);
	const codeInput = document.getElementById(codeInputId);
	const changeEmailButton = document.getElementById(changeEmailButtonId);
	const submitCodeButton = document.getElementById(submitCodeButtonId);

	const displayInProgress = () => {
		// Disable "Send the code again"
		sendCodeAgainButton.disabled = true;

		// Disable code input
		codeInput.disabled = true;

		// Disable change email
		changeEmailButton.disabled = true;

		// Disable Submit code button
		submitCodeButton.disabled = true;

		// Show "Submitting code"
		submitCodeButton.classList.remove(statusReadyClass);
		submitCodeButton.classList.add(statusInProgressClass);
	}

	const displayReady = () => {
		// Disable "Send the code again"
		sendCodeAgainButton.disabled = false;

		// Disable code input
		codeInput.disabled = false;

		// Disable change email
		changeEmailButton.disabled = false;

		// Disable Submit code button
		submitCodeButton.disabled = false;

		// Show "Submitting code"
		submitCodeButton.classList.add(statusReadyClass);
		submitCodeButton.classList.remove(statusInProgressClass);
	}

	displayInProgress();

	// Hide the error message
	hideErrorMessage(errorMessageId);

	// Get the email
	const emailInput = document.getElementById(emailInputId);

	// Make the request
	const request = {
		code: codeInput.value,
		email: emailInput.value
	};

	// Make JSON from the request
	const requestJson = JSON.stringify(request);

	// Send the request to the server
	sendPost(signInUpViaEmailUrl,
		(xhr) => {
			displayReady();

			if (xhr.status === 400) {
				showErrorMessage(errorMessageId, xhr.response);
				return true;
			}

			return false;
		},
		requestJson,
		(xhr) => {
			const code = JSON.parse(xhr.response).code.toString();
			console.log(xhr.response, signInCode, signUpCode);

			// If it is signup - show the last step
			if (code === signInCode) {
				// Reload the page
				location.reload();
			}

			// If it is sign in - refresh the page
			if (code === signUpCode) {
				const stepsElement = document.getElementById(stepsId);

				// Go to the step 3
				stepsElement.classList.remove(step2Class);
				stepsElement.classList.add(step3Class);
			}
		},
		false);
}


function handleSignUpViaEmail({
	nameInputId,
	surnameInputId,
	signUpButtonId,
	statusReadyClass,
	statusInProgressClass,
	errorMessageId,
	signUpViaEmailUrl,
	emailInputId,
	codeInputId
}) {
	const nameInput = document.getElementById(nameInputId);
	const surnameInput = document.getElementById(surnameInputId);
	const signUpButton = document.getElementById(signUpButtonId);
	const emailInput = document.getElementById(emailInputId);
	const codeInput = document.getElementById(codeInputId);

	const displayInProgress = () => {
		// Disable the name input
		nameInput.disabled = true;

		// Disable the surname input
		surnameInput.disabled = true;

		// Disable the sign up button
		signUpButton.disabled = true;

		// Show the 'in progress state'
		signUpButton.classList.remove(statusReadyClass);
		signUpButton.classList.add(statusInProgressClass);
	}

	const displayReady = () => {
		// Disable the name input
		nameInput.disabled = false;

		// Disable the surname input
		surnameInput.disabled = false;

		// Disable the sign up button
		signUpButton.disabled = false;

		// Show the 'in progress state'
		signUpButton.classList.add(statusReadyClass);
		signUpButton.classList.remove(statusInProgressClass);
	}

	displayInProgress();

	hideErrorMessage(errorMessageId);

	const request = {
		email: emailInput.value,
		code: codeInput.value,
		firstName: nameInput.value,
		surname: surnameInput.value
	}

	// Send the request to sign up
	sendPost(signUpViaEmailUrl,
		(xhr) => {
			displayReady();

			if (xhr.status === 400) {
				showErrorMessage(errorMessageId, xhr.response);
				return true;
			}

			return false;
		},
		JSON.stringify(request),
		() => {
			// Reload the page on success
			location.reload();
		}
	);
}


function validateEmail(email) {
	const re =
		/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

function changeValueAndInvokeOninput(inputId, value) {
	const input = document.getElementById(inputId);
	input.value = value;
	input.dispatchEvent(new Event('input'));
}