﻿const classPostFileProgressBarInProgress = "post-file-progress-bar-in-progress";
const classPostFileProgressBarFailed = "post-file-progress-bar-failed";
const classPostFileProgressBarSuccess = "post-file-progress-bar-success";

function handleUploadFilesSelected({
	event,
	relativeUploadUrl,
	templateOriginalId,
	uploadedFilesContainerId,
	uploadingFilesContainerId,
	modalId
}) {
	event.preventDefault();

	// Get the selected files
	const files = event.currentTarget.files;

	// If there are no files, do not do anything 
	if (files.length === 0)
		return;

	// Display the modal
	$('#' + modalId).modal('show');

	// Add it to the container with all the files
	const uploadingFilesContainer = document.getElementById(uploadingFilesContainerId);

	// Reset all the children
	uploadingFilesContainer.innerHTML = "";

	// Get template for the file loader
	const templateOriginal = document.getElementById(templateOriginalId);

	// Prepare the files to a queue for uploading
	const filesQueue = [...files];

	// The amount of files, that are gonna be uploaded simoultaneously
	const uploadingSlotsMax = 3;

	// The remaining number of slots
	let uploadingSlots = uploadingSlotsMax;

	const uploadUrl = new URL(relativeUploadUrl, window.location.origin);

	const removeBarClasses = (element) => {
		element.classList.remove(classPostFileProgressBarInProgress);
		element.classList.remove(classPostFileProgressBarFailed);
		element.classList.remove(classPostFileProgressBarSuccess);
	}

	const uploadFile = () => {
		if (filesQueue.length === 0 || !uploadingSlots) {
			return;
		}

		// Take an element from the queue
		const fileToUpload = filesQueue.shift();

		// If there is actually no file to upload, do nothing - this might be unnecessary, its just a preventive concurrency check
		if (!fileToUpload) {

			return;
		}

		uploadingSlots--;

		// Start the actual uploading by creating and sending a request to the server
		const request = new XMLHttpRequest();

		// Register the request to the element
		fileToUpload.fileElement.request = request;

		// Prepare this as the data to send to the server
		var formData = new FormData();

		// Add the file to the request
		formData.append("File", fileToUpload);

		// Get the progress bar, to modify on progress
		const progressBar = fileToUpload.fileElement.querySelector(".progress-bar");

		// Handle progress
		request.upload.addEventListener("progress", (e) => {
			console.log("progress");
			const progress = Math.round((e.loaded / e.total) * 100);

			// Set the progress on the progress bar
			progressBar.style.width = progress + '%';
		});


		// Handle timeout
		request.addEventListener("timeout", () => {
			console.log("timeout");

			// Change the UI to a failed state
			removeBarClasses(fileToUpload.fileElement);
			fileToUpload.fileElement.classList.add(classPostFileProgressBarFailed);

			// Return uploading slot
			uploadingSlots++;

			// Try to upload another file
			uploadFile();
		});

		// Handle abort
		request.addEventListener("abort", () => {
			console.log("abort");

			// Change the UI to a failed state
			removeBarClasses(fileToUpload.fileElement);
			fileToUpload.fileElement.classList.add(classPostFileProgressBarFailed);

			// Return uploading slot
			uploadingSlots++;

			// Try to upload another file
			uploadFile();
		});

		// Handle error
		request.addEventListener("error", () => {
			console.log("error");

			// Change the UI to a failed state
			removeBarClasses(fileToUpload.fileElement);
			fileToUpload.fileElement.classList.add(classPostFileProgressBarFailed);

			// Return uploading slot
			uploadingSlots++;

			// Try to upload another file
			uploadFile();
		});

		// Handle success
		request.onreadystatechange = () => {
			// If success
			if (request.readyState === 4 && request.status === 200) {
				console.log("Success");

				// Change the UI to a success state
				removeBarClasses(fileToUpload.fileElement);
				fileToUpload.fileElement.classList.add(classPostFileProgressBarSuccess);

				// Get the container for all uploaded files
				const uploadedFilesContainer = document.getElementById(uploadedFilesContainerId);

				// Add the node to the file element
				uploadedFilesContainer.innerHTML += (request.response);

				// Return uploading slot
				uploadingSlots++;

				// Try to upload another file
				uploadFile();

				// Check if all the files are already uploaded (no file is being uploaded and there is no failed file)
				if (uploadingSlots === uploadingSlotsMax &&
					filesQueue.length === 0 &&
					!uploadingFilesContainer.querySelector("." + classPostFileProgressBarFailed)) {

					setTimeout(() => {
						// Close the modal
						$('#' + modalId).modal('hide');
					}, 1150);
				}
			}
		};

		// Clean the selected files
		// event.currentTarget.value = "";

		// Initialize the request
		request.open("POST", uploadUrl);

		// Send the request asynchronously
		request.send(formData);
	}

	// Add UI for each of the files
	for (let file of files) {
		// Create copy of the UI for displaying a file download
		const fileElement = templateOriginal.cloneNode(true);

		// Get the element of the fileElement, that displays name
		const fileNameElement = fileElement.querySelector('[name="fileName"]');

		// Set the name to the name of the file
		fileNameElement.innerText = file.name;

		// Get the element of the fileElement, that displays name
		const fileSizeElement = fileElement.querySelector('[name="fileSize"]');

		// Display the file size (converted to readable format)
		fileSizeElement.innerText = toReadableFileSize(file.size);

		// Add the UI to the container
		uploadingFilesContainer.appendChild(fileElement);

		// Attach the component to the file, for setting uploading state
		file.fileElement = fileElement;

		// Sometimes we will need to access the file from other nodes
		fileElement.file = file;

		fileElement.tryAgainUploading = () => {

			// Signalize, that the file is in progress
			removeBarClasses(fileElement);
			fileElement.classList.add(classPostFileProgressBarInProgress);

			// Add himself to the queue
			filesQueue.push(file);

			// Start uploading another file
			uploadFile();
		};

		fileElement.abortUpload = () => {
			// If the file already started uploading
			if (fileElement.request) {
				fileElement.request.abort();
			} else {
				// remove the file from the queue
				filesQueue.splice(filesQueue.indexOf(file));

				// Change the UI to a failed state
				removeBarClasses(fileElement);
				fileElement.classList.add(classPostFileProgressBarFailed);
			}
		}
	}

	// Start uploading the files to the server - use all the available slots
	for (let i = 0; i < uploadingSlotsMax; i++) {
		uploadFile();
	}
}

function removeUploadingPostFile(
	{
		event,
		uploadingFilesContainerId,
		modalId
	}
) {

	const container = document.getElementById(uploadingFilesContainerId);

	// Remove the file component from UI
	event.currentTarget.parentNode.parentNode.remove();

	// if the last file is removed
	if (!container.hasChildNodes()) {

		// Close the modal
		$('#' + modalId).modal('hide');
	}
}

function tryAgainUploadingPostFile(event) {
	const fileNode = event.currentTarget.parentNode.parentNode;

	fileNode.tryAgainUploading();
}

function cancelUploadingPostFile(event) {
	const fileNode = event.currentTarget.parentNode.parentElement;
	
	fileNode.abortUpload();
}

function handleAbortRemaining({
	uploadingFilesContainerId,
	modalId
}) {
	const container = document.getElementById(uploadingFilesContainerId);

	// Abort all uploading files
	const inProgressFiles = container.querySelectorAll("." + classPostFileProgressBarInProgress);

	for (let file of inProgressFiles) {
		file.abortUpload();
	}

	// Close modal
	$('#' + modalId).modal('hide');
}