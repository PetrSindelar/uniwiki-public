﻿var fetchNotificationsXhr = null;

function handleOpeningModal({
	notificationsContainerId,
	notificationsCountId,
	fetchNotificationsButtonId,
	fetchPostsUrl
}) {
	// Dont fetch the notifications multiple times
	if (fetchNotificationsXhr) {
		return;
	}

	const notificationsCount = document.getElementById(notificationsCountId);
	const fetchNotificationsButton = document.getElementById(fetchNotificationsButtonId);
	const notificationsContainer = document.getElementById(notificationsContainerId);

	// Hide the number of new notifications
	notificationsCount.classList.add(constants.classDNone);

	// Show fetching of new posts
	const showInProgress = () => {
		fetchNotificationsButton.classList.remove(constants.classStatusReady);
		fetchNotificationsButton.classList.add(constants.classStatusInProgress);
		fetchNotificationsButton.disabled = true;
	}

	const showReady = () => {
		fetchNotificationsButton.classList.add(constants.classStatusReady);
		fetchNotificationsButton.classList.remove(constants.classStatusInProgress);
		fetchNotificationsButton.disabled = false;
	}

	showInProgress();

	// TODO: Get the last timestamp
	const timestamp = null;

	const url = fetchPostsUrl + "/" + timestamp;

	// TODO: Remove the delay, for now, show it just to see the progress bar
	setTimeout(() =>

	// TODO: Create request to fetch new posts
	fetchNotificationsXhr = sendPost(url,
		() => {
			// Handle errors
			showReady();
		},
		null,
		(xhr) => {
			// Handle success
			showReady();

			notificationsContainer.innerHTML += xhr.response;

			// Hide the load button
			// TODO: Hide the button just if there are no more notifications to load
			fetchNotificationsButton.classList.add(constants.classDNone);
		}),
	1000);

}