﻿function signInUpViaFacebook(signInUpViaFacebookUrl, response) {
	if (response.status !== 'connected') {
		return;
	}

	const signedRequest = JSON.stringify(response.authResponse.signedRequest);

	console.log("authResponse", response.authResponse);

	// Send the token to the server
	sendPost(signInUpViaFacebookUrl, () => { }, signedRequest, () => window.location.reload());
}