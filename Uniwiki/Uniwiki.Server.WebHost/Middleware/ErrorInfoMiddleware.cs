using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Constants;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebHost.Services;

namespace Uniwiki.Server.WebHost.Middleware
{
	/// <summary>
	/// This middleware determines how should errors be handled for this request. For example if the person wants to see a page that does not exist, he should recive not found page, but if he calls api, that does not exist, he should get just 404 with text of the error.
	/// </summary>
	[ScopedService(serviceType: typeof(ErrorInfoMiddleware), serviceLifetime: ServiceLifetime.Scoped)]
	public class ErrorInfoMiddleware : IMiddleware
	{
		private readonly ErrorService _errorService;

		public ErrorInfoMiddleware(ErrorService errorService)
		{
			_errorService = errorService;
		}

		public async Task InvokeAsync(HttpContext context, RequestDelegate next)
		{
			if (!_errorService.HasErrorHandling)
			{
				if (context.Request.Path.StartsWithSegments(new PathString(RouteConstants.ApiRoute)))
				{
					_errorService.SetErrorsHandling(RouteConstants.ApiRoute);
				}
				else if (context.Request.Path.StartsWithSegments(new PathString(RouteConstants.PartialViewRoute)))
				{
					_errorService.SetErrorsHandling(RouteConstants.PartialViewRoute);
				}
				else if (context.Request.Path.StartsWithSegments(new PathString(RouteConstants.PageRoute)))
				{
					_errorService.SetErrorsHandling(RouteConstants.PageRoute);
				}
				// TODO: Once there will be constant for all static files, then move this above RouteConstants.PageRoute
				else if (context.Request.Path.StartsWithSegments(new PathString(RouteConstants.StaticFileRoute)))
				{
					_errorService.SetErrorsHandling(RouteConstants.StaticFileRoute);
				}
			}


			await next.Invoke(context);
		}
	}
}