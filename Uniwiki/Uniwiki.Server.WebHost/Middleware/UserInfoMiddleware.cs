using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication.Shared.Services;

namespace Uniwiki.Server.WebHost.Middleware
{
	/// <summary>
	/// This middleware should get the information for authenticated user, that is used every time a request is sent. This means for now: the number of notifications.
	/// TODO: Get the number of new messages
	/// </summary>
	[ScopedService(serviceType: typeof(UserInfoMiddleware), serviceLifetime: ServiceLifetime.Scoped)]
	public class UserInfoMiddleware : IMiddleware
	{
		private readonly IUserInfoService _userInfoService;

		public UserInfoMiddleware(IUserInfoService userInfoService)
		{
			_userInfoService = userInfoService;
		}

		public async Task InvokeAsync(HttpContext context, RequestDelegate next)
		{
			// Initialize user service
			await _userInfoService.InitializeAsync(context.User);

			await next.Invoke(context);
		}
	}
}