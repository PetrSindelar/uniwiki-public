using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using Uniwiki.Server.WebApplication.Shared.Configurations;

namespace Uniwiki.Server.WebHost
{
	

	public class Program
	{
		public static void Main(string[] args)
		{
			//var rootDirectory = Path.GetDirectoryName(typeof(Program).Assembly.Location) ?? string.Empty;

			//Directory.SetCurrentDirectory(rootDirectory);

			// Configure Logging
			var configuration = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json")
				.Build();

			Log.Logger = new LoggerConfiguration()
				.ReadFrom
				.Configuration(configuration)
				.CreateLogger();

			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args)
			=> Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(
					webBuilder =>
					{
						webBuilder.UseStartup<Startup>()
							.UseKestrel(options => options.ConfigureEndpoints());
					}
				)
				.UseSerilog();
	}

	public static class KestrelServerOptionsExtensions
	{
		public static void ConfigureEndpoints(this KestrelServerOptions options)
		{
			var configuration = options.ApplicationServices.GetRequiredService<IConfiguration>();
			var environment = options.ApplicationServices.GetRequiredService<IWebHostEnvironment>();

			var endpoints = configuration.GetSection("HttpServer:Endpoints")
				.GetChildren()
				.ToDictionary(
					section => section.Key,
					section =>
					{
						var endpoint = new EndpointConfiguration();
						section.Bind(endpoint);

						return endpoint;
					}
				);

			foreach (var endpoint in endpoints)
			{
				var config = endpoint.Value;
				var port = config.Port ?? (config.Scheme == "https" ? 443 : 80);

				var ipAddresses = new List<IPAddress>();
				if (config.Host == "localhost")
				{
					ipAddresses.Add(IPAddress.IPv6Loopback);
					ipAddresses.Add(IPAddress.Loopback);
				}
				else if (IPAddress.TryParse(config.Host, out var address))
				{
					ipAddresses.Add(address);
				}
				else
				{
					ipAddresses.Add(IPAddress.IPv6Any);
				}

				foreach (var address in ipAddresses)
				{
					options.Listen(
						address,
						port,
						listenOptions =>
						{
							if (config.Scheme == "https")
							{
								var certificate = LoadCertificate(config, environment);
								listenOptions.UseHttps(certificate);
							}
						}
					);
				}
			}
		}

		private static X509Certificate2 LoadCertificate(EndpointConfiguration config, IWebHostEnvironment environment)
		{
			if (config.StoreName != null && config.StoreLocation != null)
			{
				using (var store = new X509Store(config.StoreName, Enum.Parse<StoreLocation>(config.StoreLocation)))
				{
					store.Open(OpenFlags.ReadOnly);
					var certificate = store.Certificates.Find(
						X509FindType.FindBySubjectName,
						config.Host,
						!environment.IsDevelopment()
					);

					if (certificate.Count == 0)
					{
						throw new InvalidOperationException($"Certificate not found for {config.Host}.");
					}

					return certificate[0];
				}
			}

			if (config.FilePath != null && config.Password != null)
			{
				return new X509Certificate2(config.FilePath, config.Password);
			}

			throw new InvalidOperationException("No valid certificate configuration found for the current endpoint.");
		}
	}
}