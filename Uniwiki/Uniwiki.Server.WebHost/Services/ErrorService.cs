﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.Shared.Exceptions;
using Uniwiki.Server.WebApplication.Shared.Constants;

namespace Uniwiki.Server.WebHost.Services
{
	[ScopedService]
	public class ErrorService
	{
		private string? _routeType;

		public void SetErrorsHandling(string routeType)
		{
			Console.WriteLine("Setting handling: " + _routeType);
			_routeType = routeType;
		}

		private string GetRouteTypeOrThrow()
			=> _routeType ?? throw new ServerException(debugMessage: "The route type is not set.");

		public bool IsApi() => GetRouteTypeOrThrow() == RouteConstants.ApiRoute;

		public bool IsPartialView() => GetRouteTypeOrThrow() == RouteConstants.PartialViewRoute;

		public bool IsPage() => GetRouteTypeOrThrow() == RouteConstants.PageRoute;

		// TODO: Make a different route for static files
		public bool IsStaticFile() => GetRouteTypeOrThrow() == RouteConstants.StaticFileRoute;

		public bool HasErrorHandling => _routeType != null;
	}
}