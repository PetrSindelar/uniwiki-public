using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Serilog;
using Uniwiki.Server.Persistence;
using Uniwiki.Server.Persistence.Models;
using Uniwiki.Server.Shared.DependencyInjection;
using Uniwiki.Server.WebApplication;
using Uniwiki.Server.WebApplication.Shared.Configurations;
using Uniwiki.Server.WebApplication.Shared.Functions;
using Uniwiki.Server.WebApplication.Shared.Services;
using Uniwiki.Server.WebApplication.User.SignInUpPage;
using Uniwiki.Server.WebHost.Middleware;
using Westwind.AspNetCore.LiveReload;

namespace Uniwiki.Server.WebHost
{
	public class ProfileModelClaimsPrincipalFactory : UserClaimsPrincipalFactory<ProfileModel>
	{
		public ProfileModelClaimsPrincipalFactory(
			UserManager<ProfileModel> userManager,
			IOptions<IdentityOptions> optionsAccessor
		)
			: base(userManager, optionsAccessor)
		{
		}

		protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ProfileModel user)
		{
			var identity = await base.GenerateClaimsAsync(user);

			var roles = await UserManager.GetRolesAsync(user);

			return UserClaimsFunctions.GenerateClaimsAsync(identity, user, roles);
		}
	}

	// Recommended order of the processing pipeline https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-5.0
	// Improve performance - caching of requests / responses - https://docs.microsoft.com/en-us/aspnet/core/performance/caching/response?view=aspnetcore-5.0#:~:text=Response%20caching%20reduces%20the%20number,and%20middleware%20to%20cache%20responses.
	// GDPR: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv%3AOJ.L_.2016.119.01.0089.01.ENG&toc=OJ%3AL%3A2016%3A119%3ATOC

	public class Startup
	{
		private readonly IWebHostEnvironment _webHostEnvironment;
		public IConfiguration Configuration { get; }

		public Startup(IWebHostEnvironment webHostEnvironment, IConfiguration configuration)
		{
			_webHostEnvironment = webHostEnvironment;
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//services.AddTransient<UserInfoMiddleware>();
			var connectionString = Configuration.GetSection("Database")
				.GetValue<string>(nameof(DatabaseOptions.ConnectionString));

			// Uniwiki application layer
			services.AddUniwikiServerWebApplicationServices(
				builder =>
					builder.UseSqlServer(connectionString),
				_webHostEnvironment.IsDevelopment()
			);


			// MVC
			services
				.AddControllersWithViews(
					config =>
					{
						var policy = new AuthorizationPolicyBuilder()
							.RequireAuthenticatedUser()
							.Build();
						config.Filters.Add(new AuthorizeFilter(policy));
					}
				)
				.AddRazorRuntimeCompilation();

			// Live reload 
			if (_webHostEnvironment.IsDevelopment())
			{
				services.AddLiveReload();
			}

			// Authentication + Authorization
			services.AddIdentity<ProfileModel, IdentityRole<Guid>>()
				.AddRoles<IdentityRole<Guid>>()
				.AddEntityFrameworkStores<UniwikiContext>()
				.AddDefaultTokenProviders()
				.AddClaimsPrincipalFactory<ProfileModelClaimsPrincipalFactory>();


			services.AddAttributeServices(typeof(Startup).Assembly);

			services.ConfigureApplicationCookie(
				options =>
				{
					// Cookie settings
					options.Cookie.HttpOnly = true;
					options.ExpireTimeSpan = TimeSpan.FromDays(Configuration.GetSection("Authentication")
						.GetValue<int>(nameof(AuthenticationOptions.SignInTokenDurationDays)));

					options.LoginPath = new SignInUpPageRoute().Build();

					// TODO: Create AccessDeniedPath
					options.AccessDeniedPath = new SignInUpPageRoute().Build();
					options.SlidingExpiration = true;
				}
			);

			// Configurations
			services.Configure<IdentityOptions>(Configuration.GetSection("Identity"));
			services.Configure<StorageConfiguration>(Configuration.GetSection("Storage"));
			services.Configure<AuthenticationOptions>(Configuration.GetSection("Authentication"));
			services.Configure<FormsOptions>(Configuration.GetSection("Forms"));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IServiceProvider serviceProvider, IApplicationBuilder app, IWebHostEnvironment env)
		{
			Log.Debug("Starting app: " + env.EnvironmentName);

			// Initialize the data in the database
			using (var scope = serviceProvider.CreateScope())
			{
				scope.ServiceProvider
					.GetRequiredService<ServerContentInitiatorService>()
					.Initialize()
					.GetAwaiter()
					.GetResult();
			}


			if (env.IsDevelopment())
			{
				// TODO: I will have to cofigure cors for the login via Facebook and Google
				app.UseCors(b
					=> b.SetIsOriginAllowed(x => _ = true).AllowAnyOrigin().AllowAnyOrigin().AllowAnyHeader());

				// Display useful exceptions
				//app.UseDeveloperExceptionPage();

				// Reloading of the app when a frontend file changes
				app.UseLiveReload();
			}
			else
			{
				// TODO: Create a page for handling exceptions
				// app.UseExceptionHandler("/Home/Error");

				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseStaticFiles();

			app.UseMiddleware<ErrorInfoMiddleware>();

			// TODO: Handle API request errors properly
			app.UseStatusCodePagesWithReExecute("/error", "?statusCode={0}"); //new ErrorPageRoute(0).BuildFormat());

			// TODO: Handle API request errors properly
			app.UseExceptionHandler("/HandleException");

			// app.UseHttpsRedirection();

			app.UseSerilogRequestLogging();

			// TODO: Move this to configuration...
			app.UseCookiePolicy(
				new CookiePolicyOptions
				{
					MinimumSameSitePolicy = SameSiteMode.Strict,
				}
			);

			app.UseRouting();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseMiddleware<UserInfoMiddleware>();

			app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
		}
	}
}