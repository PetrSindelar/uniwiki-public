﻿using System.Web;

namespace Uniwiki.Shared.Tests.FakeServices
{
	public class FakeLanguageService
	{
		public Language Language { get; private set; } = Constants.DefaultLanguage;

		public string GetTranslation(string czech, string english) => Language.Czech == Language ? czech : english;

		public T GetTranslation<T>(T czech, T english) => Language.Czech == Language ? czech : english;

		public string Sanitize(string text) => HttpUtility.HtmlEncode(text);

		public void SetLanguage(Language language) => Language = language;
	}
}
