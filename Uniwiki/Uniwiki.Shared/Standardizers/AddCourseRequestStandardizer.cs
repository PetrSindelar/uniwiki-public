﻿using Shared.Extensions;
using Shared.Services;
using Shared.Standardizers;

namespace Uniwiki.Shared.Standardizers
{
	//internal class AddCourseRequestStandardizer : IStandardizer<AddCourseRequestDto>
	//{
	//	private readonly StringStandardizationService _stringStandardizationService;

	//	public AddCourseRequestStandardizer(StringStandardizationService stringStandardizationService)
	//	{
	//		_stringStandardizationService = stringStandardizationService;
	//	}

	//	public AddCourseRequestDto Standardize(AddCourseRequestDto model)
	//		=> new AddCourseRequestDto(
	//			_stringStandardizationService.OptimizeWhiteSpaces(model.CourseName).FirstCharToUpper(),
	//			_stringStandardizationService.OptimizeWhiteSpaces(model.CourseCode),
	//			model.StudyGroupId
	//		);
	//}
}