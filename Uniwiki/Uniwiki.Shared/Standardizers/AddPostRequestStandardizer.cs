﻿using Shared.Services;
using Shared.Standardizers;

namespace Uniwiki.Shared.Standardizers
{
	//internal class AddPostRequestStandardizer : IStandardizer<AddPostRequestDto>
	//{
	//	private readonly StringStandardizationService _stringStandardizationService;

	//	public AddPostRequestStandardizer(StringStandardizationService stringStandardizationService)
	//	{
	//		_stringStandardizationService = stringStandardizationService;
	//	}

	//	public AddPostRequestDto Standardize(AddPostRequestDto model)
	//	{
	//		var postCategory = string.IsNullOrWhiteSpace(model.PostType)
	//			? null
	//			: _stringStandardizationService.OptimizeWhiteSpaces(model.PostType?.Trim().FirstCharToUpper());

	//		return new AddPostRequestDto(
	//			model.Text.FirstCharToUpper().Trim(),
	//			postCategory,
	//			model.CourseId,
	//			model.PostFiles
	//		);
	//	}
	//}
}