﻿using Shared.Extensions;
using Shared.Services;
using Shared.Standardizers;

namespace Uniwiki.Shared.Standardizers
{
	//internal class AddStudyGroupRequestStandardizer : IStandardizer<AddStudyGroupRequestDto>
	//{
	//	private readonly StringStandardizationService _stringStandardizationService;

	//	public AddStudyGroupRequestStandardizer(StringStandardizationService stringStandardizationService)
	//	{
	//		_stringStandardizationService = stringStandardizationService;
	//	}

	//	public AddStudyGroupRequestDto Standardize(AddStudyGroupRequestDto model)
	//		=> new AddStudyGroupRequestDto(
	//			_stringStandardizationService.OptimizeWhiteSpaces(model.StudyGroupName).FirstCharToUpper(),
	//			_stringStandardizationService.OptimizeWhiteSpaces(model.StudyGroupShortcut),
	//			model.UniversityId,
	//			model.PrimaryLanguage
	//		);
	//}
}