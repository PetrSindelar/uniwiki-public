﻿using Shared.Extensions;
using Shared.Services;
using Shared.Standardizers;

namespace Uniwiki.Shared.Standardizers
{
	//internal class AddUniversityRequestStandardizer : IStandardizer<AddUniversityRequestDto>
	//{
	//	private readonly StringStandardizationService _stringStandardizationService;

	//	public AddUniversityRequestStandardizer(StringStandardizationService stringStandardizationService)
	//	{
	//		_stringStandardizationService = stringStandardizationService;
	//	}

	//	public AddUniversityRequestDto Standardize(AddUniversityRequestDto model)
	//		=> new AddUniversityRequestDto(
	//			_stringStandardizationService.OptimizeWhiteSpaces(model.FullName).FirstCharToUpper(),
	//			_stringStandardizationService.OptimizeWhiteSpaces(model.ShortName).FirstCharToUpper(),
	//			model.Url
	//		);
	//}
}