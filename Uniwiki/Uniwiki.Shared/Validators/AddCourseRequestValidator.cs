﻿using FluentValidation;
using Shared.Standardizers;
using Shared.Validators;

namespace Uniwiki.Shared.Validators
{
	internal class AddCourseRequestValidator : StandardizerValidator<AddCourseRequestDto>
	{
		public AddCourseRequestValidator(TextServiceShared textServiceBase,
			IStandardizer<AddCourseRequestDto> standardizer)
			: base(standardizer)
		{
			RuleFor(f => f.CourseCode)
				.MinMaxLengthWithMessages(textServiceBase, Constants.Validations.CourseCodeMinLength,
					Constants.Validations.CourseCodeMaxLength);

			RuleFor(f => f.StudyGroupId)
				.NotEmpty()
				.WithMessage(textServiceBase.Validation_YouMustSelectFaculty);

			RuleFor(f => f.CourseName)
				.Cascade(CascadeMode.StopOnFirstFailure)
				.MinMaxLengthWithMessages(
					textServiceBase,
					Constants.Validations.CourseNameMinLength,
					Constants.Validations.CourseNameMaxLength
				);
		}
	}
}